#  西西表白墙

## 简介

一款可即时通讯的表白墙微信小程序

[前端Gitee地址](https://gitee.com/wxdku/west-west-confession-front-end)



## 信息

- `原创作者`：甲粒子 + 王晓东(前端整体) + 徐文哲(跳蚤市场模块) + 贾晋(团队页面)
- `开源协议`：GPL 3.0
- `当前版本`：v1.0.1
- `发布日期`：2022.4.1



## 架构图

![image-20221021222930822](README.assets/image-20221021222930822.png)





## 技术栈

**SpringBoot**+**Mybatis**+**Netty**+**Redis**+**Mongodb**+**Mysql**+**SpringSecurity**+**七牛云存储**

![image-20221021222842188](README.assets/image-20221021222842188.png)



## 功能支持

- 即时通讯
- 发布表白墙
- 发布失物招领
- 发布跳蚤市场
- 后台用户管理
- 后台博客管理
- 后台文件上传
- 邮箱验证码
- 七牛云文件上传
- 学校官网新闻爬取



## 参与贡献

热烈欢迎其他同学加入我们的团队！！！一起开发，一起学习，共同成长

- QQ：2285096803
- 公众号：狂码工作室 或 狂码
- 微信： zbz801314





## 项目截图

![image-20220410152634702 ](README.assets/image-20220410152634702.png)



![image-20220410152708637](README.assets/image-20220410152708637.png) 



​     ![image-20220410152721333](README.assets/image-20220410152721333.png) 



![image-20220410152804291](README.assets/image-20220410152804291.png)

![image-20220410152843771](README.assets/image-20220410152843771.png)



## 安装教程

- 替换redis的地址与端口或密码
- 替换自己的七牛云空间和密钥
- 替换自己的mysql的用户与密码或地址



## 使用说明

**项目体验地址**

![image-20220527160548035](README.assets/image-20220527160548035.png)

![image-20220527160627127](README.assets/image-20220527160627127.png)

