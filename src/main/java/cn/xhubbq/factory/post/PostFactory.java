package cn.xhubbq.factory.post;

import cn.xhubbq.pojo.post.Post;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public interface PostFactory {
    Post make();
}
