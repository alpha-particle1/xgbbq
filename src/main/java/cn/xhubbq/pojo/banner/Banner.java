package cn.xhubbq.pojo.banner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/12/21
 * Description：小程序的轮播图
 */
@Document(collection = "banner")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Banner {
    @Id
    private String id;
    private String img_url;
    private String ctime;
}
