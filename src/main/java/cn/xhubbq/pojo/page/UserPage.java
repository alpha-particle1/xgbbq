package cn.xhubbq.pojo.page;

import cn.xhubbq.pojo.XgUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/27
 * Description：用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPage {
    private long totalPage;
    private long currentPage;
    private long totalCount;
    private long pageSize;
    private List<XgUser> users;

    public void setPageSize(int pageSize){
        this.pageSize=pageSize;
        totalPage=totalCount%pageSize!=0?totalCount/pageSize+1:totalCount/pageSize;
    }
}
