package cn.xhubbq.pojo.page;

import cn.xhubbq.pojo.Admin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/10
 * Description：admin list
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminPage {
    private int totalpage;//总页面
    private int currentpage;//当前页
    private int totalcount;//管理员总数
    private int pagesize;//页面大小
    private List<Admin> admins;

    //按某数量 分页多少
    public void setPagesize(int pageSize)
    {
        this.pagesize = pageSize;
        totalpage = totalcount % pageSize != 0 ? totalcount / pageSize + 1 : totalcount / pageSize;
    }

}
