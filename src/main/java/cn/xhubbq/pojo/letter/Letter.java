package cn.xhubbq.pojo.letter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/11/24
 * Description：写信，写给未来，写给过去
 */
@Document(collection = "letters")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Letter {
    @Id
    private String id;
    private Integer userid;
    private String nickname;

    private String username;//ji xin ren
    private String sendto;//shou xin ren
    private String email;//jie shou zhe de you xiang

    private String title;
    private String content;
    private String ctime;
    private String stime;
}
