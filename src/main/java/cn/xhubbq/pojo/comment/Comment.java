package cn.xhubbq.pojo.comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：墙的评论
 */
@Document(collection = "comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    /*
    * id：评论的id
    * postid：墙的id
    * uid：评论者idu2id:上级id
    * content：评论内容
    * stime:评论时间
    * */
    @Id
    private String id;
    private String postId;
    private Integer uId;
    private String uname;
    private String uheadpic;
    private int usex;
    private Integer u2Id;//上级回复
    private String content;
    private String stime;
}
