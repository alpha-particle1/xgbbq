package cn.xhubbq.pojo.post;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
@Document(collection = "Xyq")

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Xyq implements Post, Serializable {
    //id
    @Id
    private String id;
    //userid（Inteher）
    private Integer userid;
    private String uname;
    private String uheadpic;
    private int usex;
    //content （内容）
    private String content;
    //ctime 创建时间
    private String ctime;
    //status  状态（1为可显示，0为异常不可显示（查库时跳过此状态的数据））
    private int status;
    //类型
    private int type;
    //imgPath  图片的路径(涉及文件上传)：
    private String imgPath;
    //送礼物的的总数
    private Integer giftNum;
    //点赞数
    private Integer likeNum;
    //浏览量
    private long viewNum;

    @Override
    public int postType() {
        return Post.TYPE_XYQ;
    }
}
