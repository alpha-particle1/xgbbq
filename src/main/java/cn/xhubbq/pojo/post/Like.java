package cn.xhubbq.pojo.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/12/12
 * Description：
 */
@Document(collection = "like")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Like implements Serializable {
    private String postId;
    private Integer uId;
    private String cTime;
}
