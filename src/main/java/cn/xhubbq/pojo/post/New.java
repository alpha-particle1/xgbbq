package cn.xhubbq.pojo.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/12/16
 * Description：西华新闻
 */
@Document(collection = "news")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class New {
    @Id
    private String id;
    private String new_title;//新闻标题
    private String new_ctime;//发布时间
    private String new_author;//发布作者
    private String new_article;//新闻正文
    private String new_imgs;//图片地址 以 "|"分割

    @Override
    public String toString() {
        return "New{" +
                "new_title='" + new_title + '\'' +
                ", new_ctime='" + new_ctime + '\'' +
                ", new_author='" + new_author + '\'' +
                ", new_article='" + new_article + '\'' +
                ", new_imgs='" + new_imgs + '\'' +
                '}';
    }
}
