package cn.xhubbq.pojo.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/12/12
 * Description：礼物
 */
@Document(collection = "gift")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Gift implements Serializable {
    private String postId;
    private Integer uId;
    private String uName;
    private String uHead;
    private String sTime;
    private Integer giftNum;
}
