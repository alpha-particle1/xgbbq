package cn.xhubbq.pojo.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
@Document(collection = "Bbq")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bbq implements Post, Serializable {
    //id
    @Id
    private String id;
    //userid（Inteher）
    private Integer userid;
    private String uname;
    private String uheadpic;
    private int usex;
    //content （内容）
    private String content;
    //ctime 创建时间
    private String ctime;
    //status  状态（1为可显示，0为异常不可显示（查库时跳过此状态的数据））
    private int status;
    //类型
    private int type;
    //imgPath  图片的路径(涉及文件上传)：
    private String imgPath;
    //上传到 jar包根目录img路径下  img/userid/时间戳(年月)/帖子id/  ..... .png
    //返回时带上前缀 https://www.xhubbq.cn/+img/userid/时间戳(年月)/帖子id/  ..... .png
    //多图片 路径以 "|"分割

    //送礼物的的总数
    private Integer giftNum;//mongodb中存放  postid下的uid，还有stime，nickname，uheadpic，giftNums
    //点赞数
    private Integer likeNum;
    //浏览量
    private long viewNum;

    @Override
    public int postType() {
        return Post.TYPE_BBQ;
    }

}
