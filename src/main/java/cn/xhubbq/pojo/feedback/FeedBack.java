package cn.xhubbq.pojo.feedback;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
@Document(collection = "feedback")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FeedBack {
    @Id
    private String id;
    private int type;
    private String content;
    private String imgs;
    private int status;//是否已解决
}
