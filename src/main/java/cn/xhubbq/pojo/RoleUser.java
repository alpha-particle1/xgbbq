package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/11/9
 * Description：管理员权限
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleUser implements Serializable {
    private int userid;
    private int roleid;
}
