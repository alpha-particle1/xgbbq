package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/11/16
 * Description：
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XgUser implements Serializable {
    private Integer id;//id
    private String userid;//openid
    private String nickname;//昵称
    private int gender;//性别
    private String headpic;//头像
    private String telnum;//电话
    private int attention;//关注
    private Integer fans;//粉丝
    private int status;//状态
    private int identify;//身份
    private String ctime;//创建时间
    private String ltime;//最新登录时间
    private int version;//乐观锁
}
