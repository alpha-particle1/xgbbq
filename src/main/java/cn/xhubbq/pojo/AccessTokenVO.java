package cn.xhubbq.pojo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Author:甲粒子
 * Date: 2022/4/14
 * Description：封装微信请求的access_token
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenVO {
    private String access_token;
    private Integer expires_in;
}
