package cn.xhubbq.pojo.commodity;

import cn.xhubbq.pojo.XgUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommodityPage {
    private long totalPage;
    private long currentPage;
    private long totalCount;
    private long pageSize;
    private List<Commodity> commodities;

    public void setPageSize(int pageSize){
        this.pageSize=pageSize;
        totalPage=totalCount%pageSize!=0?totalCount/pageSize+1:totalCount/pageSize;
    }
}
