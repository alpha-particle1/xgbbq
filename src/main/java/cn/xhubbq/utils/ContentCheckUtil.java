package cn.xhubbq.utils;

import cn.xhubbq.pojo.AccessTokenVO;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/4/14
 * Description：微信发布功能违规检查工具类
 */
public class ContentCheckUtil {
    /**
     * 图片违规检测,对外提供,直接使用
     *
     * @param accessToken
     * @param file
     * @return
     */
    public static Boolean imgFilter(String accessToken, MultipartFile file) {
        String contentType = file.getContentType();
        System.out.println("文件类型："+contentType);
        return checkPic(file, accessToken, contentType);
    }
    /**
     * 文本违规检测,对外提供,直接使用
     *
     * @param accessToken
     * @param content
     * @return
     */
    public static Boolean cotentFilter(String accessToken,
                                       String content/*,
                                       int  version,
                                       String openid,
                                       int scene*/) {
        return checkContent(accessToken, content/*,version,openid,scene*/);
    }

    /**
     * 校验图片是否有敏感信息
     * 用的微信版本 1（已停止维护）
     * @param multipartFile
     * @return
     */
    private static Boolean checkPic(MultipartFile multipartFile, String accessToken, String contentType) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = null;
            HttpPost request = new HttpPost("https://api.weixin.qq.com/wxa/img_sec_check?access_token=" + accessToken);
            request.addHeader("Content-Type", "application/octet-stream");

            InputStream inputStream = multipartFile.getInputStream();
            byte[] byt = new byte[inputStream.available()];
            inputStream.read(byt);

            request.setEntity(new ByteArrayEntity(byt, ContentType.create(contentType)));
            response = httpclient.execute(request);
            HttpEntity httpEntity = response.getEntity();
            String result = EntityUtils.toString(httpEntity, "UTF-8");// 转成string
            JSONObject jso = JSONObject.parseObject(result);
            return getResult(jso);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 校验内容是否有敏感信息
     * @param accessToken
     * @param content
     * @return
     */
    private static Boolean checkContent(String accessToken,
                                        String content/*,
                                        int  version,
                                        String openid,
                                        int scene*/) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = null;
            HttpPost request = new HttpPost("https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + accessToken);
            request.addHeader("Content-Type", "application/json");

            Map<String, Object> map = new HashMap<>();
            //需检测的文本内容，文本字数的上限为2500字，需使用UTF-8编码
            map.put("content", content);
            /* 下面是2.0才需要
            //接口版本号，2.0版本为固定值2
            map.put("version", version);
            //用户的openid（用户需在近两小时访问过小程序）
            map.put("openid", openid);
            //场景枚举值（1 资料；2 评论；3 论坛；4 社交日志）
            map.put("scene", scene);
            */
            String body = JSONObject.toJSONString(map);

            request.setEntity(new StringEntity(body, ContentType.create("text/json", "UTF-8")));

            response = httpclient.execute(request);
            HttpEntity httpEntity = response.getEntity();
            String result = EntityUtils.toString(httpEntity, "UTF-8");// 转成string
            JSONObject jso = JSONObject.parseObject(result);
            System.out.println("文本检测结果:"+result);
            return getResult(jso);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 返回状态信息,可以修改为自己的逻辑
     * @param jso
     * @return
     */
    private static Boolean getResult(JSONObject jso) {
        Object errcode = jso.get("errcode");
        int errCode = (int) errcode;
        if (errCode == 0) {
            return true;
        } else if (errCode == 87014) {
            return false;
        } else {
            return false;
        }
    }

    /**
     * 获取小程序的 access_token
     * @return
     */
    public static String getAccessToken() {
        AccessTokenVO accessTokenVO = null;
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = null;
            //改成自己的appid和secret
            HttpGet request = new HttpGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxa9cd98b302c457ea&secret=28336de43f57fb2ebb4eb1765c5dc6c5");
            request.addHeader("Content-Type", "application/json");
            response = httpclient.execute(request);
            HttpEntity httpEntity = response.getEntity();
            String result = EntityUtils.toString(httpEntity, "UTF-8");// 转成string
            accessTokenVO = JSONObject.parseObject(result, AccessTokenVO.class);
            //返回token
            return accessTokenVO.getAccess_token();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
