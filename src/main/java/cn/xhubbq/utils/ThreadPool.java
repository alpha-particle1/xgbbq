package cn.xhubbq.utils;

import java.util.concurrent.*;
/**
 * Author:甲粒子
 * Date: 2022/5/3
 * Description：封装线程池
 */
public class ThreadPool {
    private static volatile ThreadPool threadPool;
    /**
     * java线程池
     */
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 最大线程数
     */
    private final static int MAX_POOL_COUNTS = 8;

    /**
     * 线程存活时间
     */
    private final static long ALIVETIME = 200L;

    /**
     * 核心线程数
     */
    private final static int CORE_POOL_SIZE = 8;

    /**
     * 线程池缓存队列
     */
    private BlockingQueue<Runnable> mWorkQueue = new ArrayBlockingQueue<>(CORE_POOL_SIZE*50);

    private ThreadFactory threadFactory = Executors.defaultThreadFactory();

    private ThreadPool() {
        //初始化线程池 核心线程数为8，最大线程数8，线程存活200L，线程队列mWorkQueue,
        threadPoolExecutor = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAX_POOL_COUNTS,
                ALIVETIME, TimeUnit.SECONDS,
                mWorkQueue, threadFactory);
    }

    public static ThreadPool getInstance() {
        if (threadPool == null) {
           synchronized (ThreadPool.class){
               if(threadPool == null){
                   threadPool = new ThreadPool();
               }
           }
        }
        return threadPool;
    }
    //为线程池添加任务
    public void addTask(Runnable runnable) {
        if (runnable == null) {
            throw new NullPointerException("addTask(Runnable runnable)传入参数为空");
        }
        if (threadPoolExecutor != null
                && threadPoolExecutor.getActiveCount() < MAX_POOL_COUNTS) {

            threadPoolExecutor.execute(runnable);
        }
    }
    public <T> void addTask(Callable<T> callable) {
        if (callable == null) {
            throw new NullPointerException("addTask(Callable callable)传入参数为空");
        }
        if (threadPoolExecutor != null
                && threadPoolExecutor.getActiveCount() < MAX_POOL_COUNTS) {

            threadPoolExecutor.submit(callable);
        }
    }
    public <T> void addTask(FutureTask<T> futureTask) {
        if (futureTask == null) {
            throw new NullPointerException("addTask(Callable callable)传入参数为空");
        }
        if (threadPoolExecutor != null
                && threadPoolExecutor.getActiveCount() < MAX_POOL_COUNTS) {

            threadPoolExecutor.submit(futureTask);
        }
    }

    public void stopThreadPool() {
        if (threadPoolExecutor != null) {
            threadPoolExecutor.shutdown();
            threadPoolExecutor = null;
        }
    }
}
