package cn.xhubbq.utils;

import com.alibaba.fastjson.JSONObject;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：七牛
 */
public class QiniuUtil2 { // 记得把2删除  记得把2删除 记得把2删除

    private static final String AK=""; // 自己七牛云上的配置
    private static final String SK="";

    private static final String URL_PREFIX="https://img.xhubbq.cn/";
    public static  Map<String,Object> qiniu(List<MultipartFile> files){
        Configuration configuration=new Configuration(Region.region2());
        UploadManager uploadManager=new UploadManager(configuration);
        String key=null;//默认不指定key的情况下，以文件内容的hash值作为文件名

        String imgs="";
        int count=0;
        Map<String,Object> map = new HashMap<>();
        try{
            Auth auth=Auth.create(AK,SK);
            String upToken=auth.uploadToken("xhubbq");
            Response response;
            for (MultipartFile file : files) {
                response=uploadManager.put(file.getBytes(),key,upToken);
                DefaultPutRet putRet = JSONObject.parseObject(response.bodyString(), DefaultPutRet.class);
                imgs +=(URL_PREFIX+putRet.key+"|");//注意最后多一个"|"
                ++count;
                //System.out.println(putRet.key);//文件名
                //System.out.println(putRet.hash);//key为空时key为此值(hash值)
                //System.out.println(response.body());
            }
            map.put("state",1);
            map.put("nums",count);
            map.put("imgs",imgs);
            return  map;

        }catch (Exception e){
            e.printStackTrace();
            map.put("state",0);
            map.put("nums",count);
            map.put("imgs",imgs);
            return map;
        }
    }

    public static  Map<String,Object> qiniu2(List<InputStream> files){
        Configuration configuration=new Configuration(Region.region2());
        UploadManager uploadManager=new UploadManager(configuration);
        String key=null;//默认不指定key的情况下，以文件内容的hash值作为文件名

        String imgs="";
        int count=0;
        Map<String,Object> map = new HashMap<>();

        byte [] buffer = new byte[1024*1024*2];// 单个文件不超过2MB
        try{
            Auth auth=Auth.create(AK,SK);
            String upToken=auth.uploadToken("xhubbq");
            Response response;
            for (InputStream file : files) {
                while ( file.read(buffer,0,buffer.length) != -1 ) {
                    response=uploadManager.put(buffer,key,upToken);
                    DefaultPutRet putRet = JSONObject.parseObject(response.bodyString(), DefaultPutRet.class);
                    imgs +=(URL_PREFIX+putRet.key+"|");//注意最后多一个"|"
                    ++count;
                }
                // 关闭 流 防止内存溢出 ==== xxx ===
                file.close();
            }
            map.put("state",1);
            map.put("nums",count);
            map.put("imgs",imgs);
            return  map;

        }catch (Exception e){
            e.printStackTrace();
            map.put("state",0);
            map.put("nums",count);
            map.put("imgs",imgs);
            return map;
        }
    }
}
