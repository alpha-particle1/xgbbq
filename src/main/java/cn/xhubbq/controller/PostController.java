package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.manager.AsyncManager;
import cn.xhubbq.manager.factory.AsyncFactory;
import cn.xhubbq.pojo.post.*;
import cn.xhubbq.pojo.post.page.*;
import cn.xhubbq.service.post.IPostService;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.utils.QiniuUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/10
 * Description：
 */
@RestController
public class PostController {
    @Autowired
    private IPostService postService;
    @Autowired
    private IUserService userService;

    private static final int PAGE_SIZE=10;

    //上传
    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/post/{content}/{type}/{nums}",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
    public String posts(@PathVariable String content, @PathVariable int type, @PathVariable int nums, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        if (content!=null){
            //上传文件
            List<MultipartFile> files;
            MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
            files=req.getFiles("imgs");

            boolean b = userService.checkContentAndImages(null, content);
            if(!b){
                map.put("state",0);
                map.put("info","不要搞莫名堂的...!");
                //System.out.println("文件校验...检测不通过");
                return JSONObject.toJSONString(map);
            }

            Map<String, Object> qiniu = QiniuUtil.qiniu(files);
            if((int)qiniu.get("state")==0){
                map.put("state",0);
                return  JSONObject.toJSONString(map);
            }
            if((int)qiniu.get("nums")!=nums){
                map.put("state",0);
                return  JSONObject.toJSONString(map);
            }
            String imgs = (String)qiniu.get("imgs");
            switch (type){
                case Post.TYPE_BBQ:{
                   //Bbq bbq= (Bbq)new BbqFactory().make();
                    Bbq bbq = JSONObject.parseObject(content, Bbq.class);
                    bbq.setImgPath(imgs);
                    int c = postService.savePostInMongoDb(bbq);
                    if(c==0){
                        map.put("state",0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_XYQ:{
                    //Xyq xyq=(Xyq)new XyqFactory().make();
                    Xyq xyq = JSONObject.parseObject(content, Xyq.class);
                    xyq.setImgPath(imgs);
                    int c = postService.savePostInMongoDb(xyq);
                    if(c==0){
                        map.put("state",0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_SWZL:{
                    //Swzl swzl=(Swzl) new SwzlFactory().make();
                    Swzl swzl = JSONObject.parseObject(content, Swzl.class);
                    swzl.setImgPath(imgs);
                    int c = postService.savePostInMongoDb(swzl);
                    if(c==0){
                        map.put("state",0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_TZSC:{
                    //Tzsc tzsc=(Tzsc) new TzscFactory().make();
                    Tzsc tzsc = JSONObject.parseObject(content, Tzsc.class);
                    tzsc.setImgPath(imgs);
                    int c = postService.savePostInMongoDb(tzsc);
                    if(c==0){
                        map.put("state",0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
            }

            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    //上传
    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/post/v2/{content}/{type}/{nums}",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
    public String postsV2(@PathVariable String content, @PathVariable int type, @PathVariable int nums, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
            //上传文件
            List<MultipartFile> files;
            List<InputStream> inputStreams = new ArrayList<>();
            MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
            files = req.getFiles("imgs");

            for (MultipartFile file : files) {
                try {
                    inputStreams.add(file.getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            AsyncManager.me().execute(AsyncFactory.posts(content,type,nums,inputStreams));
            map.put("state", 1);
            return JSONObject.toJSONString(map);
    }

/*
    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/post/{content}/{type}/{nums}",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
    public String posts(@PathVariable String content, @PathVariable int type, @PathVariable int nums, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        FutureTask<Integer> future = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                try{
                    postService.posts(content,type,nums,request);
                    return 1;
                }catch (Exception e){
                    return 0;
                }
            }
        });

        try{
            ThreadPool.getInstance().addTask(future);

            if ( future.get() == 1){
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }catch (Exception e){
            e.printStackTrace();
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }
*/


    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/posts/{type}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getPostPage(@PathVariable int type,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        switch (type){
            case Post.TYPE_BBQ:{
                BbqPage bbqPage=new BbqPage();
                bbqPage.setCurrentpage(curPage);
                long l = postService.countBbq();
                bbqPage.setTotalcount(l);
                bbqPage.setPagesize(PAGE_SIZE);

                List<Bbq> bbqs = postService.selectBbqBypage(curPage, PAGE_SIZE);
                map.put("state",1);
                map.put("data",bbqs);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_XYQ:{
                XyqPage xyqPage = new XyqPage();
                xyqPage.setCurrentpage(curPage);
                xyqPage.setTotalcount(postService.countXyq());
                xyqPage.setPagesize(PAGE_SIZE);
                List<Xyq> xyqs = postService.selectXyqBypage(curPage, PAGE_SIZE);
                map.put("state",1);
                map.put("data",xyqs);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_SWZL:{
                SwzlPage swzlPage = new SwzlPage();
                swzlPage.setCurrentpage(curPage);
                swzlPage.setTotalcount(postService.countSwzl());
                swzlPage.setPagesize(PAGE_SIZE);
                List<Swzl> swzls = postService.selectSwzlBypage(curPage, PAGE_SIZE);
                map.put("state",1);
                map.put("data",swzls);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_TZSC:{
                TzscPage tzscPage = new TzscPage();
                tzscPage.setCurrentpage(curPage);
                tzscPage.setTotalcount(postService.countTzsc());
                tzscPage.setPagesize(PAGE_SIZE);
                List<Tzsc> tzscs = postService.selectTzscBypage(curPage, PAGE_SIZE);
                map.put("state",1);
                map.put("data",tzscs);
                return JSONObject.toJSONString(map);
            }
        }//end switch
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }

    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping("/deletePost/{type}/{postId}")
    public String deletePost(@PathVariable int type,@PathVariable String postId){
        Map<String,Object> map = new HashMap<>();
        switch (type){
            case Post.TYPE_BBQ:{
                int i = postService.deleteFromBbq(postId);
                if(i==0){
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_XYQ:{
                int i = postService.deleteFromXyq(postId);
                if(i==0){
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_SWZL:{
                int i = postService.deleteFromSwzl(postId);
                if(i==0){
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_TZSC:{
                int i = postService.deleteFromTzsc(postId);
                if (i==0){
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
        }//end switch
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }

    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/GiftPHB/{postId}",produces = "application/json;charset=UTF-8")
    public String getGifPHB(@PathVariable String postId){
        Map<String,Object>map=new HashMap<>();

        List<Gift> tenUserInGift = postService.getTenUserInGift(postId);
        if (tenUserInGift.isEmpty()){
            map.put("state",0);
            return  JSONObject.toJSONString(map);
        }
        map.put("state",1);
        map.put("tuhao",tenUserInGift);
        return  JSONObject.toJSONString(map);
    }

    @InterfaceLimitRequest(count = 1,time = 5000)
    @ResponseBody
    @RequestMapping("/like/{likeinfo}/{type}")
    public String addLikeInfo(@PathVariable String likeinfo,@PathVariable int type){
        Map<String,Object>map=new HashMap<>();
        Like like = JSONObject.parseObject(likeinfo, Like.class);
        if(like.getPostId()!=null && like.getUId()!=null && type!=0){

            switch (type) {
                case Post.TYPE_BBQ: {
                    boolean b = postService.userIsInLike(like.getPostId(),like.getUId());
                    if(b){
                        //取消点赞
                        boolean b1 = postService.updateLikeInfoBBq(like.getPostId(), -1);
                        boolean b2 = postService.deleteLikeNote(like.getPostId(), like.getUId());
                        if(b1 && b2){
                            map.put("state",2);
                            return  JSONObject.toJSONString(map);
                        }
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    int i = postService.addLikeNote(like);
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    //点赞
                    boolean b1 = postService.updateLikeInfoBBq(like.getPostId(), 1);
                    if(b1){
                        map.put("state",1);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                case Post.TYPE_XYQ: {
                    boolean b = postService.userIsInLike(like.getPostId(),like.getUId());
                    if(b){
                        //取消点赞
                        boolean b1 = postService.updateLikeInfoXyq(like.getPostId(), -1);
                        boolean b2 = postService.deleteLikeNote(like.getPostId(), like.getUId());
                        if(b1 && b2){
                            map.put("state",2);
                            return  JSONObject.toJSONString(map);
                        }
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    int i = postService.addLikeNote(like);
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    //点赞
                    boolean b1 = postService.updateLikeInfoXyq(like.getPostId(), 1);
                    if(b1){
                        map.put("state",1);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                case Post.TYPE_SWZL: {
                    boolean b = postService.userIsInLike(like.getPostId(),like.getUId());
                    if(b){
                        //取消点赞
                        boolean b1 = postService.updateLikeInfoSwzl(like.getPostId(), -1);
                        boolean b2 = postService.deleteLikeNote(like.getPostId(), like.getUId());
                        if(b1 && b2){
                            map.put("state",2);
                            return  JSONObject.toJSONString(map);
                        }
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    int i = postService.addLikeNote(like);
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    //点赞
                    boolean b1 = postService.updateLikeInfoSwzl(like.getPostId(), 1);
                    if(b1){
                        map.put("state",1);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
                case Post.TYPE_TZSC: {
                    boolean b = postService.userIsInLike(like.getPostId(),like.getUId());
                    if(b){
                        //取消点赞
                        boolean b1 = postService.updateLikeInfoTzsc(like.getPostId(), -1);
                        boolean b2 = postService.deleteLikeNote(like.getPostId(), like.getUId());
                        if(b1 && b2){
                            map.put("state",2);
                            return  JSONObject.toJSONString(map);
                        }
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    int i = postService.addLikeNote(like);
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    //点赞
                    boolean b1 = postService.updateLikeInfoTzsc(like.getPostId(), 1);
                    if(b1){
                        map.put("state",1);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",0);
                    return  JSONObject.toJSONString(map);
                }
            }
        }//end if
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }

    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping("/GiftTo/{GiftInfo}")
    public String addGiftInfo(@PathVariable String GiftInfo){
        Map<String,Object>map=new HashMap<>();
        Gift gift = JSONObject.parseObject(GiftInfo, Gift.class);
        if(gift.getPostId()!=null && gift.getUId()!=null && gift.getGiftNum()!=0){
            //查询余额
            Integer sx = userService.selectShengxia(gift.getUId());
            //余额满足花费的
            if(sx>=gift.getGiftNum()){
                //是否送过礼物
                boolean b = postService.userIsInGift(gift.getPostId(), gift.getUId());
                //是的
                if (b){
                    int i = postService.updateGiftInfo(gift.getPostId(), gift.getUId(), gift.getGiftNum());
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    Integer use = userService.useMoney(gift.getUId(), gift.getGiftNum());
                    if(use==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return  JSONObject.toJSONString(map);
                }else {//没有送过
                    int i = postService.addGiftNote(gift);
                    if(i==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    Integer use = userService.useMoney(gift.getUId(), gift.getGiftNum());
                    if(use==0){
                        map.put("state",0);
                        return  JSONObject.toJSONString(map);
                    }
                    map.put("state",1);
                    return  JSONObject.toJSONString(map);
                }
            }//end if sx>=...
            map.put("state",-3);//余额不足
            return  JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }


    @ResponseBody
    @RequestMapping("/viewnum/{type}/{postId}")
    @InterfaceLimitRequest(time = 1000*60*10,count = 1)//10分钟只能访问一次
    public String addViewNum(@PathVariable int type,@PathVariable String postId){
        Map<String,Object> map = new HashMap<>();
        if(postId!=null && type!=0) {
            switch (type) {
                case Post.TYPE_BBQ: {
                    boolean b = postService.updateViewNumsInBBq(postId);
                    if (!b) {
                        map.put("state", 0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state", 1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_XYQ: {
                    boolean b = postService.updateViewNumsInXyq(postId);
                    if (!b) {
                        map.put("state", 0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state", 1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_SWZL: {
                    boolean b = postService.updateViewNumsInSwzl(postId);
                    if (!b) {
                        map.put("state", 0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state", 1);
                    return JSONObject.toJSONString(map);
                }
                case Post.TYPE_TZSC: {
                    boolean b = postService.updateViewNumsInTzsc(postId);
                    if (!b) {
                        map.put("state", 0);
                        return JSONObject.toJSONString(map);
                    }
                    map.put("state", 1);
                    return JSONObject.toJSONString(map);
                }
            }
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }


    //西华新闻
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/xhuNews/{curPage}",produces = "application/json;charset=UTF-8")
    public String getXhuNews(@PathVariable long curPage){
        if(curPage==0){
            curPage=1;
        }
        Map<String,Object> map = new HashMap<>();
        NewPage newPage = new NewPage();
        newPage.setCurrentpage(curPage);
        long l = postService.countNew();
        newPage.setTotalcount(l);
        newPage.setPagesize(5);
        List<New> news = postService.selectNewBypage(curPage, 5);
        map.put("state",1);
        map.put("data",news);
        return JSONObject.toJSONString(map);
    }

    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping(value = "/selfposts/{uid}/{type}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getPostPageByUid(@PathVariable Integer uid,@PathVariable int type,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        switch (type){
            case Post.TYPE_BBQ:{
                BbqPage bbqPage=new BbqPage();
                bbqPage.setCurrentpage(curPage);
                long l = postService.countBbqByUserId(uid);
                bbqPage.setTotalcount(l);
                bbqPage.setPagesize(PAGE_SIZE);

                List<Bbq> bbqs = postService.selectBbqBypageByUserId(curPage, PAGE_SIZE,uid);
                map.put("state",1);
                map.put("data",bbqs);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_XYQ:{
                XyqPage xyqPage = new XyqPage();
                xyqPage.setCurrentpage(curPage);
                xyqPage.setTotalcount(postService.countXyqByUserId(uid));
                xyqPage.setPagesize(PAGE_SIZE);
                List<Xyq> xyqs = postService.selectXyqBypageByUserId(curPage, PAGE_SIZE,uid);
                map.put("state",1);
                map.put("data",xyqs);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_SWZL:{
                SwzlPage swzlPage = new SwzlPage();
                swzlPage.setCurrentpage(curPage);
                swzlPage.setTotalcount(postService.countSwzlByUserId(uid));
                swzlPage.setPagesize(PAGE_SIZE);
                List<Swzl> swzls = postService.selectSwzlBypageByUserId(curPage, PAGE_SIZE,uid);
                map.put("state",1);
                map.put("data",swzls);
                return JSONObject.toJSONString(map);
            }
            case Post.TYPE_TZSC:{
                TzscPage tzscPage = new TzscPage();
                tzscPage.setCurrentpage(curPage);
                tzscPage.setTotalcount(postService.countTzscByUserId(uid));
                tzscPage.setPagesize(PAGE_SIZE);
                List<Tzsc> tzscs = postService.selectTzscBypageByUserId(curPage, PAGE_SIZE,uid);
                map.put("state",1);
                map.put("data",tzscs);
                return JSONObject.toJSONString(map);
            }
        }//end switch
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }


}
