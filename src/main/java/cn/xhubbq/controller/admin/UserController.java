package cn.xhubbq.controller.admin;

import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.service.xguser.IUserService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/19
 * Description：
 */
@RestController
public class UserController {
    @Autowired
    private IUserService userService;
    //余额
    @ResponseBody
    @RequestMapping(value = "/getSx/{userid}")
    public String getSx(@PathVariable Integer userid){
        Map<String,Object> map = new HashMap<>();
        try{
            if(userid!=0){
                Integer money = userService.selectShengxia(userid);
                map.put("state",1);
                map.put("money",money);
                return  JSONObject.toJSONString(map);
            }
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }catch (Exception e){
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }

    //用户信息
    @ResponseBody
    @RequestMapping(value = "/usersInfo/ids",produces = "application/json;charset=UTF-8")
    public String select(@RequestBody List<Integer> ids){
        Map<String,Object> map = new HashMap<>();
        try{
            if (ids!=null){
                List<XgUser> xgUsers = userService.selectUserInfoByIds(ids);
                map.put("state",1);
                map.put("data",xgUsers);
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }catch (Exception e){
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }
}
