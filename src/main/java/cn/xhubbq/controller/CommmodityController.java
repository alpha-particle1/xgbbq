package cn.xhubbq.controller;
import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.commodity.Commodity;
import cn.xhubbq.pojo.commodity.CommodityFavorites;
import cn.xhubbq.pojo.commodity.CommodityPage;
import cn.xhubbq.service.commodity.ICommodityService;
import cn.xhubbq.utils.QiniuUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：
 */
@RestController
public class CommmodityController {
    @Autowired
    private ICommodityService commodityService;

    //测试
    //@ResponseBody
    //@InterfaceLimitRequest(count = 1)
    //@RequestMapping(value = "/testCommodity",produces = "application/json;charset=UTF-8")
    public String  testCommodity() {
        Map<String, Object> map = new HashMap<>();
        //Commodity commodity1 = JSONObject.parseObject(commodity, Commodity.class);
        Commodity commodity = new Commodity();
        commodity.setId("123456");
        commodity.setUpTime("2022-1-23 9:50");
        commodity.setDescription("裤子");
        commodity.setPrice(90.0);
        commodity.setPrice0(199.0);
        commodity.setTitle("卖家1");
        commodity.setUserId(3);
        System.out.println(JSONObject.toJSONString(commodity));
        return JSONObject.toJSONString(commodity);
    }

    //上架商品
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/add/{commodity}/{nums}",produces = "application/json;charset=UTF-8")
    public String  addCommodity(@PathVariable String commodity,@PathVariable int nums, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        String imgs="";
        Commodity commodity1 = JSONObject.parseObject(commodity, Commodity.class);
        if(commodity1==null){
            map.put("state",0);
            map.put("error","请求体为空");
            return JSONObject.toJSONString(map);
        }else{
           if(nums != 0){
               //上传文件
               MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
               List<MultipartFile> files=req.getFiles("imgs");
               Map<String, Object> qiniu = QiniuUtil.qiniu(files);
               if((int)qiniu.get("state")==0){
                       map.put("state",0);
                       map.put("error","上传失败");
                       return  JSONObject.toJSONString(map);
               }
               if((int)qiniu.get("nums")!=nums){
                   map.put("state",0);
                   map.put("error","部分图片上传出错");
                   return  JSONObject.toJSONString(map);
               }
               imgs = (String)qiniu.get("imgs");
           }
            commodity1.setImgs(imgs);
            int flag = commodityService.upCommodity(commodity1);
            if(flag==1){
                map.put("state",1);
                map.put("success","添加成功");
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            map.put("error","添加出错");
            return JSONObject.toJSONString(map);
        }
    }

    //删除商品
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/delete/{id}",produces = "application/json;charset=UTF-8")
    public String deleteById(@PathVariable String id){
        Map<String,Object> map = new HashMap<>();
        int flag = commodityService.deleteCommodity(id);
        if(flag == 0){
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
        map.put("state",1);
        return JSONObject.toJSONString(map);
    }

    private final static int pageSize = 8;

    //拉取商品
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/get/{curPage}",produces = "application/json;charset=UTF-8")
    public String getCommodityPage0(@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        if(curPage == 0) curPage =1;
        CommodityPage commodityPage=new CommodityPage();
        commodityPage.setCurrentPage(curPage); ;
        long l = commodityService.countCommodity();
        commodityPage.setTotalCount(l);
        commodityPage.setPageSize(pageSize);

        List<Commodity> commodities = commodityService.getCommodityPage(curPage,pageSize);
        commodityPage.setCommodities(commodities);
        map.put("state",1);
        map.put("data",commodityPage);
        return JSONObject.toJSONString(map);
    }

    //拉取商品
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/get/{type}/{sort}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getCommodityPage1(@PathVariable int type,@PathVariable int sort,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        if(curPage == 0) curPage =1;
        CommodityPage commodityPage=new CommodityPage();
        commodityPage.setCurrentPage(curPage); ;
        long l = commodityService.countCommodity(type);
        commodityPage.setTotalCount(l);
        commodityPage.setPageSize(pageSize);

        List<Commodity> commodities = commodityService.getCommodityPage1(type,sort,curPage,pageSize);
        commodityPage.setCommodities(commodities);
        map.put("state",1);
        map.put("data",commodityPage);
        return JSONObject.toJSONString(map);
    }

    //拉取商品
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/getU/{userId}/{type}/{sort}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getCommodityPage_id(@PathVariable Integer userId,@PathVariable int type,@PathVariable int sort,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        if(curPage == 0) curPage =1;
        CommodityPage commodityPage=new CommodityPage();
        commodityPage.setCurrentPage(curPage); ;
        long l = commodityService.countCommodity();
        commodityPage.setTotalCount(l);
        commodityPage.setPageSize(pageSize);

        List<Commodity> commodities = commodityService.getCommodityByPage_id(userId,type,sort,curPage,pageSize);
        commodityPage.setCommodities(commodities);
        map.put("state",1);
        map.put("data",commodityPage);
        return JSONObject.toJSONString(map);
    }


    //查询
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/get/{key}/{type}/{sort}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getCommodityPage(@PathVariable String key,@PathVariable int type,@PathVariable int sort,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        if(curPage == 0) curPage =1;
        CommodityPage commodityPage=new CommodityPage();
        commodityPage.setCurrentPage(curPage);
        long l = commodityService.countCommodity(key,type);
        commodityPage.setTotalCount(l);
        commodityPage.setPageSize(pageSize);
        List<Commodity> commodities = commodityService.getCommodityPage(key,type,sort,curPage,pageSize);
       commodityPage.setCommodities(commodities);
        map.put("state",1);
        map.put("data",commodityPage);
        return JSONObject.toJSONString(map);
    }


    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodity/getbyId/{id}",produces = "application/json;charset=UTF-8")
    public String getCommodityById(@PathVariable String id){
        Map<String,Object> map = new HashMap<>();
        Commodity commodity = commodityService.getCommodityById(id);
        map.put("commodity",commodity);
        return JSONObject.toJSONString(map);
    }

    //收藏
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodityFavorites/add/{fav}",produces = "application/json;charset=UTF-8")
    public String addFav(@PathVariable String fav){
        Map<String,Object> map = new HashMap<>();
        CommodityFavorites commodityFavorite = JSONObject.parseObject(fav, CommodityFavorites.class);
        if (commodityFavorite != null){
            int flag = commodityService.addFavorites(commodityFavorite);
            if(flag == 0){
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            map.put("state",1);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    //删除收藏
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodityFavorites/delete/{id}/{userId}",produces = "application/json;charset=UTF-8")
    public String deleteFavoritesById(@PathVariable String id,@PathVariable Integer userId){
        Map<String,Object> map = new HashMap<>();
        int flag = commodityService.deleteFromFavorites(userId,id);
        if(flag == 0){
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
        map.put("state",1);
        return JSONObject.toJSONString(map);
    }

    //分页拉取收藏页面
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/commodityFav/get/{userId}/{curPage}",produces = "application/json;charset=UTF-8")
    public String getCommodityFavoritesPage(@PathVariable Integer userId,@PathVariable long curPage){
        Map<String,Object> map = new HashMap<>();
        if(curPage == 0) curPage =1L;
        CommodityPage commodityPage=new CommodityPage();
        commodityPage.setCurrentPage(curPage); ;
        long l = commodityService.countFavorites(userId);
        commodityPage.setTotalCount(l);
        commodityPage.setPageSize(50);
        List<Commodity> commodities = commodityService.getFavoritesByPage(userId,curPage,50);
        commodityPage.setCommodities(commodities);
        map.put("state",1);
        map.put("data",commodityPage);
        return JSONObject.toJSONString(map);
    }

}
