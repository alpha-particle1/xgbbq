package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.comment.Comment;
import cn.xhubbq.pojo.comment.page.CommentPage;
import cn.xhubbq.service.comment.ICommentService;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.utils.RespStateUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
@RestController
public class CommentController {
    @Autowired
    private ICommentService commentService;
    @Autowired
    IUserService userService;

    private static final int PAGE_SIZE=16;
    //评论
    @ResponseBody
    @RequestMapping(value = "/comment/{comment}",method = RequestMethod.POST)
    @InterfaceLimitRequest
    public String comment(@PathVariable String comment){
        Map<String,Object> map = new HashMap<>();

        if(comment!=null){
            Comment comment1 = JSONObject.parseObject(comment, Comment.class);
            System.out.println(comment1.getContent());
            boolean b = userService.checkContentAndImages(null, comment1.getContent());
            if(!b){
                map.put("state",0);
                map.put("info","不要搞莫名堂的...!");
                //System.out.println("检测不通过");
                return JSONObject.toJSONString(map);
            }

            if(comment1!=null){
                int i = commentService.comment(comment1);
                if(i==0){
                    map.put("state",0);
                    return JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    //拉取评论
    @ResponseBody
    @RequestMapping(value = "/comments/{postId}/{curPage}",method = RequestMethod.POST)
    @InterfaceLimitRequest
    public Object commentPage(@PathVariable String postId,@PathVariable int curPage){
        try{
            CommentPage commentPage=new CommentPage();
            commentPage.setCurrentpage(curPage);
            commentPage.setTotalcount(commentService.countCommentByPostId(postId));
            commentPage.setPagesize(PAGE_SIZE);
            List<Comment> comments = commentService.getAllCommentByPagePostId(postId, curPage, PAGE_SIZE);
            commentPage.setComments(comments);
            return commentPage;
        }catch (Exception e){
            return RespStateUtil.state0(new HashMap<>());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/comments0/{postId}",method = RequestMethod.POST)
    @InterfaceLimitRequest
    public Object commentPage(@PathVariable String postId){
        List<Comment> comments = commentService.getAllCommentByPostId(postId);
        return comments;
    }

    //删除评论
    @ResponseBody
    @RequestMapping("/deleteComment/{id}")
    @InterfaceLimitRequest
    public Object deleteComment(@PathVariable String id){
        Map<String,Object> map = new HashMap<>();
        int i = commentService.deleteComment(id);
        if (i==0){
            map.put("state",0);
            return  map;
        }
        map.put("state",1);
        return  map;
    }
}
