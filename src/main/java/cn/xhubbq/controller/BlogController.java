package cn.xhubbq.controller;

import cn.xhubbq.pojo.blog.Blog;
import cn.xhubbq.pojo.page.BlogPage;
import cn.xhubbq.service.blog.IBlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/25
 * Description：
 */
@Controller
public class BlogController {
    @Autowired
    private IBlogService blogService;
    private static final int PAGE_SIZE=9;
    @RequestMapping("/upBlog")
    public String upBlog(){
        return "blog/index";
    }

    @RequestMapping("/blogs/{upAuthor}/{curPage}")
    public String blogs(@PathVariable String upAuthor, @PathVariable long curPage, Model model){
        try{
            BlogPage blogPage=new BlogPage();
            blogPage.setCurrentpage(curPage);
            long totalcount = blogService.countBlog(upAuthor);
            if (totalcount==0)
                return "redirect:/404";
            blogPage.setTotalcount(totalcount);
            blogPage.setPagesize(PAGE_SIZE);
            List<Blog> blogList=blogService.selectBlogPageByAuthor(upAuthor,curPage,PAGE_SIZE);
            blogPage.setBlogs(blogList);
            model.addAttribute("blogPage",blogPage);
            return "blog/blogs";
        }
        catch (Exception  e){
            return "redirect:/upBlog";
        }
    }

}
