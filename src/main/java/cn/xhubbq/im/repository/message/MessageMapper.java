package cn.xhubbq.im.repository.message;

import cn.xhubbq.im.entity.SocketMessage;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/18
 * Description：消息处理
 */
@Mapper
@Repository
public interface MessageMapper {

    //查看此人是否存在并且状态正常
    @Select("select count(1) from xg_user where id=#{id} and status=1")
    int userIsExistedAndYeah(Integer id);
    //持久化未读消息
    @Insert("insert into wdxx(user_id,chat_id,message_type,message,stime) values(#{userId},#{chatId},#{messageType},#{message},#{stime})")
    int addUnreadMessage(SocketMessage socketMessage);

    //删除已读
    @Delete("delete from wdxx where user_id=#{userId}")
    int deleteUnreadMessage(Integer userId);

    //拉取未读消息
    @Select("select * from wdxx where user_id=#{userId}")
    List<SocketMessage> selectAllUnreadMessages(Integer userId);
}
