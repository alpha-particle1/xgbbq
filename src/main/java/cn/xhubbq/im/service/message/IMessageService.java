package cn.xhubbq.im.service.message;

import cn.xhubbq.im.entity.SocketMessage;
import com.mongodb.client.result.DeleteResult;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/18
 * Description：未读消息处理服务
 */
public interface IMessageService {
    int addUnreadMessage(SocketMessage socketMessage);
    //删除已读
    int deleteUnreadMessage(Integer userId);

    int userIsExistedAndYeah(Integer id);

    //拉取未读消息
    List<SocketMessage> selectAllUnreadMessages(Integer userId);

    //对方不在线  消息存入mongoDB
    SocketMessage addMessageInMongo(SocketMessage wdxx);
    //从Mongo中删除消息
    DeleteResult deleteMessage(Integer chatId, Integer userId);
    //拉取未读消息
    List<SocketMessage> getMessages(Integer chatId, Integer userId);
    //拉取chatId的未读消息
    List<SocketMessage> getAllMessages(Integer chatId);
}
