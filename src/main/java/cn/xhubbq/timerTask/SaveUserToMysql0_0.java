package cn.xhubbq.timerTask;

import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.service.xguser.IWxLoginService;
import cn.xhubbq.utils.ContentCheckUtil;
import cn.xhubbq.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/23
 * Description：定时将redis中的数据存入mysql
 */
@Component
public class SaveUserToMysql0_0 {
    @Autowired
    private IWxLoginService wxLoginService;
    @Autowired
    RedisUtil redis;

    @Scheduled(fixedDelay = 1000*60*60*1)//1个小时执行一次  运行时执行一次
    public void updateToken(){
        try {
            String access_token = ContentCheckUtil.getAccessToken();
            redis.set( "access_token",access_token,7000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    //@Scheduled(cron="0 0 0 * * ?")
    public void testTask(){
        System.out.println("定时任务");
    }

    //@Scheduled(cron="0 1 15 * * ?")  测试下面 在 0：0：0执行的代码
    public void SaveUsers2(){
        List<Object> newUsers = wxLoginService.takeNewUsersFromRedis("test");
        if(newUsers!=null){
            for (Object newUser : newUsers) {
                int cnt = wxLoginService.addUser((XgUser) newUser);
                if(cnt!=0){
                    wxLoginService.deleteNewUserInRedis("test", ((XgUser) newUser).getUserid());
                }
            }
        }

    }

    //@Scheduled(cron="0 0 0 * * ?")
    public void SaveUsers(){
        List<Object> newUsers = wxLoginService.takeNewUsersFromRedis("newUsers");
        if(newUsers!=null){
            for (Object newUser : newUsers) {
                int cnt = wxLoginService.addUser((XgUser) newUser);
                if(cnt!=0){
                    wxLoginService.deleteNewUserInRedis("newUsers", ((XgUser) newUser).getUserid());
                }
            }
        }

    }




}
