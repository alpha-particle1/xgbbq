package cn.xhubbq.service.post;

import cn.xhubbq.pojo.post.*;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.utils.QiniuUtil;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
@Service
public class PostServiceImpl implements  IPostService{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private IUserService userService;
    @Override
    public int savePostInMongoDb(Post post) {
       if(post instanceof Bbq){
           Post insert = mongoTemplate.insert(post);
           //System.out.println("bbq");
           if(insert==null)
               return 0;
           return 1;
       }
       else if(post instanceof Xyq){
           Post insert = mongoTemplate.insert(post);
           //System.out.println("Xyq");
           if(insert==null)
               return 0;
           return 1;
       }
       else if(post instanceof Swzl){
           Post insert = mongoTemplate.insert(post);
           //System.out.println("Swzl");
           if(insert==null)
               return 0;
           return 1;
       }
       else if(post instanceof Tzsc){
           Post insert = mongoTemplate.insert(post);
           //System.out.println("Tzsc");
           if(insert==null)
               return 0;
           return 1;
       }
        return 0;
    }

    @Override
    public long countBbq() {
        return mongoTemplate.count(new Query(),Bbq.class);
    }

    @Override
    public long countXyq() {
        return mongoTemplate.count(new Query(),Xyq.class);
    }

    @Override
    public long countSwzl() {
        return mongoTemplate.count(new Query(),Swzl.class);
    }

    @Override
    public long countTzsc() {
        return mongoTemplate.count(new Query(),Tzsc.class);
    }

    @Override
    public List<Bbq> selectBbqBypage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Bbq.class);
    }

    @Override
    public List<Xyq> selectXyqBypage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Xyq.class);
    }

    @Override
    public List<Swzl> selectSwzlBypage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Swzl.class);
    }

    @Override
    public List<Tzsc> selectTzscBypage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Tzsc.class);
    }

    @Override
    public int deleteFromBbq(String postId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        DeleteResult remove = mongoTemplate.remove(query, Bbq.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public int deleteFromXyq(String postId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        DeleteResult remove = mongoTemplate.remove(query, Xyq.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public int deleteFromSwzl(String postId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        DeleteResult remove = mongoTemplate.remove(query, Swzl.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public int deleteFromTzsc(String postId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        DeleteResult remove = mongoTemplate.remove(query, Tzsc.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public int addLikeNote(Like like) {
        Like insert = mongoTemplate.insert(like);
        if(insert==null)
            return 0;
        return 1;
    }

    @Override
    public boolean deleteLikeNote(String postId, Integer uid) {
        Query query = new Query();
        query.addCriteria(Criteria.where("postId").is(postId).and("uId").is(uid));
        Update update=new Update();
        DeleteResult remove = mongoTemplate.remove(query, "like");
        if(remove!=null && remove.getDeletedCount()!=0){
            return true;
        }
        return false;
    }

    @Override
    public boolean userIsInGift(String postId, Integer uid) {
        List<Gift> uId = mongoTemplate.find(new Query(Criteria.where("uId").is(uid).and("postId").is(postId)), Gift.class);
        if(uId.isEmpty())
            return false;
        return true;
    }

    @Override
    public boolean userIsInLike(String postId,Integer uid) {
        List<Like> uId = mongoTemplate.find(new Query(Criteria.where("uId").is(uid).and("postId").is(postId)), Like.class);
        if(uId.isEmpty())
            return false;
        return true;
    }

    @Override
    public int addGiftNote(Gift gift) {
        Gift insert = mongoTemplate.insert(gift);
        if(insert==null)
            return 0;
        return 1;
    }

    @Override
    public int updateGiftInfo(String postId, Integer uid, Integer giftNum) {
        Query query=new Query();
        query.addCriteria(Criteria.where("postId").is(postId).and("uId").is(uid));
        Update update = new Update();
        update.inc("giftNum",giftNum);//追加
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Gift.class);
        if(updateResult==null){
            return 0;
        }
        if(updateResult.getModifiedCount()==0){
            return 0;
        }
        return 1;
    }

    @Override
    public List<Gift> getTenUserInGift(String postId) {
        //Sort sort = new Sort(Sort.Direction.DESC, "giftNum");
        Query query=new Query();
        query.addCriteria(Criteria.where("postId").is(postId));
        Sort sort=Sort.by(Sort.Direction.DESC, "giftNum");
        List<Gift> gifts = mongoTemplate.find(query.with(sort).limit(10), Gift.class);
        return gifts ;
    }

    @Override
    public boolean updateViewNumsInBBq(String postId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("viewNum",1);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Bbq.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateViewNumsInXyq(String postId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("viewNum",1);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Xyq.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateViewNumsInSwzl(String postId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("viewNum",1);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Swzl.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateViewNumsInTzsc(String postId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("viewNum",1);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Tzsc.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateLikeInfoBBq(String postId, int num) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("likeNum",num);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Bbq.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateLikeInfoXyq(String postId, int num) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("likeNum",num);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Xyq.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateLikeInfoSwzl(String postId, int num) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("likeNum",num);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Swzl.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public boolean updateLikeInfoTzsc(String postId, int num) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(postId));
        Update update=new Update().inc("likeNum",num);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Tzsc.class);
        if(updateResult==null){
            return false;
        }
        if(updateResult.getModifiedCount()==0){
            return false;
        }
        return true;
    }

    @Override
    public long countNew() {
        return mongoTemplate.count(new Query(),New.class);
    }

    @Override
    public List<New> selectNewBypage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"_id");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),New.class);
    }

    @Override
    public long countBbqByUserId(Integer uid) {
        return mongoTemplate.count(new Query().addCriteria(Criteria.where("userid").is(uid)),Bbq.class);
    }

    @Override
    public long countXyqByUserId(Integer uid) {
        return mongoTemplate.count(new Query().addCriteria(Criteria.where("userid").is(uid)),Xyq.class);
    }

    @Override
    public long countSwzlByUserId(Integer uid) {
        return mongoTemplate.count(new Query().addCriteria(Criteria.where("userid").is(uid)),Swzl.class);
    }

    @Override
    public long countTzscByUserId(Integer uid) {
        return mongoTemplate.count(new Query().addCriteria(Criteria.where("userid").is(uid)),Tzsc.class);
    }

    @Override
    public List<Bbq> selectBbqBypageByUserId(long curPage, int pageSize, Integer uid) {
        Query query=new Query();
        query.addCriteria(Criteria.where("userid").is(uid));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Bbq.class);
    }

    @Override
    public List<Xyq> selectXyqBypageByUserId(long curPage, int pageSize, Integer uid) {
        Query query=new Query();
        query.addCriteria(Criteria.where("userid").is(uid));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Xyq.class);
    }

    @Override
    public List<Swzl> selectSwzlBypageByUserId(long curPage, int pageSize, Integer uid) {
        Query query=new Query();
        query.addCriteria(Criteria.where("userid").is(uid));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Swzl.class);
    }

    @Override
    public List<Tzsc> selectTzscBypageByUserId(long curPage, int pageSize, Integer uid) {
        Query query=new Query();
        query.addCriteria(Criteria.where("userid").is(uid));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Tzsc.class);
    }

    @Override
    public List<Swzl> selectSwzl10() {
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(new Query().with(sort).limit(10),Swzl.class);
    }

    @Override
    public long countSwzl(String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        return mongoTemplate.count(query,Swzl.class);
    }

    @Override
    public List<Swzl> selectSwzlBypage(long curPage, int pageSize, String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Swzl.class);
    }

    @Override
    public long countBbq(String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        return mongoTemplate.count(query,Bbq.class);
    }

    @Override
    public List<Bbq> selectBbqBypage(long curPage, int pageSize, String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Bbq.class);
    }


    @Override
    public long countXyq(String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));

        return mongoTemplate.count(query,Xyq.class);
    }

    @Override
    public List<Xyq> selectXyqBypage(long curPage, int pageSize, String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Xyq.class);
    }

    @Override
    public long countTzsc(String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        return mongoTemplate.count(query,Tzsc.class);
    }

    @Override
    public List<Tzsc> selectTzscBypage(long curPage, int pageSize, String like) {
        Pattern pattern = Pattern.compile("^.*"+like+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(Criteria.where("content").regex(pattern));
        Sort sort=Sort.by(Sort.Direction.DESC,"ctime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize),Tzsc.class);
    }

    @Override
    public void posts( String content, int type, int nums, HttpServletRequest request){
        if (content!=null){
            //上传文件到七牛云
            List<MultipartFile> files;
            MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
            files=req.getFiles("imgs");

            boolean b = userService.checkContentAndImages(null, content);
            if(!b){
                //System.out.println("文件校验...检测不通过");
                throw new RuntimeException("文件校验...检测不通过");
            }
            System.out.println("files are empty: "+files.isEmpty());
            System.out.println("files are empty: "+files.size());
            Map<String, Object> qiniu = QiniuUtil.qiniu(files);
            if((int)qiniu.get("state")==0){
                throw new RuntimeException("文件上传...state == 0");
            }
            if((int)qiniu.get("nums")!=nums){
                throw new RuntimeException("文件上传...nums != nums");
            }
            String imgs = (String)qiniu.get("imgs");
            switch (type){
                case Post.TYPE_BBQ:{
                    //Bbq bbq= (Bbq)new BbqFactory().make();
                    Bbq bbq = JSONObject.parseObject(content, Bbq.class);
                    bbq.setImgPath(imgs);
                    int c = savePostInMongoDb(bbq);
                    if(c==0){
                        throw new RuntimeException("文件上传...保存失败");
                    }
                }
                case Post.TYPE_XYQ:{
                    //Xyq xyq=(Xyq)new XyqFactory().make();
                    Xyq xyq = JSONObject.parseObject(content, Xyq.class);
                    xyq.setImgPath(imgs);
                    int c = savePostInMongoDb(xyq);
                    if(c==0){
                        throw new RuntimeException("文件上传...保存失败");
                    }
                }
                case Post.TYPE_SWZL:{
                    //Swzl swzl=(Swzl) new SwzlFactory().make();
                    Swzl swzl = JSONObject.parseObject(content, Swzl.class);
                    swzl.setImgPath(imgs);
                    int c = savePostInMongoDb(swzl);
                    if(c==0){
                        throw new RuntimeException("文件上传...保存失败");
                    }
                }
                case Post.TYPE_TZSC:{
                    //Tzsc tzsc=(Tzsc) new TzscFactory().make();
                    Tzsc tzsc = JSONObject.parseObject(content, Tzsc.class);
                    tzsc.setImgPath(imgs);
                    int c = savePostInMongoDb(tzsc);
                    if(c==0){
                        throw new RuntimeException("文件上传...保存失败");
                    }
                }
            }
            throw new RuntimeException("文件上传...失败");
        }
        throw new RuntimeException("文件上传...失败");
    }



}
