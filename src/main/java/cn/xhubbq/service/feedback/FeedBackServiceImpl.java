package cn.xhubbq.service.feedback;

import cn.xhubbq.pojo.feedback.FeedBack;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
@Service
public class FeedBackServiceImpl implements IFeedbackService{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public int insertIntoMongo(FeedBack feedBack) {
        FeedBack insert = mongoTemplate.insert(feedBack);
        if(insert==null){
            return 0;
        }
        return 1;
    }

    @Override
    public int updateFeedBack(String id, int status) {
        Query query=new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update=new Update();
        update.set("status",1);
        UpdateResult upsert = mongoTemplate.upsert(query, update, FeedBack.class);
        long modifiedCount = upsert.getModifiedCount();
        if(modifiedCount==0){
            return 0;
        }
        return 1;
    }
}
