package cn.xhubbq.service.xguser;

import cn.xhubbq.mapper.xgUser.UserMapper;
import cn.xhubbq.pojo.Constant;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.utils.AesCbcUtil;
import cn.xhubbq.utils.ContentCheckUtil;
import cn.xhubbq.utils.HttpClientUtil;
import cn.xhubbq.utils.RedisUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Author:甲粒子
 * Date: 2021/12/2
 * Description：
 */
@Service
public class UserServiceImpl implements  IUserService{
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public Integer selectShengxia(Integer userId) {
        return userMapper.selectShengxia(userId);
    }
    @Override
    public Integer useMoney(Integer userid, Integer nums) {
        return userMapper.useMoney(userid,nums);
    }
    @Override
    public Integer addMoney(Integer userid, Integer nums) {
        return userMapper.addMoney(userid,nums);
    }

    @Override
    public Integer selectIdByOpenid(String openid) {
        return userMapper.selectIdByOpenid(openid);
    }

    @Override
    public Integer countUserFollows(Integer userId) {
        return userMapper.countUserFollows(userId);
    }

    @Override
    public List<XgUser> selectAllUserFollowsBypageStatus(Integer userid, long curPage, int pSize, int status) {
        return userMapper.selectAllUserFollowsBypageStatus(userid,curPage,pSize,status);
    }

    @Override
    public Integer countUserFans(Integer userId) {
        return userMapper.countUserFans(userId);
    }

    @Override
    public List<XgUser> selectAllUserFansBypageStatus(Integer userid, long curPage, int pSize, int status) {
        return userMapper.selectAllUserFansBypageStatus(userid,curPage,pSize,status);
    }

    @Override
    public Integer addAttention(Integer userid, Integer followedid, String ctime) {
        return userMapper.addAttention(userid,followedid,ctime);
    }

    @Override
    public Integer addFan(Integer userid, Integer fanid, String ctime) {
        return userMapper.addFan(userid,fanid,ctime);
    }

    @Override
    public boolean deleteFanNote(Integer userId, Integer fanid) {
        return userMapper.deleteFanNote(userId,fanid);
    }

    @Override
    public boolean deleteFollowNote(Integer userid, Integer followedid) {
        return userMapper.deleteFollowNote(userid,followedid);
    }

    @Override
    public int incUserFanNum(Integer userid) {
        return userMapper.incUserFanNum(userid);
    }

    @Override
    public int decrUserFanNum(Integer userid) {
        return userMapper.decrUserFanNum(userid);
    }

    @Override
    public int incUserAttentionNum(Integer userid) {
        return userMapper.incUserAttentionNum(userid);
    }

    @Override
    public int decrUserAttentionNum(Integer userid) {
        return userMapper.decrUserAttentionNum(userid);
    }

    @Override
    public int noteInFollows(Integer userid, Integer followedid) {
        return userMapper.noteInFollows(userid,followedid);
    }

    @Override
    public int noteInFans(Integer userid, Integer fanid) {
        return userMapper.noteInFans(userid,fanid);
    }

    @Override
    public XgUser selectUserInfoById(Integer userid) {
        return userMapper.selectUserInfoById(userid);
    }

    @Override
    public List<XgUser> selectUserInfoByIds(List<Integer> Ids) {
        return userMapper.selectUserInfoByIds(Ids);
    }

    @Override
    public boolean addAccount(Integer userid, String ctime, long money, int version, int status) {
        return userMapper.addAccount(userid,ctime,money,version,status);
    }

    @Override
    public String requestWX(String data, ObjectNode node, Map<String, String> params) {
        try {
            params.put("appid", Constant.APP_ID);
            params.put("secret",Constant.APP_SECRET);
            params.put("js_code",node.get("code").toString().replace("\"", ""));
            params.put("grant_type","authorization_code");
            /**
             * 向微信服务器发送请求
             */
            return HttpClientUtil.doGet(Constant.REQUEST_FOR_WX,params);
        } catch ( KeyStoreException | NoSuchAlgorithmException | KeyManagementException e ) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ObjectNode resolveData(String data) {
        try {
            return new ObjectMapper().readValue(data, ObjectNode.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ObjectNode resolveEncryptedData(ObjectNode data,ObjectNode node) {
        try {
            String decrypt = AesCbcUtil.decrypt(data.get("encryptedData").toString(), node.get("session_key").toString(), data.get("iv").toString(), "utf-8");
            return new ObjectMapper().readValue(decrypt, ObjectNode.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public boolean checkContent(String accessToken, String content) {

        return ContentCheckUtil.cotentFilter(accessToken,content);
    }

    @Override
    public boolean checkImages(String accessToken, MultipartFile multipartFile) {
        return ContentCheckUtil.imgFilter(accessToken,multipartFile);
    }

    @Override
    public boolean checkContentAndImages(MultipartFile multipartFile, String content) {
        try
        {
            String accessToken = (String) redisUtil.get("access_token");;

            boolean x = ContentCheckUtil.cotentFilter(accessToken,content);
            boolean y = true;
            if(multipartFile != null){
                y = ContentCheckUtil.imgFilter(accessToken,multipartFile);
            }
            return x && y;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
