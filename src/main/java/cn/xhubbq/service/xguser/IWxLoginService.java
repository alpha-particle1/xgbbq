package cn.xhubbq.service.xguser;

import cn.xhubbq.pojo.XgUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/16
 * Description：
 */

public interface IWxLoginService {

    /**
     *
     * @param
     * @return
     */
    int addUser(XgUser user);

    /**
     *查询openid是否存在
     * @param
     * @return
     */
    int userIsExisted(String userid);

    /**
     *查询最大id  放入redis
     * @param
     * @return
     */
    int maxId();

    /**
     *
     * @param
     * @return
     */
    XgUser selectUser(String userid);

    /**
     *
     * @param
     * @return
     */
    int updateLastTime(String ltime,String userid);

    //查redis的hash中是否存在openid的用户
    /**
     *
     * @param
     * @return
     */
    boolean userIsInRedis(String key,String item);

    /**
     *hash中添加用户
     * @param
     * @return
     */
    boolean addUserInRedis(String key,String item,Object value,long time);

    /**
     * hash中添加新用户
     * @param
     * @return
     */
    boolean addUsertoNewUsers(String key,String item,Object value,long time);

    /**
     * 拿到所有新用户
     * @param
     * @return
     */
    List<Object> takeNewUsersFromRedis(String key);

    /**
     * 删除redis中的新用户 一个一个的删除
     * @param
     * @return
     */
    boolean deleteNewUserInRedis(String key,String item);
}
