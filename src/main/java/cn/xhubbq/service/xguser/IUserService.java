package cn.xhubbq.service.xguser;

import cn.xhubbq.pojo.XgUser;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.ibatis.annotations.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Author:甲粒子
 * Date: 2021/12/2
 * Description：
 */
public interface IUserService {

    /**
     * 查询余额
     * @param userId
     * @return
     */
    Integer selectShengxia(Integer userId);

    /**
     * 扣款
     * @param
     * @return
     */
    Integer useMoney(Integer userid,Integer nums);

    /**
     * 充值
     * @param
     * @return
     */
    Integer addMoney(Integer userid,Integer nums);

    /**
     * get userId
     * @param
     * @return
     */
    Integer selectIdByOpenid(String openid);

    /**
     * 查询某人的关注总数
     * @param
     * @return
     */
    Integer countUserFollows(Integer userId);

    /**
     * 分页查询用户关注的 id 头像 昵称 性别
     * @param
     * @return
     */
    List<XgUser> selectAllUserFollowsBypageStatus(Integer userid, long curPage, int pSize, int status);

    /**
     * 查询某人的粉丝总数
     * @param
     * @return
     */
    Integer countUserFans(Integer userId);

    /**
     * 分页查询用户粉丝的 id 头像 昵称 性别
     * @param
     * @return
     */
    List<XgUser> selectAllUserFansBypageStatus(Integer userid,long curPage,int pSize,int status);

    /**
     * 新增关注
     * @param
     * @return
     */
    Integer addAttention(Integer userid,Integer followedid,String ctime);

    /**
     * 新增粉丝
     * @param
     * @return
     */
    Integer addFan(Integer userid,Integer fanid,String ctime);

    /**
     * 从粉丝表中删除
     * @param
     * @return
     */
    boolean deleteFanNote(Integer userId,Integer fanid);

    /**
     * 从关注表中删除
     * @param
     * @return
     */
    boolean deleteFollowNote(Integer userid,Integer followedid);

    /**
     * 粉丝+1
     * @param
     * @return
     */
    int incUserFanNum(Integer userid);

    /**
     * 粉丝-1
     * @param
     * @return
     */
    int decrUserFanNum(Integer userid);

    /**
     * 关注+1
     * @param
     * @return
     */
    int incUserAttentionNum(Integer userid);

    /**
     * 关注-1
     * @param
     * @return
     */
    int decrUserAttentionNum(Integer userid);

    /**
     *
     * @param
     * @return
     */
    int noteInFollows(Integer userid,Integer followedid);

    /**
     *
     * @param
     * @return
     */
    int noteInFans(Integer userid,Integer fanid);

    /**
     *
     * @param
     * @return
     */
    XgUser selectUserInfoById(Integer userid);

    /**
     *
     * @param
     * @return
     */
    List<XgUser> selectUserInfoByIds(List<Integer> Ids);

    /**
     *
     * @param
     * @return
     */
    boolean addAccount(Integer userid,String ctime,long money,int version,int status);

    /**
     * 微信登录，向微信服务器发送请求，并获取返回结果
     * @param data 客户端的请求信息
     * @param node 用于存储解析出的请求信息
     * @param params 存储小程序信息,appId,secret
     * @return 返回微信服务器返回的信息
     */
    String requestWX(String data, ObjectNode node, Map<String,String> params);

    /**
     * 解析数据
     * @param data 微信服务器返回的数据
     * @return 用ObjectNode承载
     */
    ObjectNode resolveData(String data);


    /**
     * 解密数据encryptedData
     * @param data resolveData(String data);的返回值
     * @return 用ObjectNode承载
     */
    ObjectNode resolveEncryptedData(ObjectNode data,ObjectNode node);


    /**
     * 校验文字是否违规
     * @param content 待检测的文本
     * @return
     */
    boolean checkContent(String accessToken,String content);

    /**
     * 校验图片是否违规
     * @param multipartFile 待检测的文件
     * @return
     */
    boolean checkImages(String accessToken,MultipartFile multipartFile);

    /**
     * 校验图片是否违规
     * @param multipartFile 待检测的文件
     * @return
     */
    boolean checkContentAndImages(MultipartFile multipartFile,String content);



}
