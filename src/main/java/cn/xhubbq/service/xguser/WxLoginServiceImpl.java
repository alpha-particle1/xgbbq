package cn.xhubbq.service.xguser;

import cn.xhubbq.mapper.xgUser.WxLoginMapper;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/16
 * Description：
 */
@Service
public class WxLoginServiceImpl implements IWxLoginService{
    @Autowired
    private WxLoginMapper wxLoginMapper;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public int addUser(XgUser user) {
        return wxLoginMapper.addUser(user);
    }

    @Override
    public int userIsExisted(String userid) {
        return wxLoginMapper.userIsExisted(userid);
    }

    @Override
    public int maxId() {
        return wxLoginMapper.maxId();
    }

    @Override
    public XgUser selectUser(String userid) {
        return wxLoginMapper.selectUser(userid);
    }

    @Override
    public int updateLastTime(String ltime, String userid) {
        return wxLoginMapper.updateLastTime(ltime,userid);
    }


    @Override
    public boolean userIsInRedis(String key, String item) {
        return redisUtil.hHasKey(key,item);
    }

    @Override
    public boolean addUserInRedis(String key, String item, Object value, long time) {
        return redisUtil.hset(key,item,value,time);
    }

    @Override
    public boolean addUsertoNewUsers(String key, String item, Object value, long time) {
        return redisUtil.hset(key,item,value,time);
    }

    @Override
    public List<Object> takeNewUsersFromRedis(String key) {
        return redisUtil.hgetAll(key);
    }

    @Override
    public boolean deleteNewUserInRedis(String key, String item) {
        redisUtil.hdel(key,item);
        return true;
    }

    public XgUser selectUserFromRedis(String key,String openid){
        return (XgUser) redisUtil.hget(key, openid);
    }
}
