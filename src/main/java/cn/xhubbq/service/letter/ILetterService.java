package cn.xhubbq.service.letter;

import cn.xhubbq.pojo.letter.Letter;
import com.mongodb.client.result.DeleteResult;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/24
 * Description：
 */
public interface ILetterService {
    //写信
    Letter addLetter(Letter letter);
    //删除
    DeleteResult deleteLetterById(String id);
    //查询所有
    List<Letter> selectAllLetters();
    //分页查询
    List<Letter> selectLettersByPage(int curPage,int pageSize);
    //查总数
    long letterCount();

}
