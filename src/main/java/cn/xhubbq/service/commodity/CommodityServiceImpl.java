package cn.xhubbq.service.commodity;
import cn.xhubbq.pojo.commodity.Commodity;
import cn.xhubbq.pojo.commodity.CommodityFavorites;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：
 */
@Service
public class CommodityServiceImpl implements ICommodityService{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public int upCommodity(Commodity commodity) {
        Commodity insert = mongoTemplate.insert(commodity);
        if(insert != null){
            return 1;
        }
        return 0;
    }

    @Override
    public int deleteCommodity(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult remove = mongoTemplate.remove(query, Commodity.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public List<Commodity> getCommodityPage(long curPage, int pageSize) {
        Query query=new Query();
        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);
    }

    @Override
    public long countCommodity() {
        return mongoTemplate.count(new Query(),Commodity.class);
    }

    @Override
    public List<Commodity> getCommodityPage(String key, int type,int srt, long curPage, int pageSize) {
        Criteria criteria = new Criteria();
        Pattern pattern = Pattern.compile("^.*"+key+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query ;
        //key
        if (type == 0 && key.equals("kmtzsc")){
            query =  new Query();
            //query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern),Criteria.where("description").regex(pattern)));
        }else if (type != 0 && key.equals("kmtzsc")){
            query = new Query(Criteria.where("type").is(type));
        }
        else if(type == 0 && !key.equals("kmtzsc")){
            query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern),Criteria.where("description").regex(pattern)));
        }
        else{
            query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern).and("type").is(type),Criteria.where("description").regex(pattern).and("type").is(type)));
        }

        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        switch (srt){
            case 1:{
                sort=Sort.by(Sort.Direction.DESC,"upTime");
            }break;
            case 2:{
                sort=Sort.by(Sort.Direction.ASC,"upTime");
            }break;
            case 3:{
                sort=Sort.by(Sort.Direction.DESC,"price");
            }break;
            case 4:{
                sort=Sort.by(Sort.Direction.ASC,"price");
            }break;
        }
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);
    }

    public List<Commodity> getCommodityPage1(int type,int srt, long curPage, int pageSize) {
        Criteria criteria = new Criteria();
        Query query = new Query(criteria.orOperator(Criteria.where("type").is(type)));
        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        switch (srt){
            case 1:{
                sort=Sort.by(Sort.Direction.DESC,"upTime");
            }break;
            case 2:{
                sort=Sort.by(Sort.Direction.ASC,"upTime");
            }break;
            case 3:{
                sort=Sort.by(Sort.Direction.DESC,"price");
            }break;
            case 4:{
                sort=Sort.by(Sort.Direction.ASC,"price");
            }break;
        }
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);
    }

    @Override
    public long countCommodity(String key, int type) {
        Criteria criteria = new Criteria();
        Pattern pattern = Pattern.compile("^.*"+key+".*$",Pattern.CASE_INSENSITIVE) ;
        Query query = new Query(criteria.orOperator(Criteria.where("title").regex(pattern).and("type").is(type),Criteria.where("description").regex(pattern).and("type").is(type)));
        return mongoTemplate.count(query,Commodity.class);
    }

    @Override
    public Commodity getCommodityById(String id) {
        List<Commodity> c = mongoTemplate.find(new Query(Criteria.where("_id").is(id)), Commodity.class);
        if (c.size()>=1){
            return c.get(0);
        }
        return null;
    }

    @Override
    public int addFavorites(CommodityFavorites favorites) {
        Query query = new Query();
        query.addCriteria(Criteria.where("cid").is(favorites.getCid()).and("userId").is(favorites.getUserId()));
        long count = mongoTemplate.count(query, CommodityFavorites.class);
        if(count >= 1)return 0;
        CommodityFavorites insert = mongoTemplate.insert(favorites);
        if(insert != null){
            return 1;
        }
        return 0;
    }

    @Override
    public int deleteFromFavorites(Integer userId, String commodityId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("cid").is(commodityId).and("userId").is(userId));
        DeleteResult remove = mongoTemplate.remove(query, CommodityFavorites.class);
        if(remove.getDeletedCount()==0)
            return 0;
        return 1;
    }

    @Override
    public long countFavorites(Integer userId) {
        return mongoTemplate.count(new Query(Criteria.where("userId").is(userId)),CommodityFavorites.class);
    }

    @Override
    public List<Commodity> getFavoritesByPage(Integer userId,long curPage, int pageSize) {
        Query query=new Query(Criteria.where("userId").is(userId));
        Sort sort=Sort.by(Sort.Direction.DESC,"cTime");
        List<CommodityFavorites> commodityFavorites = mongoTemplate.find(query.with(sort).skip((curPage - 1) * pageSize).limit(pageSize), CommodityFavorites.class);
        //System.out.println(commodityFavorites);
        List<String> ids = new ArrayList<>();
        if (commodityFavorites.size() < 1){
            return null;
        }
        for (CommodityFavorites commodityFavorite : commodityFavorites) {
            //System.out.println(commodityFavorite.getId());
            //System.out.println(commodityFavorite.getCid());
            ids.add(commodityFavorite.getCid());
        }
        return mongoTemplate.find(new Query(Criteria.where("_id").in(ids)).skip((curPage - 1) * pageSize).limit(pageSize),Commodity.class);
    }

    @Override
    public long countCommodity(int type) {
        Criteria criteria = new Criteria();
        Query query = new Query(criteria.orOperator(Criteria.where("type").is(type)));
        return mongoTemplate.count(query,Commodity.class);
    }

    @Override
    public List<Commodity> getCommodityByPage_id(Integer userId, int type,int srt,long curPage, int pageSize) {
        Query query = new Query(Criteria.where("type").is(type).and("userId").is(userId));
        if (type == 0){
            query = new Query(Criteria.where("userId").is(userId));
        }
        Sort sort=Sort.by(Sort.Direction.DESC,"upTime");
        switch (srt){
            case 1:{
                sort=Sort.by(Sort.Direction.DESC,"upTime");
            }break;
            case 2:{
                sort=Sort.by(Sort.Direction.ASC,"upTime");
            }break;
            case 3:{
                sort=Sort.by(Sort.Direction.DESC,"price");
            }break;
            case 4:{
                sort=Sort.by(Sort.Direction.ASC,"price");
            }break;
        }
        return mongoTemplate.find(query.with(sort).skip((curPage-1)*pageSize).limit(pageSize), Commodity.class);
    }
}
