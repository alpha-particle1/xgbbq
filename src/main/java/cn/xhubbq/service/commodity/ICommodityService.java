package cn.xhubbq.service.commodity;

import cn.xhubbq.pojo.commodity.Commodity;
import cn.xhubbq.pojo.commodity.CommodityFavorites;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：
 */
public interface ICommodityService {
    //上架二手商品
    int upCommodity(Commodity commodity);
    //下架二手商品
    int deleteCommodity(String id);
    //分页拉取非模糊查询下
    List<Commodity> getCommodityPage(long curPage, int pageSize);
    //分页数据总数
    long countCommodity();
    public List<Commodity> getCommodityPage1(int type,int srt, long curPage, int pageSize);
    //分页拉取模糊查询下
    List<Commodity> getCommodityPage(String key,int type,int srt,long curPage, int pageSize);
    //分页数据总数
    long countCommodity(String key,int type);
    //通过id查找商品信息
    Commodity getCommodityById(String id);
    //新增收藏
    int addFavorites(CommodityFavorites favorites);
    //从收藏夹移除
    int deleteFromFavorites(Integer userId,String commodityId);
    //收藏的数量
    long countFavorites(Integer userId);
    //分页拉取收藏的商品
    List<Commodity> getFavoritesByPage(Integer userId,long curPage, int pageSize);
    long countCommodity(int type);
    List<Commodity> getCommodityByPage_id(Integer userId,int type,int srt,long curPage, int pageSize);
}
