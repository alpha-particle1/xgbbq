package cn.xhubbq.service.banner;

import cn.xhubbq.pojo.banner.Banner;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/21
 * Description：
 */
@Service
public class BannerServiceImpl implements IBannerService{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public int deleteAllBannerInMongo() {
        DeleteResult remove = mongoTemplate.remove(new Query(), Banner.class);
        if (remove!=null && remove.getDeletedCount()>=1){
            return 1;
        }
        return 0;
    }

    @Override
    public int addNewBanners(Banner banner) {
        Banner insert = mongoTemplate.insert(banner);
        if (insert!=null)
            return 1;
        return 0;
    }

    @Override
    public UpdateResult updateBanner(String id, String img_urls, String mtime) {
        Query query=new Query(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("img_url",img_urls);
        UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Banner.class);
        return updateResult;
    }

    @Override
    public List<Banner> getAllBanners() {
        return mongoTemplate.findAll(Banner.class);
    }
}
