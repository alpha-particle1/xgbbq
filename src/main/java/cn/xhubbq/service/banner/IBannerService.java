package cn.xhubbq.service.banner;

import cn.xhubbq.pojo.banner.Banner;
import com.mongodb.client.result.UpdateResult;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/21
 * Description：
 */
public interface IBannerService {
    int deleteAllBannerInMongo();
    int addNewBanners(Banner banner);
    UpdateResult updateBanner(String id, String img_urls, String mtime);
    List<Banner> getAllBanners();
}
