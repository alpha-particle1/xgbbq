package cn.xhubbq.service.lunbo;

import cn.xhubbq.pojo.lunbo.LunboImg;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/5
 * Description：
 */
public interface ILunboService {
    int updateLunboImg(int id,String imgUrl);
    List<LunboImg> selectAll();
    int updateLunboArticle(int id,String articleUrl);
}
