package cn.xhubbq.service.lunbo;

import cn.xhubbq.mapper.lunbo.LunboMapper;
import cn.xhubbq.pojo.lunbo.LunboImg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/5
 * Description：
 */
@Service
public class LunboServiceImpl implements  ILunboService{
    @Autowired
    private LunboMapper lunboMapper;
    @Override
    public int updateLunboImg(int id, String imgUrl) {
        return lunboMapper.updateLunboImg(id,imgUrl);
    }

    @Override
    public List<LunboImg> selectAll() {
        return lunboMapper.selectAll();
    }

    @Override
    public int updateLunboArticle(int id, String articleUrl) {
        return lunboMapper.updateLunboArticle(id,articleUrl);
    }
}
