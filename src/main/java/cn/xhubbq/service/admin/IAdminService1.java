package cn.xhubbq.service.admin;

import cn.xhubbq.pojo.XgUser;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/27
 * Description：
 */
public interface IAdminService1 {
    //查询所有用户
    List<XgUser> selectAllUsers();
    //查询所有用户根据某种排序
    //List<XgUser> selectAllUsers0();
    //更新用户状态
    int updateUserStatus(Integer id,int status);
    //修改用户权限
    int updateUserQuan(Integer id,int identify);
    //分页查询
    List<XgUser> selectAllUsersBypage(long curPage,int pSize);
    //查询用户总数(分页用到)
    int countUser();
    int updateUser3(String id,int status,int identify,int gender);
    List<XgUser> selectAllUsersBypageStatus(long curPage,int pSize,int status);
    int countUserwithStatus(int status);
    int countUserLike(String key);
    List<XgUser> selectAllUsersBypageLike(long curPage,int pSize,String key);
}
