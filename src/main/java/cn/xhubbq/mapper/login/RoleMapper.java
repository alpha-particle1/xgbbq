package cn.xhubbq.mapper.login;

import cn.xhubbq.pojo.XglmRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/9
 * Description：权限列表
 */
@Mapper
public interface RoleMapper {

    @Select("select r.*  from xglm_role as r where r.id in(select roleid from role_user where userid=#{userid})")
    List<XglmRole> findRoleByAdminId(int userid);
}
