package cn.xhubbq.mapper.provider;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/20
 * Description：
 */
public class UserProvider {

    public String findUsersInfoByIds(@Param("Ids") List<Integer> Ids){
        StringBuilder builder=new StringBuilder();
        builder.append("SELECT * FROM xg_user WHERE id IN (");
        int i=1;
        for (Integer id : Ids) {
            builder.append(id);
            if (i==Ids.size()){break;}
            builder.append(",");
            i++;
        }
        builder.append(")");
        return builder.toString();
    }
}
