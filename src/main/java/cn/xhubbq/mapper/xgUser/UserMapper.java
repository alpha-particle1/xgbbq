package cn.xhubbq.mapper.xgUser;

import cn.xhubbq.mapper.provider.UserProvider;
import cn.xhubbq.pojo.XgUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/27
 * Description：
 */
@Mapper
public interface UserMapper {

    /**
     * 查询所有用户
     * @param
     * @return
     */
    @Select("select * from xg_user")
    List<XgUser> selectAllUsers();


    /**
     * 查询所有用户根据某种排序
     * @param
     * @return
     */
    List<XgUser> selectAllUsers0();

    /**
     * @param
     * @return
     */
    @Update("update xg_user set status=#{status},identify=#{identify},gender=#{gender},version=version+1 where userid=#{id} and version=#{version}")
    int updateUser3(String id,int status,int identify,int gender,int version);



    /**
     * 更新用户状态
     * @param
     * @return
     */
    @Update("update xg_user set status=#{status} where id=#{id}")
    int updateUserStatus(Integer id,int status);

    /**
     * 修改用户权限
     * @param
     * @return
     */
    @Update("update xg_user set identify=#{status} where id=#{id}")
    int updateUserQuan(Integer id,int identify);

    /**
     * 分页查询
     * @param
     * @return
     */
    @Select("select * from xg_user limit #{curPage},#{pSize}")
    List<XgUser> selectAllUsersBypage(long curPage,int pSize);

    /**
     * @param
     * @return
     */
    //分页查询（查根据状态查询）
    @Select("select * from xg_user where status=#{status} limit #{curPage},#{pSize}")
    List<XgUser> selectAllUsersBypageStatus(long curPage,int pSize,int status);

    /**
     * 模糊查询的分页
     * @param
     * @return
     */
    @Select("select * from xg_user where nickname like \"%\"#{key}\"%\" limit #{curPage},#{pSize}")
    List<XgUser> selectAllUsersBypageLike(long curPage,int pSize,String key);

    /**
     * 查询用户总数(分页用到)
     * @param
     * @return
     */
    @Select("select count(1) from xg_user")
    int countUser();

    /**
     * 查询某种状态下的用户总数
     * @param
     * @return
     */
    @Select("select count(1) from xg_user where status=#{status}")
    int countUserwithStatus(int status);

    /**
     * 模糊查询的用户总数
     * @param
     * @return
     */
    @Select("select count(1) from xg_user where nickname like \"%\"#{key}\"%\"")
    int countUserLike(String key);

    /**
     * @param
     * @return
     */
    @Select("select version from xg_user where userid=#{userid}")
    int watch(String userid);//乐观锁

    /**
     * 添加账户记录
     * @param
     * @return
     */
    @Insert("insert into user_account(userid,nums,ctime,version,status) values(#{userid},#{money},#{ctime},#{version},#{status})")
    boolean addAccount(Integer userid,String ctime,long money,int version,int status);

    /**
     * 查询余额
     * @param
     * @return
     */
    @Select("select * from user_account where userid=#{userid}")
    Integer selectShengxia(Integer userId);

    /**
     * 扣款
     * @param
     * @return
     */
    @Update("update user_account set nums=nums-#{nums} where userid=#{userid}")
    Integer useMoney(Integer userid,Integer nums);

    /**
     * 充值
     * @param
     * @return
     */
    @Update("update user_account set nums=nums+#{nums} where userid=#{userid}")
    Integer addMoney(Integer userid,Integer nums);

    /**
     * 根据openid查询id
     * @param
     * @return
     */
    @Select("select id from xg_user where userid=#{openid}")
    Integer selectIdByOpenid(String openid);

    /**
     * 查询某人的关注总数
     * @param
     * @return
     */
    @Select("select count(1) from user_follows where userid=#{userId}")
    Integer countUserFollows(Integer userId);

    /**
     * 分页查询用户关注的 id 头像 昵称 性别
     * @param
     * @return
     */
    @Select("select u1.* from xg_user u1 where u1.status=#{status} and u1.id in(select u2.followedid from user_follows u2 where u2.userid=#{userid}) limit #{curPage},#{pSize}")
    List<XgUser> selectAllUserFollowsBypageStatus(Integer userid,long curPage,int pSize,int status);

    /**
     * 查询某人的粉丝总数
     * @param
     * @return
     */
    @Select("select count(1) from user_fans where userid=#{userId}")
    Integer countUserFans(Integer userId);

    /**
     * 分页查询用户粉丝的 id 头像 昵称 性别
     * @param
     * @return
     */
    @Select("select u1.* from xg_user u1 where u1.status=#{status} and u1.id in(select u2.fanid from user_fans u2 where u2.userid=#{userid}) limit #{curPage},#{pSize}")
    List<XgUser> selectAllUserFansBypageStatus(Integer userid,long curPage,int pSize,int status);

    /**
     * 新增关注
     * @param
     * @return
     */
    @Insert("insert into user_follows values(#{userid},#{followedid},#{ctime})")
    Integer addAttention(Integer userid,Integer followedid,String ctime);

    /**
     * 新增粉丝
     * @param
     * @return
     */
    @Insert("insert into user_fans values(#{userid},#{fanid},#{ctime})")
    Integer addFan(Integer userid,Integer fanid,String ctime);

    /**
     * 从粉丝表中删除
     * @param
     * @return
     */
    @Delete("delete from user_fans where userid=#{userid} and fanid=#{fanid}")
    boolean deleteFanNote(Integer userId,Integer fanid);

    /**
     * 从关注表中删除
     * @param
     * @return
     */
    @Delete("delete from user_follows where userid=#{userid} and followedid=#{followedid}")
    boolean deleteFollowNote(Integer userid,Integer followedid);

    /**
     * 粉丝+1
     * @param
     * @return
     */
    @Update("update xg_user set fans=fans+1 where id=#{userid}")
    int incUserFanNum(Integer userid);

    /**
     * 粉丝-1
     * @param
     * @return
     */
    @Update("update xg_user set fans=fans-1 where id=#{userid}")
    int decrUserFanNum(Integer userid);

    /**
     * 关注+1
     * @param
     * @return
     */
    @Update("update xg_user set attention=attention+1 where id=#{userid}")
    int incUserAttentionNum(Integer userid);

    /**
     * 关注-1
     * @param
     * @return
     */
    @Update("update xg_user set attention=attention-1 where id=#{userid}")
    int decrUserAttentionNum(Integer userid);

    /**
     * @param
     * @return
     */
    @Select("select count(1) from user_follows where userid=#{userid} and followedid=#{followedid}")
    int noteInFollows(Integer userid,Integer followedid);

    /**
     * @param
     * @return
     */
    @Select("select count(1) from user_fans where userid=#{userid} and fanid=#{fanid}")
    int noteInFans(Integer userid,Integer fanid);

    /**查询用户信息
     * @param
     * @return
     */
    @Select("select * from xg_user where id=#{userid}")
    XgUser selectUserInfoById(Integer userid);

    /**批量查询用户信息
     * @param
     * @return
     */
    @SelectProvider(type = UserProvider.class,method = "findUsersInfoByIds")
    List<XgUser> selectUserInfoByIds(@Param("Ids")List<Integer> Ids);

    /**
     * 查询用户是否存在
     * @param openId
     * @return
     */
    @Select("select count(*) from xg_user where userid=#{openId}")
    int selectUser(String openId);
}
