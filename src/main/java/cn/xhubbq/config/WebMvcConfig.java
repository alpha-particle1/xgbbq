package cn.xhubbq.config;

import cn.xhubbq.interceptor.JwtTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Autowired
    private JwtTokenInterceptor jwtTokenInterceptor;
    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        //super.addViewControllers(registry);
        registry.addViewController("/").setViewName("/main");
        registry.addViewController("/km/login").setViewName("login");
        //registry.addViewController("/up").setViewName("up");
        registry.addViewController("/index").setViewName("/main");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //调试过程中不加这一句会报 static/component/style/components.css 等资源找不到
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/","file:static/");
        registry.addResourceHandler("/upload/**").addResourceLocations("classpath:/upload/");
        registry.addResourceHandler("/articles/**").addResourceLocations("classpath:/articles/");
        //  获取与jar同级目录下的upload文件夹  设置与jar同级静态资源配置
        ApplicationHome h = new ApplicationHome(this.getClass());
        // 本地获取的路径 D:\idea\springboot2.x\target  upload 跟 项目jar平级
        String path = h.getSource().getParent();
        String realPath = path + "/upload/";
        registry.addResourceHandler("/upload/**").addResourceLocations("file:"+realPath);
        realPath=path+"/articles/";
        registry.addResourceHandler("/articles/**").addResourceLocations("file:"+realPath);
    }
}
