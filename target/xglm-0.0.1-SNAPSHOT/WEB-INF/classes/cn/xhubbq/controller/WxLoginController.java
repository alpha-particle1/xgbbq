package cn.xhubbq.controller;

import cn.xhubbq.utils.AesCbcUtil;
import cn.xhubbq.utils.EncryptUtil;
import cn.xhubbq.utils.HttpClientUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/11
 * Description：微信登录
 */
@RestController
public class WxLoginController {
    private final static String AppID="wxde60288085ae0d08";
    private final static String AppSecret="8a53de07bf991f1dc0e6e682081471c3";

    @RequestMapping("/wxLogin")
    public String wxLogin(@RequestBody String data){
        ObjectMapper mapper=new ObjectMapper();
        ObjectNode node = null;

        try {
            //1、获取前端传来的code
            //data是json字符串
            node = new ObjectMapper().readValue(data, ObjectNode.class);
            Map<String,String> params = new HashMap<>();
            params.put("appid", AppID);
            params.put("secret",AppSecret);
            if (node.has("code")) {
                params.put("js_code",node.get("code").toString().replace("\"", ""));
                System.out.println(node.get("code").toString());
            }
            // params.put("js_code",);
            params.put("grant_type","authorization_code");
            //2、微信服务器发送请求
            String wxRequestResult = null;
            wxRequestResult = HttpClientUtil.doGet("https://api.weixin.qq.com/sns/jscode2session?",params);
            //3、将openid 与 session_key 加密
            //4、将openid 与 session_key存入数据库
            //5、将openid 与 session_key封装好返回给前端，前端做缓存
            ObjectNode node2 = new ObjectMapper().readValue(wxRequestResult, ObjectNode.class);
            String session_key=node2.get("session_key").toString();
            String openid=node2.get("openid").toString();
            //生成自定义登录状态
            String session=null;
            //String session1=null;
            Map<String,String> result=new HashMap<>();
            Map<String,String> sessionMap=new HashMap<>();
            sessionMap.put("sessionKey",session_key);
            sessionMap.put("openId",openid);
            session=mapper.writeValueAsString(sessionMap);
            //session1=session;
            EncryptUtil encryptUtil=new EncryptUtil();
            session=encryptUtil.encrypt(session);
            result.put("session",session);

            //System.out.println(encryptUtil.decrypt(session).equals(session1)); //测试解密后的session
            //密文解密,放入数据库
            String decrypt = AesCbcUtil.decrypt(node.get("encryptedData").toString(), node2.get("session_key").toString(), node.get("iv").toString(), "utf-8");
            System.out.println(decrypt);
            /*
            //拼接url
            StringBuilder url = new StringBuilder("https://api.weixin.qq.com/sns/jscode2session?");
            url.append("appid=");//appid设置
            url.append(AppID);
            url.append("&secret=");//secret设置
            url.append(AppSecret);
            url.append("&js_code=");//code设置
            url.append(node.get("code").toString());
            url.append("&grant_type=authorization_code");
            wxRequestResult = HttpClientUtil.doGet(url.toString().replace("\"", ""));
            * */
            System.out.println(wxRequestResult);
            System.out.println(data);
            return mapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
