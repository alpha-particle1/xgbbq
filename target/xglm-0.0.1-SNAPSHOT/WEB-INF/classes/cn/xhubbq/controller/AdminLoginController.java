package cn.xhubbq.controller;

import cn.xhubbq.service.login.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

@Controller
public class AdminLoginController {
    @Autowired
    private IAdminService adminService;

    @RequestMapping("/xglm/admin")
    public String login(@RequestParam(value = "userName",required = false)String username, @RequestParam(value = "userPwd",required = false)String userpwd,HttpServletRequest request, HttpSession session){
        System.out.println(session.getId());
        if(session.getAttribute("userName")!=null){
            return "index";
        }else{
            return "redirect:/login";
        }
    }

    @RequestMapping("/up")
    public String upload(){
        return "up";
    }

    @RequestMapping("/index")
    public String index(HttpSession session){
        //获取上下栈
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication1 = context.getAuthentication();
        User user = (User) authentication1.getPrincipal();
        String username = user.getUsername();
        session.setAttribute("userName",username);
        String nickname = adminService.selectNickname(username);
        session.setAttribute("nickName",nickname);
        //System.out.println(session.getAttribute("userName"));
        return "index";
    }

    @RequestMapping("/controlpanel")
    public String controlpanel(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "controlpanel";
        else{
            return "login";
        }
    }

}
