package cn.xhubbq.controller;

import cn.xhubbq.pojo.mail.MailVo;
import cn.xhubbq.service.login.IAdminService;
import cn.xhubbq.service.mail.MailService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.xml.bind.util.JAXBSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Author:甲粒子
 * Date: 2021/11/11
 * Description：请求验证码
 */
@RestController
public class AskCheckCodeController {
    @Autowired
    private IAdminService adminService;
    @Autowired
    private MailService mailService;

    public static String verificationCode() {
        //生成六位随机正整数
        Random random = new Random();
        String verificationCode = String.valueOf(random.nextInt(9) + 1);
        for (int i = 0; i < 5; i++) {
            verificationCode += random.nextInt(10);
        }
        return verificationCode;
    }

    @CrossOrigin
    @ResponseBody
    @RequestMapping("/admin/sendcode")
    public String sendCode(HttpSession session){
        System.out.println(session.getId());
        Map<String,Object> map=new HashMap<>();
        System.out.println(session.getAttribute("userName"));
        if(session.getAttribute("userName")!=null){
            String username= (String) session.getAttribute("userName");
            String youxiang=adminService.adminMail(username);
            System.out.println(youxiang);
            //随机生成6位数
            String vcode=verificationCode();
            session.setAttribute("verificationCode",vcode);
            //发送邮件
            MailVo vo=new MailVo();
            vo.setTo(youxiang);
            vo.setSubject("狂码后台验证码");
            vo.setText("请务必不要泄露此次验证码："+vcode);
            mailService.sendTextMail(vo);
            map.put("state",1);
            return JSONObject.toJSONString(map);
        }else{
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }

    @ResponseBody
    @PostMapping("/admin/checkcode")
    public String checkcode(HttpSession session,String vcode){
        Map<String,Object> map=new HashMap<>();
        if(session.getAttribute("userName")!=null){
           String code= (String) session.getAttribute("verificationCode");
           if(code!=null) {
               if(code.equals(vcode)){
                   map.put("state",1);
                   return JSONObject.toJSONString(map);
               }else{
                   map.put("state",0);
                   return JSONObject.toJSONString(map);
               }
           }
           else{
               map.put("state",0);
               return JSONObject.toJSONString(map);
           }
        }else{
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }

}
