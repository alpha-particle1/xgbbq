package cn.xhubbq.im.repository;
import cn.xhubbq.im.entity.UnreadMessage;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;

@Mapper
public interface ReadUnReadMessageMapper {
    //根据自身id 拉取未读消息
    ArrayList<UnreadMessage> selectUnreadMessage(String userid);
}
