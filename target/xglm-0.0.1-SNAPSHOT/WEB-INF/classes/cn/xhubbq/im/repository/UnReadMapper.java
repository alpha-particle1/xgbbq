package cn.xhubbq.im.repository;

import cn.xhubbq.im.entity.UnreadMessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface UnReadMapper {
    //将未读消息写入数据库
    //@Insert("")
    boolean writeUnreadMessage(Map<String,Object> map);

    //已读后删除数据库的数据
    boolean deleteUnreadMessage(UnreadMessage unreadMessage);
}
