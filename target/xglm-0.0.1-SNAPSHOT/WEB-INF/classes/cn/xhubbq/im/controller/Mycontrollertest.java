package cn.xhubbq.im.controller;

import cn.xhubbq.im.entity.SocketMessage;
import cn.xhubbq.im.entity.UnreadMessage;
import cn.xhubbq.im.repository.UnReadMapper;
import cn.xhubbq.im.service.IUnReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.UUID;

@RestController
public class Mycontrollertest {
    @Autowired
    IUnReadService unReadService;

    @RequestMapping("/test")
    public String test(){
        String id= UUID.randomUUID().toString().replace("-","");
        System.out.println(id);
        SocketMessage socketMessage=new SocketMessage(1016,id,3,21016,"heartbeat","2021/11/05 14:55:22");
        UnreadMessage unreadMessage=new UnreadMessage(socketMessage);
        HashMap<String,Object> map=new HashMap<>();
        map.put("id",id);
        map.put("userId",unreadMessage.getSocketMessage().getUserId());
        map.put("chatId",unreadMessage.getSocketMessage().getChatId());
        map.put("messageType",unreadMessage.getSocketMessage().getMessageType());
        map.put("message",unreadMessage.getSocketMessage().getMessage());
        map.put("stime",unreadMessage.getSocketMessage().getStime());
        unReadService.writeUnreadMessage(map);
        return "123";
    }
}
