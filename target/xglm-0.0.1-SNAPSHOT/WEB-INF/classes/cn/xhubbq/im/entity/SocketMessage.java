package cn.xhubbq.im.entity;
import java.io.Serializable;
public class SocketMessage implements Serializable {
    /**
     * 消息类型
     * 0 私聊
     * 1 群聊
     * 2 评论消息
     * 3 回复消息
     * 5 点赞消息
     * 6 送礼消息
     * 1016 心跳包
     * 1017 服务器推送消息（系统消息）
     * 1018 关注消息
     * 2021 确认收到消息 已读
     */
    public static final int PRIVATE_CHAT=0;
    public static final int GROUP_CHAT=1;
    public static final int REVIEW=2;
    public static final int REPLY=3;
    public static final int GREAT=5;
    public static final int PRESENT=6;
    public static final int HEART_BEAT=1016;
    public static final int SYSTEM=1017;
    public static final int ATTENTION=1018;
    public static final int READ=2021;

    private int messageType;
    /**
     * 消息id
     */
    private String id;
    /**
     * 消息发送者id
     */
    private Integer userId;
    /**
     * 消息接受者id或群聊id
     */
    private Integer chatId;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 发送时间
     */
    private String stime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SocketMessage(int messageType, String id, Integer userId, Integer chatId, String message, String stime) {
        this.messageType = messageType;
        this.id = id;
        this.userId = userId;
        this.chatId = chatId;
        this.message = message;
        this.stime = stime;
    }

    public SocketMessage(int messageType, Integer userId, Integer chatId, String message, String stime) {
        this.messageType = messageType;
        this.userId = userId;
        this.chatId = chatId;
        this.message = message;
        this.stime = stime;
    }

    public String getStime() {
        return stime;
    }
    public void setStime(String stime) {
        this.stime = stime;
    }
    //....省略get set方法


    public SocketMessage() {
    }

    public SocketMessage(int messageType, Integer userId, Integer chatId, String message) {
        this.messageType = messageType;
        this.userId = userId;
        this.chatId = chatId;
        this.message = message;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getChatId() {
        return chatId;
    }

    public void setChatId(Integer chatId) {
        this.chatId = chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

