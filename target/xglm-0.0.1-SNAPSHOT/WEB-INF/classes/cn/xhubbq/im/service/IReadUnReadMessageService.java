package cn.xhubbq.im.service;
import cn.xhubbq.im.entity.UnreadMessage;
import java.util.ArrayList;

public interface IReadUnReadMessageService {
    //根据自身id 拉取未读消息
    ArrayList<UnreadMessage> selectUnreadMessage(String userid);
}
