package cn.xhubbq.im.listener;
import cn.xhubbq.im.ImServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//容器启动后加载Netty服务类
@Component
public class ImInitListen implements CommandLineRunner {

        @Value("${netty.port}")
        Integer nettyPort;
        @Value("${server.port}")
        Integer serverPort;

        @Override
        public void run(String... args) throws Exception {
            try {
                System.out.println("nettyServer starting ...");
                System.out.println("http://127.0.0.1:" + serverPort + "/login");
                new ImServer(nettyPort).start();
            } catch (Exception e) {
                System.out.println("NettyServerError:" + e.getMessage());
            }
        }

}
