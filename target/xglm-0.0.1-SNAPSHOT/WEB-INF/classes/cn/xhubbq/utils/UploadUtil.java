package cn.xhubbq.utils;

import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 *Author:甲粒子
 *Date:
 *Description：文件上传工具类
 */
public class UploadUtil {

    //上传多个图片
    //返回文件路径字符串
    public static String uploadImages(List<MultipartFile> files, File folder){
        //将所有文件名通过"|"链接起来
        String filesNames="";
        for (MultipartFile file : files) {
            //获取文件信息
            String oldName = file.getOriginalFilename().toLowerCase();
            if (!oldName.endsWith(".bmp") && !oldName.endsWith(".jpg")
                                     && !oldName.endsWith(".jpeg") && !oldName.endsWith(".png")
                                     && !oldName.endsWith(".gif")) {
                break;
            }
            //新文件名
            String newName = UUID.randomUUID().toString().substring(0,5)+file.getSize()+oldName.substring(oldName.lastIndexOf("."));
            try {
                //小文件使用transferTo
                file.transferTo(new File(folder, newName));
                filesNames +=(newName+"|");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filesNames;
    }


    //上传多个文件
    //返回文件路径字符串
    public static String uploadFiles(List<MultipartFile> files, File folder){
        //将所有文件名通过"|"链接起来
        String filesNames="";
        for (MultipartFile file : files) {
            //获取文件信息
            String oldName = file.getOriginalFilename();
            //新文件名
            String newName = UUID.randomUUID().toString().substring(0,5)+file.getSize()+oldName.substring(oldName.lastIndexOf("."));
            try {
                //小文件使用transferTo
                file.transferTo(new File(folder, newName));
                filesNames +=(newName+"|");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filesNames;
    }

    //创建folder
    public static boolean makeFolder(String path){
        File folder=new File(path);
        if(!folder.exists()){
            folder.mkdirs();
            return true;
        }
        return false;//证明多级文件路径已存在
    }
}
