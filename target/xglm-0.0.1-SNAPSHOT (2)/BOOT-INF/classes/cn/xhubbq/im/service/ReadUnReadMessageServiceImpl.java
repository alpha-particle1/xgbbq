package cn.xhubbq.im.service;

import cn.xhubbq.im.entity.UnreadMessage;
import cn.xhubbq.im.repository.ReadUnReadMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class ReadUnReadMessageServiceImpl implements IReadUnReadMessageService{

    @Autowired
    private ReadUnReadMessageMapper readUnReadMessageMapper;

    @Override//拉取未读消息
    public ArrayList<UnreadMessage> selectUnreadMessage(String userid) {
        return readUnReadMessageMapper.selectUnreadMessage(userid);
    }
}
