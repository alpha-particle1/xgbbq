package cn.xhubbq.im.service;

import cn.xhubbq.im.entity.UnreadMessage;
import cn.xhubbq.im.repository.UnReadMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UnReadServiceImpl implements  IUnReadService{
    @Autowired
    //@Qualifier("unReadMapper")
    private UnReadMapper unReadMapper;
    @Override //缓存未读消息
    public boolean writeUnreadMessage(Map<String,Object> map) {
        return unReadMapper.writeUnreadMessage(map);
    }

    @Override//删除未读消息 (已读)
    public boolean deleteUnreadMessage(UnreadMessage unreadMessage) {
        return unReadMapper.deleteUnreadMessage(unreadMessage);
    }
}
