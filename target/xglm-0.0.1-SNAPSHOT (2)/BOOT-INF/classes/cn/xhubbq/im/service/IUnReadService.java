package cn.xhubbq.im.service;

import cn.xhubbq.im.entity.UnreadMessage;

import java.util.Map;

public interface IUnReadService {
    //将未读消息写入数据库
    boolean writeUnreadMessage(Map<String,Object> map);
    //已读后删除数据库的数据
    boolean deleteUnreadMessage(UnreadMessage unreadMessage);
}
