package cn.xhubbq.im.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnreadMessage { //未读消息
    private SocketMessage socketMessage;
}
