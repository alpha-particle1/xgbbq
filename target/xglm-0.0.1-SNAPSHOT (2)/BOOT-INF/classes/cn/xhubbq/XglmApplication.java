package cn.xhubbq;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class XglmApplication {

    public static void main(String[] args) { SpringApplication.run(XglmApplication.class, args);
    }

}
