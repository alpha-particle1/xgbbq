package cn.xhubbq.config;

import cn.xhubbq.interceptor.LoginHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        //super.addViewControllers(registry);
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/up").setViewName("up");
        registry.addViewController("/index").setViewName("index");
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //调试过程中不加这一句会报 static/component/style/components.css 等资源找不到
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }

    /*
    *
    *
    * */
    //注册拦截器
    //@Override
    //protected void addInterceptors(InterceptorRegistry registry) {
        //静态资源: *.css *.js ...
        //SpringBoot已经做好了静态资源映射
       // registry.addInterceptor(new LoginHandlerInterceptor())
        //        .addPathPatterns("/**")//拦截 http://域名or-ip:8080/下的所有资源-请求
       //         .excludePathPatterns("/login","/xglm/admin","/",
       //                 "/**/*.html",            //html静态资源
       //                 "/**/*.js",              //js静态资源
       //                "/**/*.css",             //css静态资源
       //                 "/**/*.woff",
       //                 "/**/*.ttf");//除这些外
    //}
}
