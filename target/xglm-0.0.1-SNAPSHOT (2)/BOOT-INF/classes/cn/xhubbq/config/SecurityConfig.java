package cn.xhubbq.config;

import cn.xhubbq.service.login.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

/**
 * Author:甲粒子
 * Date: 2021/11/9
 * Description：SpringSecurity
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AdminServiceImpl adminService;

    private BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();
        //处理跨域请求中的Preflight请求
        http.authorizeRequests().requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
            .antMatchers("/").permitAll().antMatchers("/check").permitAll()
            .antMatchers("/403").permitAll()
            .antMatchers("/wxLogin").permitAll()
            .antMatchers("/index").hasRole("admin")
            .antMatchers("/admin/user").hasRole("admin")
            .antMatchers("/administrators/**").hasRole("superadmin")
            .antMatchers("/controlpanel").hasRole("admin")
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
            .antMatchers("/sendcode").permitAll()
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
            .antMatchers("/checkcode").permitAll()
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
            .antMatchers("/resetpwdPage").hasRole("admin")
            .antMatchers("/resetpassword").hasRole("admin")
            .antMatchers("/insertadmin").hasRole("superadmin")
            .antMatchers(HttpMethod.OPTIONS).permitAll();
        http.formLogin().loginPage("/login")
                        .usernameParameter("userName")
                        .passwordParameter("userPwd")
                        .loginProcessingUrl("/xglm/admin")
                        .failureUrl("/login?error=true")
                        .successForwardUrl("/index")
                        .and().sessionManagement()
                        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)//IF_REQUIRED  STATELESS不主动创建Session
                        .invalidSessionUrl("/login")
                        .maximumSessions(1);//只能有一个浏览器在线
                        //如果同时登录：This session has been expired (possibly due to multiple concurrent logins being attempted as the same user).

        //如果让logout在GET请求下生效，必须关闭防止CSRF攻击csrf().disable()。
        // 如果开启了CSRF，必须使用 post方式请求/logout
        http.csrf().disable();

        //iframe
        http.headers().frameOptions().disable();
        http.logout().logoutSuccessUrl("/login").invalidateHttpSession(true);

        http.exceptionHandling().accessDeniedPage("/403");

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(adminService).passwordEncoder(bCryptPasswordEncoder);
    }
}
