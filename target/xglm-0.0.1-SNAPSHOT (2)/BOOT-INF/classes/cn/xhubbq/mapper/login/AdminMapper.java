package cn.xhubbq.mapper.login;

import cn.xhubbq.pojo.XglmAdmin;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMapper {
    //根据 账号密码查询admin
    //@Select("select count(1) from xglm_admin where userid=#{userid} and userpwd=#{userpwd}")
    int selectAdminByIdPwd(Map<String,Object> map);

    @Select("select * from xglm_admin where username=#{name}")
    XglmAdmin loadAdminByAdminname(String name);

    //查询所有admin
    @Select("select * from xglm_admin")
    List<XglmAdmin> selectAlladmins();

    //按页查询admin
    @Select("select * from xglm_admin limit #{curPS},#{pageSize}")//curPS=pageSize*currentPage
    List<XglmAdmin> selectAlladminsBypage(int curPS,int pageSize);
    @Select("select count(1) from xglm_admin")
    int adminsNum();

    @Select("select user_mail from admin_info where user_name=#{username}")
    String adminMail(String username);

    @Update("update xglm_admin set userpwd=#{newpwd} where username=#{username}")
    int updatePwd(String username,String newpwd);

    //增加管理员
    @Insert("insert into xglm_admin values(#{id},#{username},#{userpwd},#{nickname})")
    int addAdmin(int id,String username,String userpwd,String nickname);
    //最大id
    @Select("select max(id) from xglm_admin")
    int maxId();

    //插入
    @Insert("insert into role_user values(#{userid},#{roleid})")
    int addRole(int userid,int roleid);

    //添加邮箱
    @Insert("insert into admin_info values(#{username},#{useremail})")
    int addEmail(String username,String useremail);

    //查询姓名
    @Select("select nickname from xglm_admin where username=#{username}")
    String selectNickname(String username);

}
