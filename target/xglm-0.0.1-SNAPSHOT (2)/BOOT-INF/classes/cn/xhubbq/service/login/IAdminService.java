package cn.xhubbq.service.login;

import cn.xhubbq.pojo.XglmAdmin;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

public interface IAdminService extends UserDetailsService {
    //账号密码是否存在
    int adminExist(Map<String,Object> map);

    List<XglmAdmin> selectAlladmins();//所有admins

    List<XglmAdmin> selectAlladminsBypage(int curPS,int pageSize);

    int adminsNum();

    String adminMail(String username);

    int updatePwd(String username,String newpwd);

    //增加管理员
    int addAdmin(int id,String username,String userpwd,String nickname);
    //最大id
    int maxId();

    int addRole(int userid,int roleid);

    int addEmail(String username,String useremail);

    String selectNickname(String username);
}
