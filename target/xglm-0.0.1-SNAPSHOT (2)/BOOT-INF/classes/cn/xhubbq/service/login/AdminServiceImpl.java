package cn.xhubbq.service.login;

import cn.xhubbq.mapper.login.AdminMapper;
import cn.xhubbq.mapper.login.RoleMapper;
import cn.xhubbq.pojo.XglmAdmin;
import cn.xhubbq.pojo.XglmRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements IAdminService{

    @Autowired
    //@Qualifier(value="adminMapper")
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Override
    public int adminExist(Map<String,Object> map) {
        return adminMapper.selectAdminByIdPwd(map);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            XglmAdmin admin =adminMapper.loadAdminByAdminname(username);
            if(admin==null) {
                throw new UsernameNotFoundException("用户名错误，或不存在!");
                //return null;
            }
            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            List<XglmRole> list = roleMapper.findRoleByAdminId(admin.getId());
            for(XglmRole role : list) {
                authorities.add(new SimpleGrantedAuthority(role.getName()));
            }
            //封装 SpringSecurity  需要的UserDetails 对象并返回
            // 加密
            String encodedPassword = passwordEncoder.encode(admin.getUserpwd().trim());
            admin.setUserpwd(encodedPassword);
            for (SimpleGrantedAuthority authority : authorities) {
                System.out.println(authority.getAuthority());
            }
            UserDetails userDetails = new User(admin.getUsername(),admin.getUserpwd(),authorities);
            return userDetails;
        } catch (Exception e) {
            e.printStackTrace();
            //返回null即表示认证失败
            throw new UsernameNotFoundException("错误,授权失败!");
            //return null;
        }
    }

    @Override
    public List<XglmAdmin> selectAlladmins() {
        return adminMapper.selectAlladmins();
    }

    @Override
    public List<XglmAdmin> selectAlladminsBypage(int curPS, int pageSize) {
        return adminMapper.selectAlladminsBypage(curPS,pageSize);
    }

    @Override
    public int adminsNum() {
        return adminMapper.adminsNum();
    }

    @Override
    public String adminMail(String username) {
        return adminMapper.adminMail(username);
    }

    @Override
    public int updatePwd(String username, String newpwd) {
        return adminMapper.updatePwd(username,newpwd);
    }

    @Override
    public int addAdmin(int id, String username, String userpwd, String nickname) {
        return adminMapper.addAdmin(id,username,userpwd,nickname);
    }

    @Override
    public int maxId() {
        return adminMapper.maxId();
    }

    @Override
    public int addRole(int userid, int roleid) {
        return adminMapper.addRole(userid,roleid);
    }

    @Override
    public int addEmail(String username, String useremail) {
        return adminMapper.addEmail(username,useremail);
    }

    @Override
    public String selectNickname(String username) {
        return adminMapper.selectNickname(username);
    }
}
