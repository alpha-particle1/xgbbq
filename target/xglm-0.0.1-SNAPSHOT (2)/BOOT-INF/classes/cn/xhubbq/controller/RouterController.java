package cn.xhubbq.controller;

import cn.xhubbq.mapper.login.RoleMapper;
import cn.xhubbq.pojo.Admin;
import cn.xhubbq.pojo.XglmAdmin;
import cn.xhubbq.pojo.XglmRole;
import cn.xhubbq.pojo.page.AdminPage;
import cn.xhubbq.service.login.IAdminService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/8
 * Description：页面路由
 */
@Controller
public class RouterController {
    private static final int PAGE_SIZE=8;
    @Autowired
    private IAdminService adminService;
    @Autowired
    private RoleMapper roleMapper;

    @CrossOrigin
    @ResponseBody
    @PostMapping("/check")
    public String check(String username,String userpwd){
        Map<String,Object> m=new HashMap<>();
       if(username!=null&&userpwd!=null){
           Map<String,Object> map=new HashMap<>();
           map.put("username",username);
           map.put("userpwd",userpwd);
           int count=adminService.adminExist(map);

           if(count==1){
               m.put("state",1);
               m.put("status","successful");
               return JSONObject.toJSONString(m);
           }else{
               m.put("state",0);
               m.put("status","error");
               return JSONObject.toJSONString(m);
           }
       }
        m.put("state",0);
        m.put("status","error");
        return JSONObject.toJSONString(m);
    }

    @RequestMapping("/")
    public String root(HttpSession session){
        if(session.getAttribute("userName")!=null){
            return "index";
        }else{
            return "redirect:/login";
        }
    }
    @RequestMapping("/resetpwd")
    public String resetpwd(HttpSession session){
        if(session.getAttribute("userName")!=null){
            return "reset";
        }else{
            return "redirect:/login";
        }
    }
    @RequestMapping("/resetpwdPage")
    public String resetpwdPage(HttpSession session){
        if(session.getAttribute("userName")!=null){
            return "resetpwdPage";
        }else{
            return "redirect:/login";
        }
    }
    @PostMapping("/resetpassword")
    public String resetpassword(HttpSession session,@RequestParam("newpwd2") String newpwd){
        if(session.getAttribute("userName")!=null){
            //拿到账号密码
            String username= (String) session.getAttribute("userName");
            String newpasspword=newpwd;
            System.out.println(newpasspword);
            if(newpasspword!=null){
                adminService.updatePwd(username,newpasspword);
                session.invalidate();
                return "redirect:/login";
            }
            return "error";

        }else{
            return "redirect:/login";
        }
    }

    @RequestMapping("/admin/user")
    public String user(){

        return "user";
    }

    @RequestMapping("/403")
    public String f403(){
        return "403";
    }

    @RequestMapping("/administrators/{curPage}")
    public String administrators(@PathVariable int curPage,HttpSession session, Model model){
        if(session.getAttribute("userName")!=null) {
            AdminPage adminPage=new AdminPage();
            adminPage.setCurrentpage(curPage);
            adminPage.setTotalcount(adminService.adminsNum());
            adminPage.setPagesize(PAGE_SIZE);

            List<XglmAdmin> xglmAdmins = adminService.selectAlladminsBypage(curPage*PAGE_SIZE,PAGE_SIZE);
            List<Admin> admins=new ArrayList<>();
            for (XglmAdmin xglmAdmin : xglmAdmins) {
                List<XglmRole> roles = roleMapper.findRoleByAdminId(xglmAdmin.getId());
                admins.add(new Admin(xglmAdmin.getId(),xglmAdmin.getUsername(),xglmAdmin.getUserpwd(),xglmAdmin.getNickname(),roles.get(0).getId()));
            }
            adminPage.setAdmins(admins);
            model.addAttribute("adminsPage",adminPage);
            return "administrators";
        }
        else{
            return "login";
        }
    }
    //网站日志
    @RequestMapping("/logs")
    public String logs(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "logs";
        else{
            return "login";
        }
    }

    //留言页面
    @RequestMapping("/message")
    public String message(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "message";
        else{
            return "login";
        }
    }

    @RequestMapping("/friendlinks")
    public String friendlinks(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "friendlinks";
        else{
            return "login";
        }
    }

    @PostMapping("/insertadmin")
    public String insert(String username,String nickname2,String useremail,int admin2,String userpassword,HttpSession session){
        if(session.getAttribute("userName")!=null) {
            int count=0;
            if(username!=null && useremail!=null && userpassword!=null && nickname2!=null){
                //插入 xglm_admin
                int id = adminService.maxId()+1;
                if(adminService.addAdmin(id,username,userpassword,nickname2)==1){
                    if( adminService.addRole(id,admin2)==1){
                        count=adminService.addEmail(username,useremail);
                        if(count==1){
                            return "redirect:/administrators/0";
                        }
                    }
                }
            }
            return "redirect:/administrators/0";
        }
        else{
            return "login";
        }
    }

}
