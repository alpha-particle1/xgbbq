package cn.xhubbq.controller;

import cn.xhubbq.utils.UploadUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/6
 * Description：文件上传
 */
@RestController
public class UploadController {
    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
    @PostMapping("/upload")
    public String upload( HttpServletRequest request, HttpSession session){
        List<MultipartFile> files;
        MultipartHttpServletRequest req= (MultipartHttpServletRequest) request;
        files=req.getFiles("files");
        //模拟用户id
        String userid="1016";
        //准备把文件上传到upload目录下,还有子目录,upload/tiezi/userid/时间/
        String realPath= session.getServletContext().getRealPath("/upload");
        realPath += File.separator+"tiezi";
        realPath += File.separator+userid;
        realPath += File.separator+sdf.format(new Date())+File.separator;
        UploadUtil.makeFolder(realPath);
        File folder=new File(realPath);
        System.out.println(realPath);
        return UploadUtil.uploadFiles(files,folder);


    }
}
