package cn.xhubbq.controller;

import cn.xhubbq.pojo.XglmAdmin;
import cn.xhubbq.service.login.IAdminService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class Test {

    @Autowired
    private IAdminService adminService;

    @ResponseBody
    @RequestMapping("/login2")
    public String test(){
        HashMap<String,Object>map=new HashMap<>();
        map.put("userid","20021016");
        map.put("userpwd","123456");
        int is = adminService.adminExist(map);
        System.out.println(is);
        String json=JSONObject.toJSONString(is);
        return json;
    }

    @ResponseBody
    @RequestMapping("/test2")
    public String test0(Model model){
        List<XglmAdmin> xglmAdmins = adminService.selectAlladmins();
        model.addAttribute("admins",xglmAdmins);
        String s = JSONObject.toJSONString(xglmAdmins);
        return s;
    }
}
