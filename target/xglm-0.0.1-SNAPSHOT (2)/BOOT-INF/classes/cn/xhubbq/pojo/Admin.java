package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/10
 * Description：admin
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Admin implements Serializable {
    private int id;
    private String username;
    private String userpwd;
    private String nickname;
    private int roleid;
}
