package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XglmAdmin {
    private int id;
    private String username;
    private String userpwd;
    private String nickname;
}
