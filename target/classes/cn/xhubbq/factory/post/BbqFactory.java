package cn.xhubbq.factory.post;

import cn.xhubbq.pojo.post.Bbq;
import cn.xhubbq.pojo.post.Post;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public class BbqFactory implements PostFactory{

    @Override
    public Post make() {
        return new Bbq();
    }
}
