package cn.xhubbq.factory.post;

import cn.xhubbq.pojo.post.Post;
import cn.xhubbq.pojo.post.Xyq;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public class XyqFactory implements PostFactory{

    @Override
    public Post make() {
        return new Xyq();
    }
}
