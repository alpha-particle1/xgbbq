package cn.xhubbq.factory.post;

import cn.xhubbq.pojo.post.Post;
import cn.xhubbq.pojo.post.Tzsc;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public class TzscFactory implements  PostFactory{


    @Override
    public Post make() {
        return new Tzsc();
    }
}
