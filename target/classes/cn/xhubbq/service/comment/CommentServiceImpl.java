package cn.xhubbq.service.comment;

import cn.xhubbq.pojo.comment.Comment;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
@Service
public class CommentServiceImpl implements  ICommentService{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public int comment(Comment comment) {
        Comment insert = mongoTemplate.insert(comment);
        if(insert==null){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteComment(String id) {
        Query query=new Query();
        query.addCriteria(Criteria.where("id").is(id));
        DeleteResult commentdel = mongoTemplate.remove(query, "comment");
        if(commentdel.getDeletedCount()==0){
            return 0;
        }
        return 1;
    }

    @Override
    public long countCommentByPostId(String postId) {
        Query query=new Query();
        query.addCriteria(Criteria.where("postId").is(postId));
        return mongoTemplate.count(query,Comment.class);
    }

    @Override
    public List<Comment> getAllCommentByPagePostId(String postId, int curPage, int pageSize) {
        Query query=new Query();
        query.addCriteria(Criteria.where("postId").is(postId));
        return mongoTemplate.find(query.skip((curPage-1)*pageSize).limit(pageSize),Comment.class);
    }

    @Override
    public List<Comment> getAllCommentByPostId(String postId) {

        return mongoTemplate.find(new Query().addCriteria(Criteria.where("postId").is(postId)),Comment.class);
    }
}
