package cn.xhubbq.service.comment;

import cn.xhubbq.pojo.comment.Comment;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
public interface ICommentService {
    //评论
    int comment(Comment comment);
    //删除评论
    int deleteComment(String id);
    //点赞

    //获取postid的评论的数量
    long countCommentByPostId(String postId);
    //分页查询
    List<Comment> getAllCommentByPagePostId(String postId,int curPage, int pageSize);

    List<Comment> getAllCommentByPostId(String postId);
    //
}
