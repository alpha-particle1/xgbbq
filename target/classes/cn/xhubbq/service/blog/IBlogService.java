package cn.xhubbq.service.blog;

import cn.xhubbq.pojo.blog.Blog;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/25
 * Description：
 */
public interface IBlogService {
    //添加博客信息到MongoDB
    Blog insertBlog(Blog blog);

    //分页查找
    List<Blog> selectBlogPageByAuthor(String author,long curPage,int pageSize);

    //某人博客的总数
    long countBlog(String author);
}
