package cn.xhubbq.service.blog;

import cn.xhubbq.pojo.blog.Blog;
import cn.xhubbq.pojo.letter.Letter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/25
 * Description：
 */
@Service
public class BlogServiceImpl implements IBlogService{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Blog insertBlog(Blog blog) {
        return mongoTemplate.insert(blog);
    }

    @Override
    public List<Blog> selectBlogPageByAuthor(String author,long curPage,int pageSize) {
        Query query=new Query(Criteria.where("upAuthor").is(author));
        return mongoTemplate.find(query.skip((curPage-1)*pageSize).limit(pageSize), Blog.class);
    }

    @Override
    public long countBlog(String author) {
        Query query=new Query(Criteria.where("upAuthor").is(author));
        return mongoTemplate.count(query,Blog.class);
    }
}
