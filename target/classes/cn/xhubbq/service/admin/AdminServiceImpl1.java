package cn.xhubbq.service.admin;

import cn.xhubbq.mapper.xgUser.UserMapper;
import cn.xhubbq.pojo.XgUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/27
 * Description：
 */
@Service
public class AdminServiceImpl1 implements IAdminService1 {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<XgUser> selectAllUsers() {
        return userMapper.selectAllUsers();
    }

    @Override
    public int updateUserStatus(Integer id, int status) {
        return userMapper.updateUserStatus(id, status);
    }

    @Override
    public int updateUserQuan(Integer id, int identify) {
        return userMapper.updateUserQuan(id,identify);
    }

    @Override
    public List<XgUser> selectAllUsersBypage(long curPage, int pSize) {
        return userMapper.selectAllUsersBypage(curPage,pSize);
    }

    @Override
    public int countUser() {
        return userMapper.countUser();
    }

    @Override
    public int updateUser3(String id, int status, int identify, int gender) {
        int version=userMapper.watch(id);
        return userMapper.updateUser3(id,status,identify,gender,version);
    }

    @Override
    public List<XgUser> selectAllUsersBypageStatus(long curPage, int pSize, int status) {
        return userMapper.selectAllUsersBypageStatus(curPage, pSize, status);
    }

    @Override
    public int countUserwithStatus(int status) {
        return userMapper.countUserwithStatus(status);
    }

    @Override
    public int countUserLike(String key) {
        return userMapper.countUserLike(key);
    }

    @Override
    public List<XgUser> selectAllUsersBypageLike(long curPage, int pSize, String key) {
        return userMapper.selectAllUsersBypageLike(curPage, pSize, key);
    }
}
