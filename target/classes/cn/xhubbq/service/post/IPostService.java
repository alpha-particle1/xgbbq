package cn.xhubbq.service.post;

import cn.xhubbq.pojo.post.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public interface IPostService {

    int savePostInMongoDb(Post post);

    //查询bbq总数
    long countBbq();
    //查询xyq总数
    long countXyq();
    //查询swzl总数
    long countSwzl();
    //查询tzsc总数
    long countTzsc();
    //分页查询
    List<Bbq> selectBbqBypage(long curPage,int pageSize);
    List<Xyq> selectXyqBypage(long curPage, int pageSize);
    List<Swzl> selectSwzlBypage(long curPage, int pageSize);
    List<Tzsc> selectTzscBypage(long curPage, int pageSize);

    //删除
    int deleteFromBbq(String postId);
    int deleteFromXyq(String postId);
    int deleteFromSwzl(String postId);
    int deleteFromTzsc(String postId);

    //点赞记录
    int addLikeNote(Like like);
    //取消点赞
    boolean deleteLikeNote(String postId,Integer uid);
    //是否点过赞
    boolean userIsInLike(String postId,Integer uid);
    //是否送过礼物
    boolean userIsInGift(String postId,Integer uid);
    //送礼记录
    int addGiftNote(Gift gift);
    //更新送礼记录
    int updateGiftInfo(String postId,Integer uid,Integer giftNum);
    //获取礼物榜前10
    List<Gift> getTenUserInGift(String postId);

    boolean updateViewNumsInBBq(String postId);
    boolean updateViewNumsInXyq(String postId);
    boolean updateViewNumsInSwzl(String postId);
    boolean updateViewNumsInTzsc(String postId);

    //点赞
    boolean updateLikeInfoBBq(String postId,int num);
    boolean updateLikeInfoXyq(String postId,int num);
    boolean updateLikeInfoSwzl(String postId,int num);
    boolean updateLikeInfoTzsc(String postId,int num);

    long countNew();
    //分页查询
    List<New> selectNewBypage(long curPage,int pageSize);

    //分页查询 某人发布的帖子
    //查询bbq总数
    long countBbqByUserId(Integer uid);
    //查询xyq总数
    long countXyqByUserId(Integer uid);
    //查询swzl总数
    long countSwzlByUserId(Integer uid);
    //查询tzsc总数
    long countTzscByUserId(Integer uid);
    //分页查询
    List<Bbq> selectBbqBypageByUserId(long curPage,int pageSize,Integer uid);
    List<Xyq> selectXyqBypageByUserId(long curPage, int pageSize,Integer uid);
    List<Swzl> selectSwzlBypageByUserId(long curPage, int pageSize,Integer uid);
    List<Tzsc> selectTzscBypageByUserId(long curPage, int pageSize,Integer uid);

    List<Swzl> selectSwzl10();

    //查询tzsc总数
    long countSwzl(String like);
    //分页查询
    public List<Swzl> selectSwzlBypage(long curPage, int pageSize, String like);
    //查询tzsc总数
    long countBbq(String like);
    //分页查询
    public List<Bbq> selectBbqBypage(long curPage, int pageSize, String like);
    //查询tzsc总数
    long countXyq(String like);
    //分页查询
    public List<Xyq> selectXyqBypage(long curPage, int pageSize, String like);
    //查询tzsc总数
    long countTzsc(String like);
    //分页查询
    public List<Tzsc> selectTzscBypage(long curPage, int pageSize, String like);

    //posts
    void posts( String content, int type, int nums, HttpServletRequest request);

}
