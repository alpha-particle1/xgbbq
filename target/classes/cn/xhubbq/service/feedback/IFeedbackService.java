package cn.xhubbq.service.feedback;

import cn.xhubbq.pojo.feedback.FeedBack;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
public interface IFeedbackService {
    //写入mongo
    int insertIntoMongo(FeedBack feedBack);

    //已处理
    int updateFeedBack(String id,int status);
}
