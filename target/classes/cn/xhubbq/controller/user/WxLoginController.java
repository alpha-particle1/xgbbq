package cn.xhubbq.controller.user;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.config.SnowflakeConfig;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.service.xguser.UserServiceImpl;
import cn.xhubbq.service.xguser.WxLoginServiceImpl;
import cn.xhubbq.utils.*;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Author:甲粒子
 * Date: 2021/11/11
 * Description：微信登录
 */

@RestController
public class WxLoginController {

    @Autowired
    RedisUtil redisUtil;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    RedisUtil redis;
    @Autowired
    SnowflakeConfig snowflakeConfig;//雪花算法
    @Autowired
    private WxLoginServiceImpl wxLoginService;
    @Autowired
    private UserServiceImpl uService;

    //微信小程序id和密码  已经封装到常量池中
    private final static String AppID="wxa9cd98b302c457ea";
    private final static String AppSecret="28336de43f57fb2ebb4eb1765c5dc6c5";
    private final  static String WX_REQUEST = "https://api.weixin.qq.com/sns/jscode2session?";

    @Deprecated
    //@RequestMapping("/wxLogin")
    //@InterfaceLimitRequest(count = 1)
    public String wxLogin(@RequestBody String data){//请求数据
        Map<String,Object>resultmap=new HashMap<>();//返回值
        ObjectMapper mapper=new ObjectMapper();
        ObjectNode node = null;
        try {
            //1、获取前端传来的code
            //data是json字符串
            node = new ObjectMapper().readValue(data, ObjectNode.class);
            Map<String,String> params = new HashMap<>();
            params.put("appid", AppID);
            params.put("secret",AppSecret);
            if (node.has("code")) {
                params.put("js_code",node.get("code").toString().replace("\"", ""));
            }
            // params.put("js_code",);
            params.put("grant_type","authorization_code");
            //2、向微信服务器发送请求
            String wxRequestResult = null;
            wxRequestResult = HttpClientUtil.doGet(WX_REQUEST,params);
            //3、将openid 与 session_key 加密
            //4、将openid 与 session_key存入（缓存）数据库 --这里未处理
            //5、将openid 与 session_key封装好返回给前端，前端做缓存
            ObjectNode node2 = new ObjectMapper().readValue(wxRequestResult, ObjectNode.class);
            String session_key=node2.get("session_key").toString();
            String openid=node2.get("openid").toString();


            //密文解密,放入数据库
            String decrypt = AesCbcUtil.decrypt(node.get("encryptedData").toString(), session_key, node.get("iv").toString(), "utf-8");
            ObjectNode n = new ObjectMapper().readValue(decrypt, ObjectNode.class);
            String nickName = n.get("nickName").toString();
            int gender=Integer.parseInt(n.get("gender").toString());
            String headpic=n.get("avatarUrl").toString();

            //判断是否是第一次登录
            //先查询redis数据库
            boolean flag=wxLoginService.userIsInRedis("loginUsers",openid);
            //如果不在redis中：flag=false，就查询mysql
            if(!flag) {
                XgUser xgUser = wxLoginService.selectUser(openid);
                //是xgUser==null 代表是第一次登录
                if(xgUser==null){
                    System.out.println("该用户第一次登录");
                    xgUser=new XgUser();
                    //Integer maxId=-1;//这个最大id支持 199999999
                    Object rs=null;
                    Integer uid=generateId();
                    while(redisUtil.sHasKey("userIds", uid)){
                        uid=generateId();
                    }
                    long userId = redisUtil.sSet("userIds", uid);
                    if(userId<=0){
                        resultmap.put("state",0);
                        resultmap.put("info","error");
                        return JSONObject.toJSONString(resultmap);
                    }
                    xgUser.setId(uid);
                    xgUser.setUserid(openid);
                    xgUser.setNickname(nickName);
                    xgUser.setGender(gender);
                    xgUser.setHeadpic(headpic);
                    xgUser.setIdentify(0);
                    xgUser.setCtime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                    xgUser.setLtime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                    xgUser.setStatus(1);
                    xgUser.setFans(0);
                    xgUser.setAttention(0);
                    //wxLoginService.addUser(xgUser);
                    //向redis中写入User
                    boolean x=wxLoginService.addUserInRedis("loginUsers",openid,xgUser,604800);
                    boolean y=false;
                    if(x){
                        //y=wxLoginService.addUsertoNewUsers("newUsers",openid,xgUser,604800);
                        int cnt = wxLoginService.addUser(xgUser);
                        if(cnt!=0){
                            y=true;
                            boolean b = uService.addAccount(xgUser.getId(), new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()),
                                    999, 0, 1);
                            if (!b){
                                resultmap.put("info","首充失败");
                            }
                        }
                    }
                    if(!y){
                        resultmap.put("state",0);
                        resultmap.put("token","error");
                        return JSONObject.toJSONString(resultmap);
                    }

                    Map<String,String>map=new HashMap<>();
                    map.put("userOpenId",openid);
                    map.put("userGender",gender+"");
                    String token = JWTUtil.createToken(map);
                    resultmap.put("state",1);
                    resultmap.put("token",token);
                    //resultmap.put("id",maxId+1);
                    resultmap.put("id",uid);
                    //充值
                   // Integer add = uService.addMoney(maxId + 1, 9999);
                    Integer add = uService.addMoney(uid, 9999);
                    if(add>0){
                        resultmap.put("money",9999);
                    }
                    return JSONObject.toJSONString(resultmap);
                }
                else{//不是
                    //System.out.println("最大Id:"+redisUtil.get("maxUserId"));
                    //更新最近登陆时间
                    String newTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
                    wxLoginService.updateLastTime(newTime,openid);
                    //从mysql中取出数据并写入redis
                    XgUser u= wxLoginService.selectUser(openid);
                    boolean y=false;
                    if(u!=null){
                        y=redisUtil.hset("loginUsers",openid,u,604800);
                    }else{
                        resultmap.put("state",0);
                        resultmap.put("token","error");
                        return JSONObject.toJSONString(resultmap);
                    }
                    if(y==false){
                        resultmap.put("state",0);
                        resultmap.put("token","error");
                        return JSONObject.toJSONString(resultmap);
                    }
                    Map<String,String>map=new HashMap<>();
                    map.put("userOpenId",openid);
                    map.put("userGender",gender+"");
                    String token = JWTUtil.createToken(map);

                    resultmap.put("state",1);
                    resultmap.put("token",token);
                    resultmap.put("id",u.getId());
                    //DecodedJWT tokenPayload = JWTUtil.getTokenPayload(token);
//                    System.out.println(tokenPayload.getClaim("userOpenId").asString());
//                    System.out.println(tokenPayload.getClaim("userGender").asString());
                    return JSONObject.toJSONString(resultmap);
                }
            }
            else{//不是
                //更新最近登陆时间
                String newTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
                wxLoginService.updateLastTime(newTime,openid);
                //更新redis中的值
                XgUser u=(XgUser)redisUtil.hget("loginUsers",openid);
                u.setLtime(newTime);
                redisUtil.hset("loginUsers",openid,u,604800);
                Map<String,String>map=new HashMap<>();
                map.put("userOpenId",openid);
                map.put("userGender",gender+"");
                String token = JWTUtil.createToken(map);

                resultmap.put("state",1);
                resultmap.put("token",token);
                resultmap.put("id",u.getId());
                //DecodedJWT tokenPayload = JWTUtil.getTokenPayload(token);
//                System.out.println(tokenPayload.getClaim("userOpenId").asString());
//                System.out.println(tokenPayload.getClaim("userGender").asString());
                return JSONObject.toJSONString(resultmap);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        } catch (IOException e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        } catch (KeyManagementException e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        } catch (Exception e) {
            e.printStackTrace();
            resultmap.put("state",0);
            resultmap.put("token","error");
            return JSONObject.toJSONString(resultmap);
        }
    }

    @ResponseBody
    @PostMapping("/wxLogin")
    public String wxLogin(@RequestBody String data, HttpServletRequest request) throws JsonProcessingException {
        String token = request.getHeader("token");
        //携带code iv encryptedData
        ObjectNode node = null;
        //微信服务器返回的数据有sessionKey和openId
        ObjectNode dataNode = null;

        try{
            node = new ObjectMapper().readValue(data, ObjectNode.class);
            String _data_ = uService.requestWX(data, node, new HashMap<>());
            System.out.println(_data_);
            dataNode = uService.resolveData(_data_);

            String openId = dataNode.get("openid").toString();
            System.out.println(openId);
            if(token == null || "".equals(token)){
                //不是第一次登录
                XgUser user = wxLoginService.selectUser(openId);
                if (user != null) {
                    //更新登录时间并返回新token
                    if (RespStateUtil.isSuccessfully(wxLoginService.updateLastTime(TimeUtil.getNowTime(),openId))){
                        Integer userId = user.getId();
                        String newToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                        Map<String,Object> returnMap = new HashMap<>();
                        returnMap.put("state",1);
                        returnMap.put("id",userId);
                        returnMap.put("token",newToken);
                        return JSONObject.toJSONString(returnMap);
                    }
                    return RespStateUtil.state0(new HashMap<>());
                }

                //是第一次登录

                //node指向解密后的数据节点
                //包含昵称等信息
                node = uService.resolveEncryptedData(node,dataNode);
                //新建用户
                XgUser xgUser = new XgUser();
                xgUser.setNickname(node.get("nickName").toString());
                xgUser.setGender(Integer.parseInt(String.valueOf(node.get("gender"))));
                xgUser.setHeadpic(node.get("avatarUrl").toString());
                xgUser.setCtime(TimeUtil.getNowTime());
                xgUser.setLtime(TimeUtil.getNowTime());
                xgUser.setUserid(openId);
                //生成userId
                Integer userId = 0;
                do {
                    userId = IdUtil.generateUserId();
                }while (redis.getBit("userIds",userId));//自旋
                redis.setBit("userIds",userId,true);
                xgUser.setId(userId);


                try{
                    //新用户及账户创建成功
                    wxLoginService.addUser(xgUser);
                    uService.addAccount(xgUser.getId(), new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()),
                            999, 0, 1);
                    //返回token
                    String userToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                    Map<String,Object> returnMap = new HashMap<>();
                    returnMap.put("state",1);
                    returnMap.put("id",userId);
                    returnMap.put("token",userToken);
                    return JSONObject.toJSONString(returnMap);
                }catch (Exception e){
                    return RespStateUtil.state0(new HashMap<>());
                }

            }
            //不是第一次登录
            else {
                //更新登录时间并返回新token
                XgUser user = wxLoginService.selectUser(openId);
                Integer userId = user.getId();
                if (RespStateUtil.isSuccessfully(wxLoginService.updateLastTime(TimeUtil.getNowTime(),openId))){
                    String newToken = JWTUtil.doCreateToken(new HashMap<>(), userId);
                    Map<String,Object> returnMap = new HashMap<>();
                    returnMap.put("state",1);
                    returnMap.put("token",newToken);
                    returnMap.put("id",userId);
                    return JSONObject.toJSONString(returnMap);
                }
                return RespStateUtil.state0(new HashMap<>());
            }

        }catch (Exception e){
            e.printStackTrace();
            return RespStateUtil.stateError(new HashMap<>());
        }

    }


    //生成id
    public static Integer generateId(){
        long id = SnowflakeIdWorker.generateId();
        StringBuilder sb=new StringBuilder(id+"");
        StringBuilder reverse = sb.reverse();
        id=new Long(reverse.toString())/1000;
        while(id>1999999999){
            id/=10;
        }
        Integer id1 = Integer.parseInt(id+"");
        return id1;
    } 
}
