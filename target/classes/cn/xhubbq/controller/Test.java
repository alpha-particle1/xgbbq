package cn.xhubbq.controller;
import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.factory.post.SwzlFactory;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.pojo.XglmAdmin;
import cn.xhubbq.pojo.page.UserPage;
import cn.xhubbq.pojo.post.Swzl;
import cn.xhubbq.service.admin.IAdminService;
import cn.xhubbq.service.post.IPostService;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.utils.RedisUtil;
import cn.xhubbq.utils.SnowflakeIdWorker;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.*;

//小小测试： 测试要编辑的接口

@Controller
public class Test {

    @Autowired
    private IAdminService adminService;
    @Autowired
    private RedisUtil redisUtil;

    @ResponseBody
    @RequestMapping("/login2")
    public String test(){
        HashMap<String,Object>map=new HashMap<>();
        map.put("userid","20021016");
        map.put("userpwd","123456");
        int is = adminService.adminExist(map);
        System.out.println(is);
        String json=JSONObject.toJSONString(is);
        return json;
    }

    @ResponseBody
    @RequestMapping("/test2")
    public String test0(Model model){
        List<XglmAdmin> xglmAdmins = adminService.selectAlladmins();
        model.addAttribute("admins",xglmAdmins);
        String s = JSONObject.toJSONString(xglmAdmins);
        return s;
    }

    @ResponseBody
    @RequestMapping(value="/test3",produces = "text/plain;charset=UTF-8")
    public String test3(){

        Map<String,Object> map=new HashMap<>();
        map.put("userid",1999989);
        map.put("username","小米");
        map.put("sex",1);
        map.put("headpic","https://www.xhubbq.cn/index/");
        map.put("status",1);
        redisUtil.hmset("user",map);
        Map<Object, Object> user = redisUtil.hmget("user");
        user.forEach((k,v)->{
            //System.out.println(v);
        });
        return JSONObject.toJSONString(user);
    }

    @ResponseBody
    @RequestMapping(value="/test5",produces = "text/plain;charset=UTF-8")
    public String test5(){

        XgUser user=new XgUser();
        user.setUserid("wxadadq");
        user.setId(1999989);
        user.setNickname("小米");
        user.setGender(0);
        user.setHeadpic("https://www.xhubbq.cn/index/");
        user.setStatus(1);
        redisUtil.sSet("users",user);
        XgUser user1=new XgUser();
        user1.setUserid("wxadadq");
        user1.setId(1999999);
        user1.setNickname("小翟");
        user1.setGender(1);
        user1.setHeadpic("https://www.xhubbq.cn/index/");
        user1.setStatus(1);
        redisUtil.sSet("users",user1);
        Set<Object> user12 = redisUtil.sGet("users");
        boolean users = redisUtil.sHasKey("users", user1);
        System.out.println(users);
        return JSONObject.toJSONString(user12);
    }

    @ResponseBody
    @RequestMapping("/test6")
    public String test6(){
        redisUtil.sSetAndTime("loginUsers",604800,"openid1");
        redisUtil.sSetAndTime("loginUsers",604800,"openid2");
        System.out.println(redisUtil.sHasKey("loginUsers","openid1"));
        Set<Object> loginUsers = redisUtil.sGet("loginUsers");
        return JSONObject.toJSONString(loginUsers);
    }

    @ResponseBody
    @RequestMapping(value="/test7",produces="text/plain;charset=UTF-8")
    public String test7(){
        XgUser user1=new XgUser();
        user1.setUserid("openid1");
        user1.setId(1999);
        user1.setNickname("小翟");
        user1.setHeadpic("https://www.");
        redisUtil.hset("loginUsers",user1.getUserid(),user1,604800);
        XgUser hget = (XgUser)redisUtil.hget("loginUsers", "openid1");
        hget.setNickname("小米");
        redisUtil.hset("users", user1.getUserid(),hget, 604800);
        hget=(XgUser)redisUtil.hget("loginUsers", "openid1");
        boolean b = redisUtil.hHasKey("loginUsers", "openid1");
        System.out.println(b);
        return JSONObject.toJSONString(hget);
    }

    @ResponseBody
    @RequestMapping(value="/test8",produces="text/plain;charset=UTF-8")
    public String test8(){
        List<Object> newUsers = redisUtil.hgetAll("newUsers");
        for (Object newUser : newUsers) {
            System.out.println((XgUser)newUser);
        }
        return JSONObject.toJSONString(newUsers);
    }
    @ResponseBody
    @RequestMapping(value="/test9",produces="text/plain;charset=UTF-8")
    public String test9(){
        XgUser user1=new XgUser();
        user1.setUserid("openid1");
        user1.setId(1999);
        user1.setNickname("小翟");
        user1.setHeadpic("https://www.");
        redisUtil.hset("test",user1.getUserid(),user1,604800);
        XgUser hget =(XgUser)redisUtil.hget("test", "openid1");
        //redisUtil.hdel("test","openid1");
        return JSONObject.toJSONString(hget);
    }

    @RequestMapping("/diyBolgs")
    public String blogs(){
        String str="";

        return "/upload/ html/1016/2021/1b2a246395.html";
    }
    @Autowired
    private IPostService postService;
    private static final String prefix="https://img.xhubbq.cn/";
    @ResponseBody
    @RequestMapping("/posts")
    public String posts(){

        Swzl swzl = (Swzl) new SwzlFactory().make();
        swzl.setContent("谁的五毛钱掉了！");
        swzl.setId("dabdjasbjab");
        swzl.setCtime("今天");
        swzl.setStatus(1);
        swzl.setUserid(1);
        String imgUrls=prefix+"FhToz2Qo7X9fvjcrNeRNiws1LV6N"+"|"+prefix+"FjFTP2SO2ZOZ2JdN7rMsupn27E5M|";
        swzl.setImgPath(imgUrls);
        postService.savePostInMongoDb(swzl);
        return "posts";
    }

    @ResponseBody
    @RequestMapping(value = "/limit",produces = "application/json;charset=UTF-8")
    @InterfaceLimitRequest(count = 1)
    public String limit(){
        System.out.println("访问接口");
        return "123";
    }
    @Autowired
    private IUserService userService;
    @ResponseBody
    @RequestMapping(value = "/followtest/{curPage}/{userid}",produces = "application/json;charset=UTF-8")
    public String follows(@PathVariable int curPage,@PathVariable Integer userid){
        UserPage userPage = new UserPage();
        userPage.setCurrentPage(curPage);
        userPage.setTotalCount(userService.countUserFollows(userid));
        userPage.setPageSize(5);
        userPage.setUsers(userService.selectAllUserFollowsBypageStatus(userid,curPage,5,1));
        Map<String,Object> map = new HashMap<>();
        map.put("users",userPage);
        return  JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping(value = "/id",produces = "application/json;charset=UTF-8")
    public String id() {
        Map<String,Object> map = new HashMap<>();
        Integer uid = generateId();
        while (redisUtil.sHasKey("userIds", uid)) {
            uid = generateId();
        }
        redisUtil.sSet("userIds",uid);
        System.out.println("id :"+uid);
        map.put("uid",uid);
        return JSONObject.toJSONString(map);
    }




    public static Integer generateId(){
        long id = SnowflakeIdWorker.generateId();
        StringBuilder sb=new StringBuilder(id+"");
        StringBuilder reverse = sb.reverse();
        id=new Long(reverse.toString())/1000;
        while(id>1999999999){
            id/=10;
        }
        Integer id1 = Integer.parseInt(id+"");
        return id1;
    }

}
