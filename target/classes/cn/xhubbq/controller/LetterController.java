package cn.xhubbq.controller;

import cn.hutool.extra.ssh.JschRuntimeException;
import cn.xhubbq.pojo.letter.Letter;
import cn.xhubbq.pojo.page.LetterPage;
import cn.xhubbq.service.letter.LetterServiceImpl;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/24
 * Description：
 */
@RestController
public class LetterController {
    @Autowired
    private LetterServiceImpl letterService;

    private static final int PAGE_SIZE=5;

    @ResponseBody
    @RequestMapping("/findLetters")
    public List<Letter> findLetter(){
        return letterService.selectAllLetters();
    }

    //信件 - 分页
    @ResponseBody
    @RequestMapping("/letterPage/{curPage}")
    public Object letterPage(@PathVariable int curPage){
        LetterPage letterPage=new LetterPage();
        letterPage.setCurrentpage(curPage);
        letterPage.setTotalcount(letterService.letterCount());//查总数
        letterPage.setPagesize(PAGE_SIZE);

        List<Letter> letters=letterService.selectLettersByPage(curPage,PAGE_SIZE);
        letterPage.setLetters(letters);
        return letterPage;
    }

    @ResponseBody
    @RequestMapping("/deleteLetter/{id}")
    public String deleteLetter(@PathVariable String id){
        Map<String,Object> map=new HashMap<>();
        if(id!=null){
            DeleteResult deleteResult = letterService.deleteLetterById(id);
            if (deleteResult!=null){
                return JSONObject.toJSONString(deleteResult);
            }
            map.put("state",0);
            map.put("info","error");
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        map.put("info","error");
        return JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping("/writeLetter/{letter}/")
    public String writeLetter(@PathVariable String letter){
        Map<String,Object> map = new HashMap<>();
        Letter letter1 = JSONObject.parseObject(letter, Letter.class);
        Letter letter2 = letterService.addLetter(letter1);
        if(letter2==null){
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
        map.put("state",1);
        return JSONObject.toJSONString(map);
    }
}
