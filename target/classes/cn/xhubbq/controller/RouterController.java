package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.mapper.login.RoleMapper;
import cn.xhubbq.pojo.Admin;
import cn.xhubbq.pojo.XglmAdmin;
import cn.xhubbq.pojo.XglmRole;
import cn.xhubbq.pojo.page.AdminPage;
import cn.xhubbq.service.admin.IAdminService;
import cn.xhubbq.service.lunbo.ILunboService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/8
 * Description：页面路由
 */
@Controller
public class RouterController {
    private static final int PAGE_SIZE=8;
    @Autowired
    private IAdminService adminService;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private ILunboService lunboService;


    @CrossOrigin
    @ResponseBody
    @PostMapping("/km/check")
    @InterfaceLimitRequest(count = 1)
    public String check(String username,String userpwd){
        Map<String,Object> m=new HashMap<>();
       if(username!=null&&userpwd!=null){
           Map<String,Object> map=new HashMap<>();
           map.put("username",username);
           map.put("userpwd",userpwd);
           int count=adminService.adminExist(map);

           if(count==1){
               m.put("state",1);
               m.put("status","successful");
               return JSONObject.toJSONString(m);
           }else{
               m.put("state",0);
               m.put("status","error");
               return JSONObject.toJSONString(m);
           }
       }
        m.put("state",0);
        m.put("status","error");
        return JSONObject.toJSONString(m);
    }

    @RequestMapping("/main")
    public String main(HttpServletRequest request){
        //System.out.println(request.getRemoteAddr());
        return "main/index";
    }

    @RequestMapping("/")
    public String root(){
        if(true){
            RequestAttributes ra = RequestContextHolder.getRequestAttributes();
            ServletRequestAttributes sra = (ServletRequestAttributes) ra;
            HttpServletRequest request = sra.getRequest();
            String ip = request.getHeader("X-Forwarded-For");
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("X-Real-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
            System.out.println(ip);
            return "main/index";
        }else{
            return "main/index";
        }
    }

    @RequestMapping("/resetpwd")
    public String resetpwd(HttpSession session){
        if(session.getAttribute("userName")!=null){
            return "reset";
        }else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/resetpwdPage")
    public String resetpwdPage(HttpSession session){
        if(session.getAttribute("userName")!=null){
            return "resetpwdPage";
        }else{
            return "redirect:/km/login";
        }
    }

    @PostMapping("/resetpassword")
    public String resetpassword(HttpSession session,@RequestParam("newpwd2") String newpwd){
        if(session.getAttribute("userName")!=null){
            //拿到账号密码
            String username= (String) session.getAttribute("userName");
            String newpasspword=newpwd;
            //System.out.println(newpasspword);
            if(newpasspword!=null){
                adminService.updatePwd(username,newpasspword);
                session.invalidate();
                return "redirect:/km/login";
            }
            return "error";
        }else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/admin/user")
    public String user(){
        return "user";
    }

    @RequestMapping("/403")
    public String f403(){
        return "403";
    }

    @RequestMapping("/administrators/{curPage}")
    public String administrators(@PathVariable int curPage,HttpSession session, Model model){
        if(session.getAttribute("userName")!=null) {
            AdminPage adminPage=new AdminPage();
            adminPage.setCurrentpage(curPage);
            adminPage.setTotalcount(adminService.adminsNum());
            adminPage.setPagesize(PAGE_SIZE);
            List<XglmAdmin> xglmAdmins = adminService.selectAlladminsBypage(curPage*PAGE_SIZE,PAGE_SIZE);
            List<Admin> admins=new ArrayList<>();
            for (XglmAdmin xglmAdmin : xglmAdmins) {
                List<XglmRole> roles = roleMapper.findRoleByAdminId(xglmAdmin.getId());
                admins.add(new Admin(xglmAdmin.getId(),xglmAdmin.getUsername(),xglmAdmin.getUserpwd(),xglmAdmin.getNickname(),roles.get(0).getId()));
            }
            adminPage.setAdmins(admins);
            model.addAttribute("adminsPage",adminPage);
            return "administrators";
        }
        else{
            return "redirect:/km/login";
        }
    }

    //网站日志
    @RequestMapping("/logs")
    public String logs(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "logs";
        else{
            return "redirect:/km/login";
        }
    }

    //留言页面
    @RequestMapping("/message")
    public String message(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "message";
        else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/friendlinks")
    public String friendlinks(HttpSession session){
        if(session.getAttribute("userName")!=null)
            return "friendlinks";
        else{
            return "redirect:/km/login";
        }
    }

    @PostMapping("/insertadmin")
    public String insert(String username,String nickname2,String useremail,int admin2,String userpassword,HttpSession session){
        if(session.getAttribute("userName")!=null) {
            int count=0;
            if(username!=null && useremail!=null && userpassword!=null && nickname2!=null){
                //插入 xglm_admin
                int id = adminService.maxId()+1;
                if(adminService.addAdmin(id,username,userpassword,nickname2)==1){
                    if( adminService.addRole(id,admin2)==1){
                        count=adminService.addEmail(username,useremail);
                        if(count==1){
                            return "redirect:/administrators/0";
                        }
                    }
                }
            }
            return "redirect:/administrators/0";
        }
        else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/picture_create")
    public String picture_create(){
        return "picture_create";
    }

    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/allImgs",produces = "text/plain;charset=UTF-8")
    public String allImgs(){
        return JSONObject.toJSONString(lunboService.selectAll());
    }

    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/updateLunbo/{a}/{b}/{c}",produces = "text/plain;charset=UTF-8")
    public String updateLunbo(@PathVariable String a, @PathVariable String b,@PathVariable String c,HttpSession session){
        final String prefix="https://";
        Map<String,Object>map=new HashMap<>();
        if(session.getAttribute("nickName")==null){
            map.put("state",-1);
            return  JSONObject.toJSONString(map);
        }
        a=a.replaceAll("1290183704","/");
        b=b.replaceAll("1290183704","/");
        c=c.replaceAll("1290183704","/");
        System.out.println(a+""+b+""+c);
        int x=lunboService.updateLunboImg(1,prefix+a);
        if(x==1){
            int y=lunboService.updateLunboImg(2,prefix+b);
            if(y==1){
                int z=lunboService.updateLunboImg(3,prefix+c);
                if(z==1){
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
            }
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    @RequestMapping("/lunbo2")
    public String liunbo2(){
        return "lunbo2_create";
    }

    //次级图文--轮播图下面两个
    @ResponseBody
    @RequestMapping(value = "/updateLunbo2/{a}/{b}",produces = "text/plain;charset=UTF-8")
    @InterfaceLimitRequest(count = 1)
    public String updateLunbo2(@PathVariable String a, @PathVariable String b,HttpSession session){
        final String prefix="https://";
        Map<String,Object>map=new HashMap<>();
        if(session.getAttribute("nickName")==null){
            map.put("state",-1);
            return  JSONObject.toJSONString(map);
        }
        a=a.replaceAll("1290183704","/");
        b=b.replaceAll("1290183704","/");
        System.out.println(a+""+b+"");
        int x=lunboService.updateLunboImg(4,prefix+a);
        if(x==1){
            int y=lunboService.updateLunboImg(5,prefix+b);
            if(y==1){
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    @RequestMapping("/kmgzs/banner")
    public String banner(){
        return "banner";
    }

}
