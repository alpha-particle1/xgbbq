package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.feedback.FeedBack;
import cn.xhubbq.service.feedback.IFeedbackService;
import cn.xhubbq.utils.QiniuUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 * 暂不使用：腾讯兔小巢免费使用
 */
@Deprecated
@RestController
public class FeedBackController {
    @Autowired
    private IFeedbackService feedbackService;

    @ResponseBody
    @RequestMapping(value = "/feedback/{content}",produces = "application/json;charset=utf-8")
    @InterfaceLimitRequest
    public String feedback(@PathVariable String content, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        if(content!=null){
            FeedBack feedBack = JSONObject.parseObject(content, FeedBack.class);
            //上传文件
            List<MultipartFile> files;
            MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
            files=req.getFiles("imgs");
            Map<String, Object> qiniu = QiniuUtil.qiniu(files);
            if((int)qiniu.get("state")==0){
                map.put("state",0);
                map.put("error","上传失败");
                return  JSONObject.toJSONString(map);
            }
//            if((int)qiniu.get("nums") == 0){
//                map.put("state",0);
//                map.put("error","图片上传出错");
//                return  JSONObject.toJSONString(map);
//            }
            String imgs = (String)qiniu.get("imgs");
            feedBack.setImgs(imgs);
            if(feedBack!=null){
                int i = feedbackService.insertIntoMongo(feedBack);
                if(i==0){
                    map.put("state",0);
                    return JSONObject.toJSONString(map);
                }
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping(value = "/feedback/{id}/{status}",produces = "application/json;charset=utf-8")
    public String feedback(@PathVariable String id,@PathVariable int status){
        Map<String,Object> map = new HashMap<>();
        if(id!=null){
            int i = feedbackService.updateFeedBack(id, status);
            if(i==0){
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            map.put("state",1);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }
}
