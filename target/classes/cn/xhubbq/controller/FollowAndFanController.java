package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.pojo.page.UserPage;
import cn.xhubbq.service.xguser.IUserService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/12/13
 * Description：
 */
@RestController
public class FollowAndFanController {
    private static final String S1="_follows";
    private static final String S2="_fans";
    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Autowired
    private IUserService userService;

    //关注
    @ResponseBody
    @RequestMapping("/followSb/{myid}/{uid}")
    public String followSomeOne(@PathVariable Integer myid,@PathVariable Integer uid){
        //String uid_follow,String f_uid
        Map<String,Object> map = new HashMap<>();
        if(myid!=0 && uid!=0) {
            //userService
            if(userService.noteInFollows(myid,uid)!=1){
                Integer in1 = userService.addAttention(myid, uid, sdf.format(new Date()));
                Integer in2 = userService.addFan(uid, myid, sdf.format(new Date()));
                if(in1!=0 && in2 !=0){
                    //System.out.println("关注成功!");
                    int i1 = userService.incUserFanNum(uid);
                    int i2 = userService.incUserAttentionNum(myid);
                    map.put("state",1);
                    return JSONObject.toJSONString(map);
                }
            }
            map.put("state",-1);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    //取消关注
    @ResponseBody
    @RequestMapping("/excelfollowSb/{myid}/{uid}")
    public String excelfollowSomeOne(@PathVariable Integer myid,@PathVariable Integer uid){
        //String uid_follow,String f_uid
        Map<String,Object> map = new HashMap<>();
        if(myid!=0 && uid!=0) {
           if(userService.noteInFollows(myid,uid)==1){
               boolean in1 = userService.deleteFollowNote(myid, uid);
               boolean in2 = userService.deleteFanNote(uid, myid);
               if (in1 && in2) {
                   //System.out.println("删除成功!");
                   int i1 = userService.decrUserFanNum(uid);
                   int i2 = userService.decrUserAttentionNum(myid);
                   map.put("state", 1);
                   return JSONObject.toJSONString(map);
               }
           }
            map.put("state",-1);//不存在
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    //关注列表
    @ResponseBody
    @RequestMapping(value = "/follows/{userid}/{curPage}",produces = "application/json;charset=UTF-8")
    @InterfaceLimitRequest
    public String follows(@PathVariable int curPage,@PathVariable Integer userid){
        UserPage userPage = new UserPage();
        userPage.setCurrentPage(curPage);
        userPage.setTotalCount(userService.countUserFollows(userid));
        userPage.setPageSize(5);
        userPage.setUsers(userService.selectAllUserFollowsBypageStatus(userid,curPage,5,1));
        Map<String,Object> map = new HashMap<>();
        map.put("users",userPage);
        return  JSONObject.toJSONString(map);
    }

    //粉丝列表
    @ResponseBody
    @RequestMapping(value = "/fans/{userid}/{curPage}",produces = "application/json;charset=UTF-8")
    @InterfaceLimitRequest
    public String fans(@PathVariable int curPage,@PathVariable Integer userid){
        UserPage userPage = new UserPage();
        userPage.setCurrentPage(curPage);
        userPage.setTotalCount(userService.countUserFans(userid));
        userPage.setPageSize(5);
        userPage.setUsers(userService.selectAllUserFansBypageStatus(userid,curPage,5,1));
        Map<String,Object> map = new HashMap<>();
        map.put("users",userPage);
        return  JSONObject.toJSONString(map);
    }

    //查询自己的信息
    @ResponseBody
    @RequestMapping(value = "/selfinfo/{userid}",produces = "application/json;charset=UTF-8")
    @InterfaceLimitRequest
    public String selfInfo(@PathVariable Integer userid){
        Map<String,Object> map = new HashMap<>();
        if (userid!=0){
            XgUser xgUser = userService.selectUserInfoById(userid);
            if (xgUser!=null){
                map.put("state",1);
                map.put("userInfo",xgUser);
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            map.put("userInfo",xgUser);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return  JSONObject.toJSONString(map);
    }

    //是否关注
    @ResponseBody
    @RequestMapping(value = "/checkfollow/{myid}/{uid}",produces = "application/json;charset=UTF-8")
    @InterfaceLimitRequest
    public String checkFollow(@PathVariable Integer myid,@PathVariable Integer uid){
        Map<String,Object> map = new HashMap<>();
        if (myid!=0){
            int i = userService.noteInFollows(myid, uid);
            if(i!=0){
                map.put("state",1);
                map.put("followed",true);
                return JSONObject.toJSONString(map);
            }
            map.put("state",1);
            map.put("followed",false);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

}
