package cn.xhubbq.controller._404;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Author:甲粒子
 * Date: 2022/2/18
 * Description：404
 */
@Controller
public class _404 {

    @RequestMapping("/404")
    public String mode(){
        return "404";
    }
}
