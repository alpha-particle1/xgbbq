package cn.xhubbq.controller.admin;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.im.entity.SocketMessage;
import cn.xhubbq.im.service.message.MessageServiceImpl;
import cn.xhubbq.pojo.XgUser;
import cn.xhubbq.pojo.page.UserPage;
import cn.xhubbq.pojo.post.Bbq;
import cn.xhubbq.pojo.post.Swzl;
import cn.xhubbq.pojo.post.Xyq;
import cn.xhubbq.pojo.post.page.BbqPage;
import cn.xhubbq.pojo.post.page.SwzlPage;
import cn.xhubbq.pojo.post.page.XyqPage;
import cn.xhubbq.service.admin.IAdminService1;
import cn.xhubbq.service.post.IPostService;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.service.xguser.WxLoginServiceImpl;
import cn.xhubbq.utils.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/27
 * Description：管理员操作
 */
@Controller
public class AdminController {
    @Autowired
    private IAdminService1 adminService;
    @Autowired
    private WxLoginServiceImpl wxLoginService;
    @Autowired
    private MessageServiceImpl messageService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IPostService postService;

    private static final int PAGE_SIZE=10;

    @CrossOrigin
    //查看所有用户
    @RequestMapping(value = "/allUsers/{cPage}",produces = "text/plain;charset=UTF-8")
    public String users(HttpSession session, Model model, @PathVariable long cPage){
       String nickName = (String)session.getAttribute("nickName");
       if(nickName!=null){
           UserPage userPage=new UserPage();
           userPage.setCurrentPage(cPage);
           int userNum=adminService.countUser();
           if(userNum!=0){
               userPage.setTotalCount(userNum);
               userPage.setPageSize(PAGE_SIZE);
               List<XgUser> xgUsers = adminService.selectAllUsersBypage(cPage*PAGE_SIZE, PAGE_SIZE);
               userPage.setUsers(xgUsers);
               //userPage.getUsers().get(0).getHeadpic().replace('"',' ');
               model.addAttribute("userPage",userPage);
           }
            return "user";
       }else{
           return "/km/login";
       }
    }


    @RequestMapping("/user_by_status")
    public String by_status(){
        return "user_by_status";
    }

    @RequestMapping("/user_by_like")
    public String by_like(){
        return "user_by_like";
    }

    //查看某种状态下的用户
    @CrossOrigin
    @RequestMapping(value = "/allUsers0/{cPage}/{status}",produces = "text/plain;charset=UTF-8")
    public String users(HttpSession session, Model model, @PathVariable long cPage,@PathVariable int status){
        String nickName = (String)session.getAttribute("nickName");
        if(nickName!=null){
            UserPage userPage=new UserPage();
            userPage.setCurrentPage(cPage);
            int userNum=adminService.countUserwithStatus(status);
            if(userNum!=0){
                userPage.setTotalCount(userNum);
                userPage.setPageSize(PAGE_SIZE);
                List<XgUser> xgUsers = adminService.selectAllUsersBypageStatus(cPage, PAGE_SIZE,status);
                userPage.setUsers(xgUsers);
                //userPage.getUsers().get(0).getHeadpic().replace('"',' ');
                model.addAttribute("userPage",userPage);
                model.addAttribute("status_by",status);
                return "user_by_status";
            }
            return "404";
        }else{
            return "/km/login";
        }
    }

    @InterfaceLimitRequest(count = 1)
    @ResponseBody
    @RequestMapping("/key/{key}")
    public String key(@PathVariable String key,HttpSession session){
        String nickName = (String)session.getAttribute("nickName");
        Map<String,Object> map=new HashMap<>();
        if(nickName!=null){

            if(key==null){
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            int i = adminService.countUserLike(key);
            if(i>0){
                map.put("state",1);
                return JSONObject.toJSONString(map);
            }
            map.put("state",0);
            return JSONObject.toJSONString(map);

        }else{
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }


    @CrossOrigin
    //模糊查询下的用户
    @RequestMapping(value = "/allUsers1/{cPage}/{key}/1",produces = "text/plain;charset=UTF-8")
    public String users(HttpSession session, Model model, @PathVariable long cPage,@PathVariable String key){
        String nickName = (String)session.getAttribute("nickName");
        if(nickName!=null){
            UserPage userPage=new UserPage();
            userPage.setCurrentPage(cPage);
            int userNum=adminService.countUserLike(key);
            if(userNum!=0){
                userPage.setTotalCount(userNum);
                userPage.setPageSize(PAGE_SIZE);
                List<XgUser> xgUsers = adminService.selectAllUsersBypageLike(cPage, PAGE_SIZE,key);
                userPage.setUsers(xgUsers);
                //userPage.getUsers().get(0).getHeadpic().replace('"',' ');
                model.addAttribute("userPage",userPage);
                model.addAttribute("key_by",key);
                return "user_by_like";
            }
            return "404";
        }else{
            return "/km/login";
        }
    }

    @InterfaceLimitRequest(count = 1)
    //修改值
    @ResponseBody
    @RequestMapping(value = "/update/{id}/{status}/{identify}/{gender}",method = RequestMethod.POST)
    public String update(HttpSession session,@PathVariable String id,@PathVariable int status,@PathVariable int identify,@PathVariable int gender){
        String nickName = (String)session.getAttribute("nickName");
        Map<String,Object> map=new HashMap<>();
        if(nickName!=null){
            boolean y = wxLoginService.userIsInRedis("loginUsers", id);
            XgUser userOld = wxLoginService.selectUser(id);
            //System.out.println(y);
            if(!y){
                int i = adminService.updateUser3(id, status, identify, gender);
                if(i>0){
                    map.put("state",1);
                    map.put("status",status);
                    map.put("identify",identify);
                    map.put("gender",gender);
                    if(status==0 && userOld.getStatus()!=0){
                        Integer uid = userService.selectIdByOpenid(id);
                        SocketMessage wdxx=new SocketMessage();
                        wdxx.setChatId(uid);
                        wdxx.setUserId(20021016);
                        wdxx.setMessage("账号异常");
                        wdxx.setMessageType(SocketMessage.SYSTEM);//1017 -系统消息
                        wdxx.setStime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                        messageService.addMessageInMongo(wdxx);
                    }
                    if(identify==1 &&userOld.getIdentify()!=1){
                        Integer uid = userService.selectIdByOpenid(id);
                        SocketMessage wdxx=new SocketMessage();
                        wdxx.setChatId(uid);
                        wdxx.setUserId(20021016);
                        wdxx.setMessage("您被设置为管理员");
                        wdxx.setMessageType(SocketMessage.SYSTEM);//1017 -系统消息
                        wdxx.setStime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                        messageService.addMessageInMongo(wdxx);
                    }
                    return JSONObject.toJSONString(map);
                }
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            XgUser u = wxLoginService.selectUserFromRedis("loginUsers", id);
            u.setStatus(status);
            u.setGender(gender);
            u.setIdentify(identify);
            boolean x = wxLoginService.addUserInRedis("loginUsers", id, u, 604800);
            if(!x){
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            int i=0;
            int count=0;
            do{
                i = adminService.updateUser3(id, status, identify, gender);
                count++;
            }
            while(i==0 && count<5);

            if(status==0 && userOld.getStatus()!=0){
                Integer uid = userService.selectIdByOpenid(id);
                SocketMessage wdxx=new SocketMessage();
                wdxx.setChatId(uid);
                wdxx.setUserId(20021016);
                wdxx.setMessage("账号异常");
                wdxx.setMessageType(SocketMessage.SYSTEM);//1017 -系统消息
                wdxx.setStime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                messageService.addMessageInMongo(wdxx);
            }
            if(identify==1 &&userOld.getIdentify()!=1){
                Integer uid = userService.selectIdByOpenid(id);
                SocketMessage wdxx=new SocketMessage();
                wdxx.setChatId(uid);
                wdxx.setUserId(20021016);
                wdxx.setMessage("您被设置为管理员");
                wdxx.setMessageType(SocketMessage.SYSTEM);//1017 -系统消息
                wdxx.setStime(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                messageService.addMessageInMongo(wdxx);
            }
            map.put("state",1);
            map.put("status",status);
            map.put("identify",identify);
            map.put("gender",gender);
            return JSONObject.toJSONString(map);

        }else{
            map.put("state",0);
            return JSONObject.toJSONString(map);
        }
    }

    @RequestMapping("/swzl/{curPage}")
    public String swzl(HttpSession session, Model model,@PathVariable Integer curPage){
        if(session.getAttribute("userName")!=null) {
            SwzlPage swzlPage = new SwzlPage();
            swzlPage.setCurrentpage(curPage);
            swzlPage.setTotalcount(postService.countSwzl());
            swzlPage.setPagesize(PAGE_SIZE);
            List<Swzl> swzls = postService.selectSwzlBypage(curPage, PAGE_SIZE);
            swzlPage.setSwzls(swzls);
            model.addAttribute("flag",1);
            model.addAttribute("swzlPage",swzlPage);
            return "swzl";
        }
        else{
            return "redirect:/km/login";
        }
    }
    @ResponseBody
    @RequestMapping("/swzl_like/{like}")
    public String swzl_like(@PathVariable String like){
        Map<String,Object> map = new HashMap<>();
        map.put("count",postService.countSwzl(like));
        return  JSONObject.toJSONString(map);
    }

    @RequestMapping("/swzl_like/{curPage}/{like}")
    public String swzl0(HttpSession session, Model model,@PathVariable Integer curPage,@PathVariable String like){
        if(session.getAttribute("userName")!=null) {
            SwzlPage swzlPage = new SwzlPage();
            swzlPage.setCurrentpage(curPage);
            swzlPage.setTotalcount(postService.countSwzl(like));
            swzlPage.setPagesize(PAGE_SIZE);
            List<Swzl> swzls = postService.selectSwzlBypage(curPage, PAGE_SIZE,like);
            swzlPage.setSwzls(swzls);
            model.addAttribute("flag",2);
            model.addAttribute("swzlPage",swzlPage);
            model.addAttribute("like",like);
            return "swzl";
        }
        else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/bbq/{curPage}")
    public String bbq(HttpSession session, Model model,@PathVariable Integer curPage){
        if(session.getAttribute("userName")!=null) {
            BbqPage bbqPage = new BbqPage();
            bbqPage.setCurrentpage(curPage);
            bbqPage.setTotalcount(postService.countBbq());
            bbqPage.setPagesize(PAGE_SIZE);
            List<Bbq> bbqs = postService.selectBbqBypage(curPage, PAGE_SIZE);
            bbqPage.setBbqs(bbqs);
            model.addAttribute("flag",1);
            model.addAttribute("bbqPage",bbqPage);
            return "bbq";
        }
        else{
            return "redirect:/km/login";
        }
    }
    @ResponseBody
    @RequestMapping("/bbq_like/{like}")
    public String bbq_like(@PathVariable String like){
        Map<String,Object> map = new HashMap<>();
        map.put("count",postService.countBbq(like));
        return  JSONObject.toJSONString(map);
    }

    @RequestMapping("/bbq_like/{curPage}/{like}")
    public String bbq0(HttpSession session, Model model,@PathVariable Integer curPage,@PathVariable String like){
        if(session.getAttribute("userName")!=null) {
            BbqPage bbqPage = new BbqPage();
            bbqPage.setCurrentpage(curPage);
            bbqPage.setTotalcount(postService.countBbq(like));
            bbqPage.setPagesize(PAGE_SIZE);
            List<Bbq> bbqs = postService.selectBbqBypage(curPage, PAGE_SIZE,like);
            bbqPage.setBbqs(bbqs);
            model.addAttribute("flag",2);
            model.addAttribute("bbqPage",bbqPage);
            return "bbq";
        }
        else{
            return "redirect:/km/login";
        }
    }

    @RequestMapping("/xyq/{curPage}")
    public String xyq(HttpSession session, Model model,@PathVariable Integer curPage){
        if(session.getAttribute("userName")!=null) {
            XyqPage xyqPage = new XyqPage();
            xyqPage.setCurrentpage(curPage);
            xyqPage.setTotalcount(postService.countXyq());
            xyqPage.setPagesize(PAGE_SIZE);
            List<Xyq> xyqs = postService.selectXyqBypage(curPage, PAGE_SIZE);
            xyqPage.setXyqs(xyqs);
            model.addAttribute("flag",1);
            model.addAttribute("xyqPage",xyqPage);
            return "xyq";
        }
        else{
            return "redirect:/km/login";
        }
    }
    @ResponseBody
    @RequestMapping("/xyq_like/{like}")
    public String xyq_like(@PathVariable String like){
        Map<String,Object> map = new HashMap<>();
        map.put("count",postService.countXyq(like));
        return  JSONObject.toJSONString(map);
    }

    @RequestMapping("/xyq_like/{curPage}/{like}")
    public String xyq0(HttpSession session, Model model,@PathVariable Integer curPage,@PathVariable String like){
        if(session.getAttribute("userName")!=null) {
            XyqPage xyqPage = new XyqPage();
            xyqPage.setCurrentpage(curPage);
            xyqPage.setTotalcount(postService.countXyq(like));
            xyqPage.setPagesize(PAGE_SIZE);
            List<Xyq> xyqs = postService.selectXyqBypage(curPage, PAGE_SIZE,like);
            xyqPage.setXyqs(xyqs);
            model.addAttribute("flag",1);
            model.addAttribute("xyqPage",xyqPage);
            return "xyq";
        }
        else{
            return "redirect:/km/login";
        }
    }
}
