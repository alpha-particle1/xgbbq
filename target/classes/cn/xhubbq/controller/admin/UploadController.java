package cn.xhubbq.controller.admin;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.blog.Blog;
import cn.xhubbq.service.blog.BlogServiceImpl;
import cn.xhubbq.service.lunbo.ILunboService;
import cn.xhubbq.utils.QiniuUtil;
import cn.xhubbq.utils.UploadUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/6
 * Description：文件上传
 */
@RestController
public class UploadController {
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private ILunboService lunboService;
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy");

    @PostMapping("/upload")
    @InterfaceLimitRequest(count = 1)
    public String upload( HttpServletRequest request, HttpSession session){
        List<MultipartFile> files;
        MultipartHttpServletRequest req= (MultipartHttpServletRequest) request;
        files=req.getFiles("files");
        //模拟用户id
        String userid="1016";
        //准备把文件上传到upload目录下,还有子目录,upload/tiezi/userid/时间/
        String realPath= session.getServletContext().getRealPath("/upload");
        realPath += File.separator+"tiezi";
        realPath += File.separator+userid;
        realPath += File.separator+sdf.format(new Date())+File.separator;
        UploadUtil.makeFolder(realPath);
        File folder=new File(realPath);
        System.out.println(realPath);
        return UploadUtil.uploadFiles(files,folder);


    }
    public final static String UPLOAD_PATH_PREFIX = "blogs";
    @PostMapping("/upload0")
    @InterfaceLimitRequest(count = 1)
    public String upload0( HttpServletRequest request, HttpSession session){
        String userName = (String)session.getAttribute("userName");
        String nickName=(String)session.getAttribute("nickName");
        Map<String,Object> map=new HashMap<>();
        //userName="小翟";
        if(userName!=null && nickName!=null ){
            List<MultipartFile> files;
            MultipartHttpServletRequest req= (MultipartHttpServletRequest) request;
            files=req.getFiles("files");
            //模拟用户id
            String userid=nickName;
            //准备把文件上传到upload目录下,还有子目录,upload/tiezi/userid/时间/
            //String realPath= session.getServletContext().getRealPath("/upload");
            String realPath = new String("upload/" + UPLOAD_PATH_PREFIX);
            realPath += File.separator+"html";
            realPath += File.separator+userid;
            realPath += File.separator+sdf.format(new Date())+File.separator;
            UploadUtil.makeFolder(realPath);

            File folder=new File(realPath);
            System.out.println(realPath);
            String originName=files.get(0).getOriginalFilename();
            String fname= UploadUtil.uploadFiles(files,folder);
            StringBuilder sb=new StringBuilder(fname);
            sb.deleteCharAt(sb.length()-1);
            fname=sb.toString();
           if(fname==null){
               map.put("state",0);
               return JSONObject.toJSONString(map);
           }
           blogService.insertBlog(new Blog(userid,originName,new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()),realPath+fname));
           map.put("state",1);
           map.put("fname",fname);
            return JSONObject.toJSONString(map);
        }
        map.put("state",-1);//未登录
        return JSONObject.toJSONString(map);
    }

    public final static String UPLOAD_PATH_PREFIX_ = "articles";
    public final static String PREFIX = "https://www.xhubbq.cn/";

    @PostMapping("/uploadT/{who}")
    @InterfaceLimitRequest(count = 1)
    public String uploadT(HttpServletRequest request, HttpSession session, @PathVariable int who){
        String userName = (String)session.getAttribute("userName");
        String nickName=(String)session.getAttribute("nickName");
        Map<String,Object> map=new HashMap<>();
        int id=0;
        if(who==1){
            id=4;
        }else if(who==2){
            id=5;
        }
        //userName="小翟";
        if(userName!=null && nickName!=null ){
            List<MultipartFile> files;
            MultipartHttpServletRequest req= (MultipartHttpServletRequest) request;
            files=req.getFiles("files");
            //模拟用户id
            String userid=nickName;
            //准备把文件上传到upload目录下,还有子目录,upload/tiezi/userid/时间/
            //String realPath= session.getServletContext().getRealPath("/upload");
            String realPath = new String("articles/" + UPLOAD_PATH_PREFIX_);
            realPath += File.separator+"html";
            realPath += File.separator+sdf.format(new Date())+File.separator;
            UploadUtil.makeFolder(realPath);

            File folder=new File(realPath);
            System.out.println(realPath);
            String originName=files.get(0).getOriginalFilename();
            String fname= UploadUtil.uploadFiles(files,folder);
            StringBuilder sb=new StringBuilder(fname);
            sb.deleteCharAt(sb.length()-1);//删除最后多余的‘/’
            fname=sb.toString();
            if(fname==null){
                map.put("state",0);
                return JSONObject.toJSONString(map);
            }
            //blogService.insertBlog(new Blog(userid,originName,new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()),realPath+fname));
            lunboService.updateLunboArticle(id,PREFIX+realPath+fname);
            map.put("state",1);
            map.put("fname",fname);
            return JSONObject.toJSONString(map);
        }
        map.put("state",-1);//未登录
        return JSONObject.toJSONString(map);
    }


    //七牛测试
    @ResponseBody
    @PostMapping(value = "/qiniu",produces ="application/json;charset=UTF-8" )
    @InterfaceLimitRequest(count = 1)
    public String qiniu(HttpServletRequest request){
        String content=request.getParameter("content");
        System.out.println(content);
        List<MultipartFile> files;
        MultipartHttpServletRequest req= (MultipartHttpServletRequest) request;
        files=req.getFiles("files");
        //System.out.println(files.get(1).getOriginalFilename());
        if(files.get(0).getSize()!=0){
            System.out.println("come in");
            QiniuUtil.qiniu(files);
        }
        return "上传调用";
    }

}
