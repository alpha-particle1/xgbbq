package cn.xhubbq.controller;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.pojo.banner.Banner;
import cn.xhubbq.service.banner.IBannerService;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Author:甲粒子
 * Date: 2021/12/21
 * Description：
 */
@RestController
@RequestMapping("/kmgzs")
public class BannerController {
    final String prefix="https://";
    final SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Autowired
    private IBannerService bannerService;
    @ResponseBody
    @InterfaceLimitRequest(count = 1)
    @RequestMapping(value = "/updateBanner/{a}/{b}/{c}",produces = "text/plain;charset=UTF-8")
    public String updateBanner(@PathVariable String a, @PathVariable String b, @PathVariable String c, HttpSession session){

        Map<String,Object> map=new HashMap<>();
        if(session.getAttribute("nickName")==null){
            map.put("state",-1);
            return  JSONObject.toJSONString(map);
        }
        a=a.replaceAll("1290183704","/");
        b=b.replaceAll("1290183704","/");
        c=c.replaceAll("1290183704","/");
        String img_urls=prefix+a+"|"+prefix+b+"|"+prefix+c;
        UpdateResult banner = bannerService.updateBanner("banner", img_urls, null);
        if (banner!=null){
            map.put("state",1);
            return JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping(value = "/banner/allBanners",produces = "application/json;charset=utf8")
    public String allBanner(){
        Map<String,Object> map = new HashMap<>();
        List<Banner> allBanners = bannerService.getAllBanners();
        if (allBanners!=null){
            map.put("state",1);
            map.put("data",allBanners.get(0));
            return  JSONObject.toJSONString(map);
        }
        map.put("state",0);
        return JSONObject.toJSONString(map);
    }
}
