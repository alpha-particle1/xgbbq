package cn.xhubbq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Author:甲粒子
 * Date: 2021/11/9
 * Description：session 跨域配置
 * 不同的域名下，Session无法共享。
 * 即设定用户在www.a.com登录，后端在Session中放入了用户的username和age，
 * 用户从www.a.com跳转到www.b.com，无法获取到Session中的用户信息。
 * 博客：https://blog.csdn.net/qincidong/article/details/82699039
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    private CorsConfiguration buildConfig(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.addExposedHeader("Authorization");
        return corsConfiguration;
    }
    @Bean
    public CorsFilter corsFilter(){
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",buildConfig());
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                //允许远端访问的域名
                .allowedOrigins("https:///www.xhubbq.cn")
                .allowedMethods("GET","POST","DELETE","PUT")
                .maxAge(3600);
    }

}
