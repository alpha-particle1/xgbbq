package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/20
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Id {
    private Integer ids[];
}
