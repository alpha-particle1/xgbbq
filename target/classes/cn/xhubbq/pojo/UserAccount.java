package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/12/2
 * Description：账户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount implements Serializable {
    private Integer userId;
    private Integer nums;
    private String ctime;
    private String ltime;
    private int version;
    private int status;
}
