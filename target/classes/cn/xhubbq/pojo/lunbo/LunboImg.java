package cn.xhubbq.pojo.lunbo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Author:甲粒子
 * Date: 2021/12/5
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LunboImg {
    private int id;
    private String imgUrl;
    private String ctime;
    private String articleAddress;
}
