package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class XglmAdmin implements Serializable {
    private int id;
    private String username;
    private String userpwd;
    private String nickname;
}
