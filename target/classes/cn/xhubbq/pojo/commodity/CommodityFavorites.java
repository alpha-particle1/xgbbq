package cn.xhubbq.pojo.commodity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：收藏
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "commodityFavorites")
public class CommodityFavorites {
    private String cid;
    private Integer userId;
    @Id
    private String id;
    private String cTime;
}
