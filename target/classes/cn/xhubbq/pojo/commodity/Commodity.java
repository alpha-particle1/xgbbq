package cn.xhubbq.pojo.commodity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2022/1/22
 * Description：跳蚤市场商品
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "commodity")
public class Commodity {
    //日常用品
    private final static int DAILY_OBJECT = 1;
    //学习用品
    private final static int STUDY_OBJECT = 2;
    //衣着装饰
    private final static int OUTLOOK_OBJECT = 3;
    //其他
    private final static int OTHER = 4;

    private Integer userId;
    @Id
    private String id;
    private String upTime;
    private String description;
    private String title;
    private String imgs;
    private Double price0;
    private Double price;
    private int type;
}
