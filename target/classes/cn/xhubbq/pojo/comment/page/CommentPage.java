package cn.xhubbq.pojo.comment.page;

import cn.xhubbq.pojo.comment.Comment;
import cn.xhubbq.pojo.letter.Letter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/11
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentPage {
    private long totalpage;
    private long currentpage;
    private long totalcount;
    private long pagesize;
    private List<Comment> comments;
    public void setPagesize(int psize){
        this.pagesize=psize;
        totalpage=totalcount%pagesize!=0?totalcount/pagesize+1:totalcount/pagesize;
    }
}
