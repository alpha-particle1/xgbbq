package cn.xhubbq.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Author:甲粒子
 * Date: 2021/11/9
 * Description：权限表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class XglmRole implements Serializable {
    private int id;
    private String name;
}
