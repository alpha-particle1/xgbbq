package cn.xhubbq.pojo.page;

import cn.xhubbq.pojo.blog.Blog;
import cn.xhubbq.pojo.letter.Letter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/11/25
 * Description：
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlogPage {
    private long totalpage;
    private long currentpage;
    private long totalcount;
    private long pagesize;
    private List<Blog> blogs;
    public void setPagesize(int psize){
        this.pagesize=psize;
        totalpage=totalcount%pagesize!=0?totalcount/pagesize+1:totalcount/pagesize;
    }
}
