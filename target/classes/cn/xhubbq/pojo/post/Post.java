package cn.xhubbq.pojo.post;

/**
 * Author:甲粒子
 * Date: 2021/12/9
 * Description：
 */
public interface Post {
    //帖子类型常量
    int TYPE_BBQ=1;
    int TYPE_XYQ=2;
    int TYPE_SWZL=3;
    int TYPE_TZSC=4;
    int postType();
}
