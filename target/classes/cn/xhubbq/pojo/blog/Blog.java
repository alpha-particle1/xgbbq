package cn.xhubbq.pojo.blog;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Author:甲粒子
 * Date: 2021/11/25
 * Description：
 */

@Document(collection = "blogs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Blog {

    private String upAuthor;
    private String fileName;
    private String upTime;
    private String filePath;

}
