package cn.xhubbq.aop;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import com.alibaba.fastjson.JSONObject;
import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Author:甲粒子
 * Date: 2021/12/14
 * Description：
 */

@Aspect
@Component
public class LimitRequestAspect {

    private static ConcurrentHashMap<String, ExpiringMap<String, Integer>> book = new ConcurrentHashMap<>();
    // 定义切点
    // 让所有有@InterfaceLimitRequest注解的方法都执行切面方法
    @Pointcut("@annotation(interfaceLimitRequest)")
    public void excudeService(InterfaceLimitRequest interfaceLimitRequest) {
    }
    @Around("excudeService(interfaceLimitRequest)")
    public Object doAround(ProceedingJoinPoint pjp, InterfaceLimitRequest interfaceLimitRequest) throws Throwable {
        // 获得request对象
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        //System.out.println("@Limit "+ip+"@ "+request.getRequestURI());

        //返回状态
        Map<String,Object> map = new HashMap<>();
        // 获取Map对象， 如果没有则返回默认值
        // 第一个参数是key， 第二个参数是默认值
        ExpiringMap<String, Integer> uc = book.getOrDefault(request.getRequestURI(), ExpiringMap.builder().variableExpiration().build());
        Integer uCount = uc.getOrDefault(ip, 0);

        if (uCount >= interfaceLimitRequest.count()) { // 超过次数，不执行目标方法
            map.put("state",-2);
            return  JSONObject.toJSONString(map);
        } else if (uCount == 0){ // 第一次请求时，设置有效时间
            uc.put(ip, uCount + 1, ExpirationPolicy.CREATED, interfaceLimitRequest.time(), TimeUnit.MILLISECONDS);
        } else { // 未超过次数， 记录加一
            uc.put(ip, uCount + 1);
        }
        book.put(request.getRequestURI(), uc);
        // result的值就是被拦截方法的返回值
        Object result = pjp.proceed();
        return result;
    }
}
