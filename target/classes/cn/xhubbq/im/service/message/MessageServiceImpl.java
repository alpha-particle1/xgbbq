package cn.xhubbq.im.service.message;

import cn.xhubbq.im.entity.SocketMessage;
import cn.xhubbq.im.repository.message.MessageMapper;
import cn.xhubbq.utils.RedisUtil;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/18
 * Description：未读消息处理服务实现
 */
@Service
public class MessageServiceImpl implements IMessageService{
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public int addUnreadMessage(SocketMessage socketMessage) {
        return messageMapper.addUnreadMessage(socketMessage);
    }

    @Override
    public int deleteUnreadMessage(Integer userId) {
        return messageMapper.deleteUnreadMessage(userId);
    }

    @Override
    public int userIsExistedAndYeah(Integer id) {
        return messageMapper.userIsExistedAndYeah(id);
    }

    @Override
    public List<SocketMessage> selectAllUnreadMessages(Integer userId) {
        return messageMapper.selectAllUnreadMessages(userId);
    }

    @Override
    public SocketMessage addMessageInMongo(SocketMessage wdxx) {
        return mongoTemplate.insert(wdxx,"wdxx");
    }

    @Override
    public DeleteResult deleteMessage(Integer chatId, Integer userId) {//接受消息方  发送消息方
        Query query=new Query(Criteria.where("chatId").is(chatId).and("userId").is(userId));
        return mongoTemplate.remove(query,"wdxx");
    }

    @Override
    public List<SocketMessage> getMessages(Integer chatId, Integer userId) {
        Query query=new Query(Criteria.where("chatId").is(chatId));
        return mongoTemplate.find(query,SocketMessage.class,"wdxx");
    }

    @Override
    public List<SocketMessage> getAllMessages(Integer chatId) {
        Query query=new Query(Criteria.where("chatId").is(chatId));
        return mongoTemplate.find(query,SocketMessage.class);
    }
}
