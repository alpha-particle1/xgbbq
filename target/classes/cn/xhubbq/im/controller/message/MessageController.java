package cn.xhubbq.im.controller.message;

import cn.xhubbq.annotation.InterfaceLimitRequest;
import cn.xhubbq.im.entity.SocketMessage;
import cn.xhubbq.im.service.message.MessageServiceImpl;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author:甲粒子
 * Date: 2021/11/18
 * Description：未读消息拉取
 */
@RestController
public class MessageController {
    @Autowired
    private MessageServiceImpl messageService;

    @ResponseBody//用于测试
    @RequestMapping(value="/readMessage",produces = "text/plain;charset=UTF-8")
    @InterfaceLimitRequest(count = 1)
    public String readTest(){
        //验证token
        //获取到id
        //拉取未读消息
        List<SocketMessage> socketMessages = messageService.selectAllUnreadMessages(123);
        for (SocketMessage socketMessage : socketMessages) {
            System.out.println(socketMessage);
        }
        return JSONObject.toJSONString(socketMessages);
    }
    @ResponseBody
    @RequestMapping(value="/readMessage/{chatId}",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
    @InterfaceLimitRequest(count = 1)
    public String readReal(@PathVariable Integer chatId){
        //验证token
        //获取到id
        //拉取未读消息
        Map<String,Object> map=new HashMap<>();
        List<SocketMessage> socketMessages = messageService.getAllMessages(chatId);
        if(socketMessages==null){
            map.put("state",0);
            map.put("message",null);
            return  JSONObject.toJSONString(map);
        }
        for (SocketMessage socketMessage : socketMessages) {
            System.out.println(socketMessage);
        }
        map.put("state",1);
        map.put("message",socketMessages);
        return JSONObject.toJSONString(map);
    }
    //已读回执
    @ResponseBody
    @RequestMapping("/read")
    @InterfaceLimitRequest(count = 1)
    public String deleteMessages(){
        Map<String,Object> map=new HashMap<>();
        //验证token
        //获取到userid（不是openid）
        //从数据库中删除已读消息
        int count=messageService.deleteUnreadMessage(123);
        if(count==1){
            map.put("status",1);
            map.put("event","删除成功");
            return  JSONObject.toJSONString(map);
        }
        map.put("status",0);
        map.put("event","删除错误");
        return JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping(value = "/read0/{chatId}/{userId}",produces = "text/plain;charset=UTF-8")
    @InterfaceLimitRequest(count = 1)
    public String deleteMessages0(@PathVariable Integer chatId,@PathVariable Integer userId){
        Map<String,Object> map=new HashMap<>();
        //验证token
        //获取到userid（不是openid）
        //从数据库中删除已读消息
        DeleteResult deleteResult = messageService.deleteMessage(chatId, userId);
        if(deleteResult.getDeletedCount()>=1){
            map.put("status",1);
            map.put("event","删除成功");
            return  JSONObject.toJSONString(map);
        }
        map.put("status",0);
        map.put("event","删除错误");
        return JSONObject.toJSONString(map);
    }

    @ResponseBody
    @RequestMapping(value = "/addMessage",produces = "text/plain;charset=UTF-8")
    @InterfaceLimitRequest(count = 1)
    public String addMessage(){
        List<SocketMessage> messages=new ArrayList<>();
        for (int i=0;i<10;++i){
           messages.add(messageService.addMessageInMongo(new SocketMessage(SocketMessage.PRIVATE_CHAT,  i + 1, i + 2, "123", "stime")));
        }
        return JSONObject.toJSONString(messages);
    }


}
