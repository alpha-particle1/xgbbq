package cn.xhubbq.im.repository;

import java.util.List;

public interface UserGroupRepository {
    List<Integer> findGroupIdByUserId(Integer userId);
}
