package cn.xhubbq.manager.factory;


import cn.xhubbq.pojo.post.*;
import cn.xhubbq.service.post.IPostService;
import cn.xhubbq.service.xguser.IUserService;
import cn.xhubbq.utils.QiniuUtil;
import cn.xhubbq.utils.SpringUtils;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author
 */
public class AsyncFactory
{


    private static IUserService userService = SpringUtils.getBean(IUserService.class);


    private static IPostService postService = SpringUtils.getBean(IPostService.class);

    private static final Logger sys_user_logger = LoggerFactory.getLogger("sys-user");

    /**
     * 测试
     * @return 任务task
     */
    public static TimerTask posts( String content, int type, int nums, List<InputStream> inputStreams)
    {

        return new TimerTask()
        {
            @Override
            public void run()
            {
                if (content!=null){
                    boolean b = userService.checkContentAndImages(null, content);
                    if(!b){
                        return;
                    }
                    Map<String, Object> qiniu = QiniuUtil.qiniu2(inputStreams);
                    if( (int)qiniu.get("state") == 0 ){
                        return;
                    }
                    if( (int)qiniu.get("nums") != nums ){
                        return;
                    }
                    String imgs = (String)qiniu.get("imgs");
                    switch (type){
                        case Post.TYPE_BBQ:{
                            Bbq bbq = JSONObject.parseObject(content, Bbq.class);
                            bbq.setImgPath(imgs);
                            postService.savePostInMongoDb(bbq);
                        }
                        case Post.TYPE_XYQ:{
                            Xyq xyq = JSONObject.parseObject(content, Xyq.class);
                            xyq.setImgPath(imgs);
                            postService.savePostInMongoDb(xyq);
                        }
                        case Post.TYPE_SWZL:{
                            Swzl swzl = JSONObject.parseObject(content, Swzl.class);
                            swzl.setImgPath(imgs);
                            postService.savePostInMongoDb(swzl);
                        }
                        case Post.TYPE_TZSC:{
                            Tzsc tzsc = JSONObject.parseObject(content, Tzsc.class);
                            tzsc.setImgPath(imgs);
                            postService.savePostInMongoDb(tzsc);
                        }
                    }
                }
            }
        };
    }

    public  void posts_test( String content, int type, int nums, HttpServletRequest request){

        if (content!=null){
            //上传文件
            List<MultipartFile> files;
            MultipartHttpServletRequest req=(MultipartHttpServletRequest)request;
            files=req.getFiles("imgs");

            boolean b = userService.checkContentAndImages(null, content);
            if(!b){
                return;
            }

            Map<String, Object> qiniu = QiniuUtil.qiniu(files);
            if( (int)qiniu.get("state") == 0 ){
                return;
            }
            if( (int)qiniu.get("nums") != nums ){
                return;
            }
            String imgs = (String)qiniu.get("imgs");
            switch (type){
                case Post.TYPE_BBQ:{
                    Bbq bbq = JSONObject.parseObject(content, Bbq.class);
                    bbq.setImgPath(imgs);
                    postService.savePostInMongoDb(bbq);
                }
                case Post.TYPE_XYQ:{
                    Xyq xyq = JSONObject.parseObject(content, Xyq.class);
                    xyq.setImgPath(imgs);
                    postService.savePostInMongoDb(xyq);
                }
                case Post.TYPE_SWZL:{
                    Swzl swzl = JSONObject.parseObject(content, Swzl.class);
                    swzl.setImgPath(imgs);
                    postService.savePostInMongoDb(swzl);
                }
                case Post.TYPE_TZSC:{
                    Tzsc tzsc = JSONObject.parseObject(content, Tzsc.class);
                    tzsc.setImgPath(imgs);
                    postService.savePostInMongoDb(tzsc);
                }
            }
        }
    }

}
