package cn.xhubbq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling//开启任务调度（定时）
@SpringBootApplication
public class XglmApplication {

    public static void main(String[] args) { SpringApplication.run(XglmApplication.class, args);
    }

}
