package cn.xhubbq.mapper.login;

import cn.xhubbq.pojo.XglmAdmin;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AdminMapper {
    //根据 账号密码查询admin
    //@Select("select count(1) from xglm_admin where userid=#{userid} and userpwd=#{userpwd}")
    int selectAdminByIdPwd(Map<String,Object> map);

    /**
     * @param
     * @return
     */
    @Select("select * from xglm_admin where username=#{name}")
    XglmAdmin loadAdminByAdminname(String name);

    /**查询所有admin
     * @param
     * @return
     */
    @Select("select * from xglm_admin")
    List<XglmAdmin> selectAlladmins();

    /**按页查询admin
     * @param
     * @return
     */
    @Select("select * from xglm_admin limit #{curPS},#{pageSize}")//curPS=pageSize*currentPage
    List<XglmAdmin> selectAlladminsBypage(int curPS,int pageSize);

    /**
     * @param
     * @return
     */
    @Select("select count(1) from xglm_admin")
    int adminsNum();

    /**
     * @param
     * @return
     */
    @Select("select user_mail from admin_info where user_name=#{username}")
    String adminMail(String username);

    /**
     * @param
     * @return
     */
    @Update("update xglm_admin set userpwd=#{newpwd} where username=#{username}")
    int updatePwd(String username,String newpwd);

    /**增加管理员
     * @param
     * @return
     */
    @Insert("insert into xglm_admin values(#{id},#{username},#{userpwd},#{nickname})")
    int addAdmin(int id,String username,String userpwd,String nickname);

    /**最大id
     * @param
     * @return
     */
    @Select("select max(id) from xglm_admin")
    int maxId();

    /**插入
     * @param
     * @return
     */
    @Insert("insert into role_user values(#{userid},#{roleid})")
    int addRole(int userid,int roleid);

    /**添加邮箱
     * @param
     * @return
     */
    @Insert("insert into admin_info values(#{username},#{useremail})")
    int addEmail(String username,String useremail);

    /**查询姓名
     * @param
     * @return
     */
    @Select("select nickname from xglm_admin where username=#{username}")
    String selectNickname(String username);

}
