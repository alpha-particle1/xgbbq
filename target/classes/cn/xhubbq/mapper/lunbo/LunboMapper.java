package cn.xhubbq.mapper.lunbo;

import cn.xhubbq.pojo.lunbo.LunboImg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Author:甲粒子
 * Date: 2021/12/5
 * Description：
 */
@Mapper
public interface LunboMapper {

    //update lunbo img
    @Update("update lunbo_img set img_url=#{imgUrl} where id=#{id}")
    int updateLunboImg(int id,String imgUrl);

    @Select("select * from lunbo_img order by id asc ")
    List<LunboImg> selectAll();

    @Update("update lunbo_img set article_address=#{articleUrl} where id=#{id}")
    int updateLunboArticle(int id,String articleUrl);
}
