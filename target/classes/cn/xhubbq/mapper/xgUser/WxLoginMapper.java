package cn.xhubbq.mapper.xgUser;

import cn.xhubbq.pojo.XgUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * Author:甲粒子
 * Date: 2021/11/16
 * Description：微信登录
 */
@Mapper
public interface WxLoginMapper {
    @Insert("insert into xg_user values(#{id},#{userid},#{nickname},#{gender}," +
            "#{headpic},#{telnum},#{attention},#{fans},#{status},#{identify}," +
            "#{ctime},#{ltime},#{version})")
    int addUser(XgUser user);

    //根据openid查询有用户
    @Select("select * from xg_user where userid=#{userid}")
    XgUser selectUser(String userid);

    //查询openid是否存在
    @Select("select count(1) from xg_user where userid=#{userid}")
    int userIsExisted(String userid);

    //查询最大id  放入redis
    @Select("select max(id) from xg_user")
    int maxId();

    //更新最新登录时间
    @Update("update xg_user set ltime=#{ltime} where userid=#{userid}")
    int updateLastTime(String ltime,String userid);
}
