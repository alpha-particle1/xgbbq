package cn.xhubbq.annotation;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
//@InterfaceLimitRequest
public @interface InterfaceLimitRequest {
    long time() default 3000; // 限制时间 单位：毫秒
    int count() default 1; // 允许请求的次数
}
